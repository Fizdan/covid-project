        <!-- START HEADER-->
        <header class="header">
            <div class="page-brand">
                <a class="link" href="<?php echo base_url() ?>">
                    <span class="brand">ITS
                        <span class="brand-tip">&nbsp;Face Shield</span>
                    </span>
                    <span class="brand-mini">ITS</span>
                </a>
            </div>
            <div class="flexbox flex-1">
                <!-- START TOP-LEFT TOOLBAR-->
                <ul class="nav navbar-toolbar">
                    <li>
                        <a class="nav-link sidebar-toggler js-sidebar-toggler"><i class="ti-menu"></i></a>
                    </li>
                </ul>
                <!-- END TOP-LEFT TOOLBAR-->
                <!-- START TOP-RIGHT TOOLBAR-->
                <ul class="nav navbar-toolbar">
                    <li class="dropdown dropdown-user">
                        <a class="nav-link dropdown-toggle link" data-toggle="dropdown">
                            <img src="<?php echo config_item('assets');?>img/admin-avatar.png" />
                            <span></span><?php echo $sess['email']; ?><i class="fa fa-angle-down m-l-5"></i></a>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <!-- <li class="dropdown-divider"></li> -->
                            <a class="dropdown-item" href="#logout" data-toggle="modal" data-target="#logout"><i class="fa fa-power-off"></i>Logout</a>
                        </ul>
                    </li>
                </ul>
                <!-- END TOP-RIGHT TOOLBAR-->
            </div>
        </header>
        <!-- END HEADER-->
		
        <!-- Logout modal -->
        <div class="modal fade" id="logout" tabindex="-1" role="dialog" aria-hidden="true">
          <div class="modal-dialog modal-md">
            <div class="modal-content">

              <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Logout</h4>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
              </div>
              <div class="modal-body">
                <p>Apa anda yakin ingin keluar dari sistem?</p>
              </div>
              <div class="modal-footer">
                <form id="delForm" class="form-horizontal form-label-left" action="<?php echo base_url('logout'); ?>" enctype="multipart/form-data">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                  <button type="submit" class="btn btn-primary">Logout</button>
                </form>
              </div>
            </div>
          </div>
        </div>
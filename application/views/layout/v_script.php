    <!-- CORE PLUGINS-->
    <script src="<?php echo config_item('assets');?>vendors/jquery/dist/jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo config_item('assets');?>vendors/popper.js/dist/umd/popper.min.js" type="text/javascript"></script>
    <script src="<?php echo config_item('assets');?>vendors/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?php echo config_item('assets');?>vendors/metisMenu/dist/metisMenu.min.js" type="text/javascript"></script>
    <script src="<?php echo config_item('assets');?>vendors/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- PAGE LEVEL PLUGINS-->

    <script src="<?php echo config_item('assets');?>vendors/select2/dist/js/select2.full.min.js" type="text/javascript"></script>
    <script src="<?php echo config_item('assets');?>vendors/chart.js/dist/Chart.min.js" type="text/javascript"></script>
    <script src="<?php echo config_item('assets');?>vendors/jvectormap/jquery-jvectormap-2.0.3.min.js" type="text/javascript"></script>
    <script src="<?php echo config_item('assets');?>vendors/jvectormap/jquery-jvectormap-world-mill-en.js" type="text/javascript"></script>
    <script src="<?php echo config_item('assets');?>vendors/jvectormap/jquery-jvectormap-us-aea-en.js" type="text/javascript"></script>
    <script src="<?php echo config_item('assets');?>vendors/DataTables/datatables.min.js" type="text/javascript"></script>
    <!-- CORE SCRIPTS-->
    <script src="<?php echo config_item('assets');?>js/app.min.js" type="text/javascript"></script>
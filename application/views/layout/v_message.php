					<?php if ($this->session->flashdata('error')) { ?>
						<div class="form-group">
						<div class="alert alert-danger alert-primary alert-block">
							<?php echo $this->session->flashdata('error') ?>
						</div>
						</div>
					<?php } ?>
                    <?php if ($this->session->flashdata('success')) { ?>
						<div class="form-group">
						<div class="alert alert-success alert-primary alert-block">
							<?php echo $this->session->flashdata('success') ?>
                        </div>
						</div>
                    <?php } ?>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <title><?= $header['title'] ?></title>
    <!-- GLOBAL MAINLY STYLES-->
    <link href="<?php echo config_item('assets');?>vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo config_item('assets');?>vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
    <link href="<?php echo config_item('assets');?>vendors/themify-icons/css/themify-icons.css" rel="stylesheet" />
    <!-- PLUGINS STYLES-->
    <link href="<?php echo config_item('assets');?>vendors/jvectormap/jquery-jvectormap-2.0.3.css" rel="stylesheet" />
    <link href="<?php echo config_item('assets');?>vendors/select2/dist/css/select2.min.css" rel="stylesheet" />
    <link href="<?php echo config_item('assets');?>vendors/DataTables/datatables.min.css" rel="stylesheet" />
    <!-- THEME STYLES-->
    <link href="<?php echo config_item('assets');?>css/main.min.css" rel="stylesheet" />
    <link href="<?php echo config_item('assets');?>css/themes/blue-light.css" rel="stylesheet" />
    <!-- PAGE LEVEL STYLES-->
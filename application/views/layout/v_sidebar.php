<!-- START SIDEBAR-->
        <nav class="page-sidebar" id="sidebar">
            <div id="sidebar-collapse">
                <div class="admin-block d-flex">
                    <div>
                        <img src="<?php echo config_item('assets');?>img/admin-avatar.png" width="45px" />
                    </div>
                    <div class="admin-info">
                        <div class="font-strong"><?php echo $sess['nama']; ?></div><small><?php echo $sess['jenis_pengguna']; ?></small></div>
                </div>
                <ul class="side-menu metismenu">
                    <li>
                        <a href="<?php echo base_url('dashboard') ?>"><i class="sidebar-item-icon fa fa-th-large"></i>
                            <span class="nav-label">Dashboard</span>
                        </a>
                    </li>
                    <li class="heading">FEATURES</li>
                    <li>
                        <a href="<?php echo base_url('relawan/laporan') ?>"><i class="sidebar-item-icon fa fa-book"></i>
                            <span class="nav-label">Laporan</span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:;"><i class="sidebar-item-icon fa fa-list"></i>
                            <span class="nav-label">Progres</span><i class="fa fa-angle-left arrow"></i></a>
                        <ul class="nav-2-level collapse">
                          <li><a href="<?php echo base_url('relawan/progres/distribusi');?>">Distribusi</a></li>
                          <li><a href="<?php echo base_url('relawan/progres/produksi');?>">Produksi</a></li>
                        </ul>
                    </li>               
                    <li>
                        <a href="javascript:;"><i class="sidebar-item-icon fa fa-group"></i>
                            <span class="nav-label">Relawan</span><i class="fa fa-angle-left arrow"></i></a>
                        <ul class="nav-2-level collapse">
						  <li><a href="<?php echo base_url('relawan/instansi');?>">Data Instansi</a></li>
                          <li><a href="<?php echo base_url('relawan/limit');?>">Data Limit</a></li>
						  <li><a href="<?php echo base_url('relawan/request');?>">Data Request</a></li>
						  <li><a href="<?php echo base_url('relawan/kebutuhan');?>">Data Kebutuhan</a></li>
						  <li><a href="<?php echo base_url('relawan/donatur');?>">Data Donatur</a></li>
						  <!-- <li><a href="<?php echo base_url('relawan/recruit');?>">Data Relawan</a></li> -->
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
        <!-- END SIDEBAR-->
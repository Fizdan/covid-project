<!DOCTYPE html>
<html lang="en">

<head>
<?php $this->load->view('layout/v_header')?>
</head>

<body class="fixed-navbar has-animation">
    <div class="page-wrapper">
	
<?php $this->load->view('layout/v_navbar')?>

<?php $this->load->view('layout/v_sidebar')?>

        <div class="content-wrapper">
            <!-- START PAGE CONTENT-->
            <div class="page-content fade-in-up">
<?php $this->load->view('layout/v_message')?>
		
<?php $this->load->view('content/'.$header['content'])?>

            </div>
            <!-- END PAGE CONTENT-->
            <footer class="page-footer">
                <div class="font-13">2020 &copy; <b>ITS Face Shield IT Team Development</b> - All rights reserved.</div>
                <div class="to-top"><i class="fa fa-angle-double-up"></i></div>
            </footer>
        </div>
    </div>
  
    <!-- BEGIN PAGA BACKDROPS-->
    <div class="sidenav-backdrop backdrop"></div>
    <div class="preloader-backdrop">
        <div class="page-preloader">Loading</div>
    </div>
    <!-- END PAGA BACKDROPS-->

<?php $this->load->view('layout/v_script')?>

<?php $this->load->view('layout/v_js')?>

</body>

</html>
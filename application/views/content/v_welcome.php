<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo config_item('assets');?>material-kit/img/apple-icon.png">
	<link rel="icon" type="image/png" href="<?php echo config_item('assets');?>material-kit/img/favicon.png">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title><?php echo $title;?></title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />

	<!-- Fonts and icons  -->
	<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />

	<!-- CSS Files -->
    <link href="<?php echo config_item('assets');?>material-kit/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo config_item('assets');?>material-kit/css/material-kit.css?v=1.2.1" rel="stylesheet"/>

</head>

<body class="section-white">

    <nav class="navbar navbar-primary navbar-fixed-top" id="sectionsNav">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <img style="height:50px;" src="<?php echo config_item('assets');?>material-kit/img/logo.png">
            </div>

            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-left">
                    <li>
                        <a href="<?php echo base_url() ?>">
                            <b>Beranda</b>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo base_url('request') ?>">
                            <b>Request APD</b>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo base_url('relawan') ?>">
                            <b>Daftar Relawan</b>
                        </a>
                    </li>               
                </ul>
                
                <ul class="nav navbar-nav navbar-right">            
                    <li>
                        <a href="<?php echo base_url('login');?>">
                            <b>Masuk</b>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

	<div class="section-space"></div>

	<div class="cd-section" id="headers">

    <!--     *********     HEADER 1      *********      -->

    <div class="header-1">
        <div class="page-header header-filter" style="background-image: url('<?php echo config_item('assets');?>material-kit/img/header.jpg');">
            <div class="container">
                <div class="row">
					<!-- <div class="col-md-8"> -->
					<div class="col-md-8 col-md-offset-2 text-center">
						<h1 class="title">ITS Face Shield</h1>
	                    <h4>ITS akan memproduksi Face Shield yang merupakan salah satu Alat Perlengkapan Diri (APD) yang sangat dibutuhkan tenaga medis dan tenaga paramedis di Rumah Sakit, Puskesmas, Klinik atau Fasilitas Kesehatan lainnya untuk mencega penyebaran Covid-19. Donasi anda akan sangat bermanfaat untuk melindungi mereka, para pahlawan medis kita.</h4>
	                    <br />
	                    <a href="#status" class="btn btn-primary btn-lg">
							Lihat Status
						</a>
					</div>
                </div>
            </div>
        </div>
    </div>

	<div class="cd-section" id="status">

	<div class="container">

		<!--     *********     FEATURES 1      *********      -->

	    <div class="features-1">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
					<h5 class="description">Bantuan anda sangat diharapkan dalam pembuatan Face Shield ini, dan bantuan anda dalam pembuatan Face Shield ini sudah mencapai</h5>
				</div>
			</div>

			<div class="row">
				<div class="col-md-3">
					<div class="info">
						<div class="text-center">
							<img style="height: 100px;" src="<?php echo config_item('assets');?>material-kit/img/donate.svg">
						</div>
						<h3 class="info-title">967</h3>
						<p>Donatur ITS Face Shield sudah mencapai 967 Donatur.</p>
					</div>
				</div>

				<div class="col-md-3">
					<div class="info">
						<div class="text-center">
							<img style="height: 100px;" src="<?php echo config_item('assets');?>material-kit/img/request.svg">
						</div>
						<h3 class="info-title">100,455</h3>
						<p>Pemesanan kebutuhan sudah mencapai 100,455 Juta</p>
					</div>
				</div>

				<div class="col-md-3">
					<div class="info">
						<div class="text-center">
							<img style="height: 100px;" src="<?php echo config_item('assets');?>material-kit/img/distribusi.svg">
						</div>
						<h3 class="info-title">628,490</h3>
						<p>Distribusi barang untuk sudah mencapai 628,490 Juta</p>
					</div>
				</div>

				<div class="col-md-3">
					<div class="info">
						<div class="text-center">
							<img style="height: 100px;" src="<?php echo config_item('assets');?>material-kit/img/money.svg">
						</div>
						<h3 class="info-title">376,060</h3>
						<p>Dana yang tersisa saat ini berjumlah 376,060 Juta.</p>
					</div>
				</div>

				<div class="col-md-8 col-md-offset-2 text-center">
	                    <a href="#donasi" class="btn btn-primary btn-lg">
							Selengkapnya
						</a>
				</div>
			</div>

	    </div>

		<!--     *********    END FEATURES 1      *********      -->

	</div>
	</div>

    <!--     *********    END HEADER 1      *********      -->
	<div class="cd-section" id="donasi">

	<!--     *********     HEADER 3      *********      -->
    <div class="header-2">
        <div class="page-header header-filter" style="background-image: url('<?php echo config_item('assets');?>material-kit/img/header-2.jpg');">
            <div class="container" style="padding-top: 10vh;">
                <div class="row">
	    					<div class="col-md-4">
	    						<div class="card card-profile">
	    									<img class="img" src="<?php echo config_item('assets');?>material-kit/img/header.jpg" />

	    							<div class="card-content">
	    								<h4 class="card-title">Donasi</h4>
	    								<div class="category text-gray">Setiap donasi yang anda kirimkan akan sangat membantu kami dalam memenuhi alat kebutuhan</div>
									<div class="text-center">
						                    <a href="#donasiModal" data-toggle="modal" data-target="#donasiModal" class="btn btn-primary btn-lg">
												Donasi Sekarang
											</a>
									</div>
	    							</div>
	    						</div>
	    					</div>
	    					<div class="col-md-4">
	    						<div class="card card-profile">
	    									<img class="img" src="<?php echo config_item('assets');?>material-kit/img/header.jpg" />

	    							<div class="card-content">
	    								<h4 class="card-title">Daftar Relawan</h4>
	    								<div class="category text-gray">Bantuan individu sebagai sumber daya untuk pembuatan APD ini sangat diharapkan untuk memenuhi kebutuhan Alat Pelindung Diri</div>
									<div class="text-center">
						                    <a href="<?php echo base_url('relawan') ?>" class="btn btn-primary btn-lg">
												Daftar Sekarang
											</a>
									</div>
	    							</div>
	    						</div>
	    					</div>
	    					<div class="col-md-4">
	    						<div class="card card-profile">
	    									<img class="img" src="<?php echo config_item('assets');?>material-kit/img/header.jpg" />

	    							<div class="card-content">
	    								<h4 class="card-title">Request Alat Pelindung Diri</h4>
	    								<div class="category text-gray">Bila anda adalah Tenaga Medis yang membutuhkan Alat Pelindung Diri dengan jumlah yang banyak</div>
									<div class="text-center">
						                    <a href="<?php echo base_url('request') ?>" class="btn btn-primary btn-lg">
												Request Sekarang
											</a>
									</div>
	    							</div>
	    						</div>
	    					</div>
                </div>
            </div>
        </div>
    </div>

    </div>

    <!--     *********    END HEADER 3      *********      -->


<!-- Classic Modal -->
<div class="modal fade" id="donasiModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
					<i class="material-icons">clear</i>
				</button>
				<h4 class="modal-title">Donasi</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-lg-12">
					<div class="text-center">
						<img style="width: 100%;" src="<?php echo config_item('assets');?>material-kit/img/flayer.jpg">
					</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-simple" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<!--  End Modal -->

	<!--     *********    BIG FOOTER     *********      -->

					<footer class="footer footer-black footer-big">
						<div class="container">

							<div class="content">
								<div class="row">
									<div class="col-md-4">
										<h5>About Us</h5>
										<p>Creative Tim is a startup that creates design tools that make the web development process faster and easier. </p> <p>We love the web and care deeply for how users interact with a digital product. We power businesses and individuals to create better looking web projects around the world. </p>
									</div>

									<div class="col-md-4">
										<h5>Social Feed</h5>
										<div class="social-feed">
											<div class="feed-line">
												<i class="fa fa-twitter"></i>
												<p>How to handle ethical disagreements with your clients.</p>
											</div>
											<div class="feed-line">
												<i class="fa fa-twitter"></i>
												<p>The tangible benefits of designing at 1x pixel density.</p>
											</div>
											<div class="feed-line">
												<i class="fa fa-facebook-square"></i>
												<p>A collection of 25 stunning sites that you can use for inspiration.</p>
											</div>
										</div>
									</div>

									<div class="col-md-4">
										<h5>Instagram Feed</h5>

									</div>
								</div>
							</div>


							<hr />

							<ul class="pull-left">
			                    <li>
			                        <a href="<?php echo base_url() ?>">
			                            <b>Beranda</b>
			                        </a>
			                    </li>
			                    <li>
			                        <a href="<?php echo base_url('request') ?>">
			                            <b>Request APD</b>
			                        </a>
			                    </li>
			                    <li>
			                        <a href="<?php echo base_url('relawan') ?>">
			                            <b>Daftar Relawan</b>
			                        </a>
			                    </li>  
							</ul>

							<div class="copyright pull-right">
								Copyright &copy; <script>document.write(new Date().getFullYear())</script> Tim Relawan IT ITS Face Shield.
							</div>
						</div>
					</footer>

					<!--     *********   END BIG FOOTER     *********      -->

	<!--   Core JS Files   -->
	<script src="<?php echo config_item('assets');?>material-kit/js/jquery.min.js" type="text/javascript"></script>
	<script src="<?php echo config_item('assets');?>material-kit/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="<?php echo config_item('assets');?>material-kit/js/material.min.js"></script>

	<!--	Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/   -->
	<script src="<?php echo config_item('assets');?>material-kit/js/nouislider.min.js" type="text/javascript"></script>

	<!--    Control Center for Material Kit: activating the ripples, parallax effects, scripts from the example pages etc    -->
	<script src="<?php echo config_item('assets');?>material-kit/js/material-kit.js" type="text/javascript"></script>
	
	<!--	Plugin for Tags, full documentation here: http://xoxco.com/projects/code/tagsinput/   -->
	<script src="<?php echo config_item('assets');?>material-kit/js/bootstrap-tagsinput.js"></script>

</body>

</html>
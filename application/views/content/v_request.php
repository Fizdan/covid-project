<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo config_item('assets');?>material-kit/img/apple-icon.png">
    <link rel="icon" type="image/png" href="<?php echo config_item('assets');?>material-kit/img/favicon.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title><?php echo $title;?></title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />

    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />

    <!-- CSS Files -->
    <link href="<?php echo config_item('assets');?>material-kit/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo config_item('assets');?>material-kit/css/material-kit.css" rel="stylesheet"/>
</head>


<body class="signup-page">
    <nav class="navbar navbar-primary navbar-fixed-top" id="sectionsNav">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <img style="height:50px;" src="<?php echo config_item('assets');?>material-kit/img/logo.png">
            </div>

            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-left">
                    <li>
                        <a href="<?php echo base_url() ?>">
                            <b>Beranda</b>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo base_url('request') ?>">
                            <b>Request APD</b>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo base_url('relawan') ?>">
                            <b>Daftar Relawan</b>
                        </a>
                    </li>               
                </ul>
                
                <ul class="nav navbar-nav navbar-right">            
                    <li>
                        <a href="<?php echo base_url('login');?>">
                            <b>Masuk</b>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    
    <div class="page-header header-filter" style="background-image: url('<?php echo config_item('assets');?>material-kit/img/header.jpg'); background-size: cover; background-position: top center;">
        <div class="container">
            <div class="row">
                <div class="section section-basic">
                    <div class="card card-signup"  style="padding-top: 0px;padding-bottom: 40px">
                            <ul class="nav nav-pills nav-pills-rose" style="padding-top: 20px">
                              <li class="active" style="padding-left: 40px"><a href="#pill1" data-toggle="tab">Request</a></li>
                              <li><a href="#pill2" data-toggle="tab">Instansi Baru</a></li>
                            </ul>
                        <hr>

                <?php if ($this->session->flashdata('error')) { ?>
                        <div class="alert alert-danger">
                                <div class="alert-icon">
                                    <i class="material-icons">error_outline</i>
                                </div>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true"><i class="material-icons">clear</i></span>
                                </button>
                                <b>Failed:</b> <?php echo $this->session->flashdata('error') ?>
                        </div>
                <?php } ?>
                <?php if ($this->session->flashdata('success')) { ?>
                        <div class="alert alert-success">
                                <div class="alert-icon">
                                    <i class="material-icons">check</i>
                                </div>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true"><i class="material-icons">clear</i></span>
                                </button>
                                <b>Success:</b> <?php echo $this->session->flashdata('success') ?>
                        </div>
                <?php } ?> 

                        <h2 class="card-title text-center mt-10">Request APD</h2>
                            <div class="tab-content tab-space">
                                <div class="tab-pane active" id="pill1">
                                  <div class="row">
                                    <div class="col-md-10 col-md-offset-1">
                                        <form action="<?php echo base_url(). 'home/request/add'; ?>" method="POST" enctype="multipart/form-data">
                                        <div class="col-md-7">
                                            <div class="form-group"><label class="col-sm-10 control-label">Nama <span class="required">*</span></label>
                                                <div class="col-sm-12">
                                                    <input type="text" id="nama" name="nama" value="<?php echo set_value('nama'); ?>" required="required" class="form-control ">
                                                </div>
                                            </div>
                                            <div class="form-group"><label class="col-sm-10 control-label">Nomor Whatsapp <span class="required">*</span></label>
                                                <div class="col-sm-12">
                                                    <input type="text" id="wa" name="wa" required="required" class="form-control">
                                                </div>
                                            </div>
                                            <div class="form-group"><label class="col-sm-10 control-label">Instansi <span class="required">*</span></label>
                                                <div class="col-sm-12">
                                                    <select class="selectpicker" required="required" id="instansi" data-style="btn btn-default" data-live-search="true" name="instansi" data-size="7">
                                                      <option disabled="" selected="">Pilih Instansi</option>
                                                      <?php
                                                      foreach ($instansi as $row)
                                                        {
                                                            echo '<option value='.$row->id_instansi.'>'.$row->nama_instansi.' | '.$row->provinsi.' | '.$row->kota.' | '.$row->kode_pos.'</option>';
                                                        }
                                                      ?>
                                                    </select>
                                                </div>
                                            </div>
                                           <div class="form-group"><label class="col-sm-10 control-label">Alamat Kirim <span class="required">*</span></label>
                                                <div class="col-sm-12">
                                                    <textarea id="alamat" name="alamat" required="required" class="form-control"></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group"><label class="col-sm-3 control-label">Surat Permohonan <span class="required">*</span></label>
                                                <div class="col-sm-9">
                                                <span class="btn btn-file btn-sm">
                                                    <span class="fileinput-new">Select file</span>
                                                    <input type="file" name="surat_permohonan" />
                                                </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-5">
                                            <h2><small>Permintaan APD</small></h2>
                                            <hr>
                                            <table class="table table-striped">
                                                <thead>
                                                    <tr>
                                                        <th><small>Jenis APD</small></th>
                                                        <th style="text-align: center;"><small>Tersedia</small></th>
                                                        <th style="text-align: center;"><small>Permintaan</small></th>
                                                    </tr>
                                              </thead>
                                              <tbody>
                                                    <tr>
                                                        <th><small>Face Shield</small></th>
                                                        <td align="center"><small>0 Pcs</small></td>
                                                        <td align="center"><small><input type="number" style="width: 100px" min="1" id="jumlah" name="jumlah" required="required"></small></td>
                                                    </tr>
                                              </tbody>
                                            </table>
                                        </div>
                                        <div class="col-md-12">
                                            <hr>
                                            <div class="form-group">
                                              <div class="col-md-12">
                                                <button class="btn btn-secondary" type="reset">Reset</button>
                                                <button type="submit" class="btn btn-primary pull-right">Submit</button>
                                              </div>
                                            </div>
                                        </div>                            
                                    </form>
                                    </div>
                                    </div>
                                </div>

                                <!-- form instansi baru -->
                                <div class="tab-pane" id="pill2">
                                    <div class="row">
                                    <div class="col-md-10 col-md-offset-1">
                                        <form action="<?php echo base_url(). 'home/request/addInstansiBaru'; ?>" method="POST" enctype="multipart/form-data">
                                        <div class="col-md-6">
                                            <div class="form-group"><label class="col-sm-10 control-label">Nama Instansi <span class="required">*</span></label>
                                                <div class="col-sm-12">
                                                    <input type="text" id="nama_instansi" name="nama_instansi" value="<?php echo set_value('nama'); ?>" required="required" class="form-control ">
                                                </div>
                                            </div>
                                            <div class="form-group"><label class="col-sm-10 control-label">Jenis Instansi <span class="required">*</span></label>
                                                <div class="col-sm-12">
                                                    <select class="selectpicker" required="required" id="jenis_instansi" data-style="btn btn-default" data-live-search="true" name="jenis_instansi" data-size="7">
                                                      <option disabled="" selected="">Pilih Jenis Instansi</option>
                                                      <option value="Pemerintah" >Pemerintah</option>
                                                      <option value="Swasta" >Swasta</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group"><label class="col-sm-10 control-label">Tipe Instansi <span class="required">*</span></label>
                                                <div class="col-sm-12">
                                                    <select class="selectpicker" required="required" id="tipe_instansi" data-style="btn btn-default" data-live-search="true" name="tipe_instansi" data-size="7">
                                                      <option disabled="" selected="">Pilih Tipe Instansi</option>
                                                  <?php foreach ($getTipe as $row){
                                                        echo "<option value='".$row->id_tipe."'>".$row->tipe_nama."</option>";
                                                  } ?>   
                                                    </select>
                                                </div>
                                            </div>
                                           <div class="form-group"><label class="col-sm-10 control-label">Provinsi Instansi <span class="required">*</span></label>
                                                <div class="col-sm-12">
                                                    <select class="selectpicker" id="provinsi_instansi" required="required"  data-style="btn btn-default" data-live-search="true" name="provinsi_instansi" >
                                                      <option disabled="" selected="">Pilih Provinsi</option>
                                                  <?php foreach ($getProvinsi as $row){
                                                        echo "<option value='".$row->id_wilayah."'>".$row->nama_wilayah."</option>";
                                                  } ?>   
                                                    </select>
                                                </div>
                                            </div> 
                                           <div class="form-group" style="display:none" id="kota_menu"><label class="col-sm-10 control-label">Kota Instansi <span class="required">*</span></label>
                                                <div class="col-sm-12">
                                                    <select class="selectpicker" required="required" id="kota_instansi" data-style="btn btn-default" data-live-search="true" name="kota_instansi" data-size="7">
                                                      <option disabled="" selected="">Pilih Kota</option>
                                                    </select>
                                                </div>
                                            </div>                         
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group"><label class="col-sm-10 control-label">Nama <span class="required">*</span></label>
                                                <div class="col-sm-12">
                                                    <input type="text" id="nama" name="nama" value="<?php echo set_value('nama'); ?>" required="required" class="form-control ">
                                                </div>
                                            </div>
                                            <div class="form-group"><label class="col-sm-10 control-label">Nomor Whatsapp <span class="required">*</span></label>
                                                <div class="col-sm-12">
                                                    <input type="text" id="wa" name="wa" required="required" class="form-control">
                                                </div>
                                            </div>
                                           <div class="form-group"><label class="col-sm-10 control-label">Alamat Kirim <span class="required">*</span></label>
                                                <div class="col-sm-12">
                                                    <textarea id="alamat" name="alamat" required="required" class="form-control"></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group"><label class="col-sm-4 control-label">Surat Permohonan <span class="required">*</span></label>
                                                <div class="col-sm-8">
                                                <span class="btn btn-file btn-sm">
                                                    <span class="fileinput-new">Select file</span>
                                                    <input type="file" name="surat_permohonan" />
                                                </span>
                                                </div>
                                            </div>                                 
                                            <h2><small>Permintaan APD</small></h2>
                                            <hr>
                                            <table class="table table-striped">
                                                <thead>
                                                    <tr>
                                                        <th><small>Jenis APD</small></th>
                                                        <th style="text-align: center;"><small>Tersedia</small></th>
                                                        <th style="text-align: center;"><small>Permintaan</small></th>
                                                    </tr>
                                              </thead>
                                              <tbody>
                                                    <tr>
                                                        <th><small>Face Shield</small></th>
                                                        <td align="center"><small>0 Pcs</small></td>
                                                        <td align="center"><small><input type="number" style="width: 100px" min="1" id="jumlah" name="jumlah" required="required"></small></td>
                                                    </tr>
                                              </tbody>
                                            </table>
                                        </div>
                                        <div class="col-md-12">
                                            <hr>
                                            <div class="form-group">
                                              <div class="col-md-12">
                                                <button class="btn btn-secondary" type="reset">Reset</button>
                                                <button type="submit" class="btn btn-primary pull-right">Submit</button>
                                              </div>
                                            </div>
                                        </div>                            
                                    </form>
                                    </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="<?php echo config_item('assets');?>vendors/jquery/dist/jquery.min.js" type="text/javascript"></script>

    
  
    <!--   Core JS Files   -->
    <script src="<?php echo config_item('assets');?>material-kit/js/jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo config_item('assets');?>material-kit/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?php echo config_item('assets');?>material-kit/js/material.min.js"></script>

    <!--    Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/   -->
    <script src="<?php echo config_item('assets');?>material-kit/js/nouislider.min.js" type="text/javascript"></script>

    <!--    Control Center for Material Kit: activating the ripples, parallax effects, scripts from the example pages etc    -->
    <script src="<?php echo config_item('assets');?>material-kit/js/material-kit.js" type="text/javascript"></script>
    
    <!--    Plugin for Tags, full documentation here: http://xoxco.com/projects/code/tagsinput/   -->
    <script src="<?php echo config_item('assets');?>material-kit/js/bootstrap-tagsinput.js"></script>
    <!--    Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
    <script src="<?php echo config_item('assets');?>material-kit/js/bootstrap-selectpicker.js" type="text/javascript"></script>
    <script>
      $(document).ready(function() {
        // AJAX for Select KOTA
        $('#provinsi_instansi').change(function(){
          $("#kota_instansi option").remove();
          $('#kota_instansi').append($('<option selected="" disabled="">Pilih Kota</option> '));
          $("#kota_menu").hide();
          let id = $(this).val();
          $.ajax({
            url : '<?php echo base_url(). 'home/request/selectkota'; ?>',
            data: {
              "id": id
            },
            type: 'post',
            dataType: 'json',
            success: function( result )
            {
              $.each( result, function(k, v) {
                // alert(k);
                $('#kota_instansi').append($("<option></option>").attr("value",k).text(v)).selectpicker('refresh');
                $("#kota_menu").show();
              });
            },
            error: function()
            {
              //handle errors
              alert('Error...');
            }
          });
        });
      });
      </script>
</body>
</html>
        <div class="ibox">
            <div class="ibox-head">
                <div class="ibox-title">Data Distribusi</div>
                <!-- <button class="btn btn-success pull-right" type="button" href="#">Download Excel</button> -->
            </div>
            <div class="ibox-body">
              <div style="overflow-x:auto;">
                <table class="table table-striped table-bordered table-hover" id="dataTables-data" width="100%">
                    <thead>
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                        <tr>
                          <th style="text-align: center;">No</th>
                          <th style="text-align: center;" class="select-filter">Instansi</th>
                          <th style="text-align: center;" class="select-filter">Provinsi</th>
                          <th style="text-align: center;" class="select-filter">Kota</th>
                          <th style="text-align: center;">Jumlah</th>
                          <th style="text-align: center;" class="select-filter">Kurir</th>
                          <th style="text-align: center;">No Resi</th>
                          <th style="text-align: center;" class="select-filter">Tanggal</th>
                          <th style="text-align: center;" class="select-filter">Diinput</th>
                        </tr>
                    </thead>
                    <tbody>
                      <?php
                      $no = 1;
                      foreach ($distribusi as $row)
                        {
                            echo '<tr align="center">
                                    <td style="text-align: center;">'.$no.'</td>
                                    <td>'.$row->nama_instansi.'</td>
                                    <td>'.$row->provinsi.'</td>
                                    <td>'.$row->kota.'</td>
                                    <td>'.$row->jumlah_distribusi.'</td>
                                    <td>'.$row->kurir_pengiriman.'</td>
                                    <td>'.$row->resi_pengiriman.'</td>
                                    <td>'.$row->created_at.'</td>
                                    <td>'.$row->oleh.'</td>
                                  </tr>';
                        $no++;
                        }
                      ?>
                    </tbody>
                </table>
              </div>
          </div>
        </div>

        <div class="ibox">
            <div class="ibox-head">
                <div class="ibox-title">Data Produksi</div>
                <!-- <button class="btn btn-success pull-right" type="button" href="#">Download Excel</button> -->
            </div>
            <div class="ibox-body">
              <div style="overflow-x:auto;">
                <table class="table table-striped table-bordered table-hover" id="dataTables-data2" width="100%">
                    <thead>
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                        <tr>
                          <th style="text-align: center;">No</th>
                          <th style="text-align: center;" class="select-filter">Nama Barang</th>
                          <th style="text-align: center;">Jumlah Produksi</th>
                          <th style="text-align: center;" class="select-filter">Tanggal</th>
                          <th style="text-align: center;" class="select-filter">Diinput</th>
                        </tr>
                    </thead>
                    <tbody>
                      <?php
                      $no = 1;
                      foreach ($produksi as $row)
                        {
                            echo '<tr align="center">
                                    <td style="text-align: center;">'.$no.'</td>
                                    <td>'.$row->nama_kebutuhan.'</td>
                                    <td>'.$row->jumlah_produksi.'</td>
                                    <td>'.$row->created_at.'</td>
                                    <td>'.$row->oleh.'</td>
                                  </tr>';
                        $no++;
                        }
                      ?>
                    </tbody>
                </table>
              </div>
          </div>

  <!-- CORE PLUGINS-->
  <script src="<?php echo config_item('assets');?>vendors/jquery/dist/jquery.min.js" type="text/javascript"></script>

  <!-- Datatables -->
  <script>
      $(document).ready(function(){    
          $("table[id^='dataTables-data']").DataTable({
              pageLength: 5,
              lengthMenu: [5, 10, 20, 50, 100],
              responsive: true,
              scrollCollapse: true 
          });
      });
  </script>
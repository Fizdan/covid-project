  <div class="ibox">
    <div class="ibox-head">
        <div class="ibox-title">Data Request</div>
        <div class="ibox-tools">
            <a class="ibox-collapse"><i class="fa fa-minus"></i></a>
        </div>
    </div>
      <div class="ibox-body">
      <div style="overflow-x:auto;">
        <table class="table table-striped table-bordered table-hover" id="dataTables-data" width="100%">
            <thead>
              <tr>
                  <th></th>
                  <th style="text-align: center;"></th>
                  <th style="text-align: center;"></th>
                  <th style="text-align: center;"></th>
                  <th style="text-align: center;"></th>
                  <th style="text-align: center;"></th>
                  <th style="text-align: center;"></th>
              </tr>
              <tr>
                  <th style="text-align: center;">No</th>
                  <th style="text-align: center; width: 100%;" class="select-filter">Instansi</th>
                  <th style="text-align: center;">Permintaan</th>
                  <th style="text-align: center;">Pemenuhan</th>
                  <th style="text-align: center;">Biaya</th>
                  <th style="text-align: center;">Detail</th>
                  <th style="text-align: center; min-width: 80px">Action</th>
              </tr>
            </thead>
            <tbody>
              <?php
              $no = 1;
              foreach ($requestTerima as $row){
                    echo '<tr>
                            <td style="text-align: center;">'.$no.'</td>
                            <td>'.$row->nama_instansi.'</td>
                            <td style="text-align: center;">'.$row->jumlah_permintaan.'</td>
                            <td style="text-align: center;">'.$row->jumlah_pemenuhan.'</td>
                            <td style="text-align: center;">'.$row->estimasi_biaya.'</td>
                            <td style="text-align: center;"><a data-toggle="modal" data-nm="'.$row->nama_instansi.'" data-jenis="'.$row->jenis_instansi.'" data-ins="'.$row->tipe_instansi.'" data-status="'.$row->status.'" data-pic="'.$row->pic.'" data-wa="'.$row->wa_pic.'" data-permintaan="'.$row->jumlah_permintaan.'" data-pemenuhan="'.$row->jumlah_pemenuhan.'" data-biaya="'.$row->estimasi_biaya.'" data-surat="'.$row->surat_permohonan.'" data-wilayah="'.$row->id_wilayah.'" data-alamat="'.$row->alamat_kirim.'" data-target="#detail" href="#detail">Lihat<a/></td> 
                            <td style="text-align: center;">
                                <button class="btn btn-info" type="button" data-toggle="modal" data-id="'.$row->id_transaksi_kebutuhan.'" data-permintaan="'.$row->jumlah_permintaan.'" data-pemenuhan="'.$row->jumlah_pemenuhan.'" data-biaya="'.$row->estimasi_biaya.'" data-target="#edit"><i class="fa fa-edit"></i></button>
                                <button class="btn btn-success" type="button" data-toggle="modal" data-target="#finish" data-id="'.$row->id_transaksi_kebutuhan.'"><i class="fa fa-check"></i></button>
                            </td>
                          </tr>';
                $no++;
                }
              ?>
            </tbody>
          </table>
        </div>
      </div>
  </div>

  <div class="ibox">
    <div class="ibox-head">
        <div class="ibox-title">Data Masuk (Instansi Baru)</div>
        <div class="ibox-tools">
            <a class="ibox-collapse"><i class="fa fa-minus"></i></a>
        </div>
    </div>
      <div class="ibox-body">
      <div style="overflow-x:auto;">
          <table class="table table-striped table-bordered table-hover" id="dataTables-data3" width="100%">
              <thead>
                <tr>
                    <th></th>
                    <th style="text-align: center;"></th>
                    <th style="text-align: center;"></th>
                    <th style="text-align: center;"></th>
                    <th style="text-align: center;"></th>
                    <th style="text-align: center;"></th>
                </tr>
                <tr>
                    <th style="text-align: center;">No</th>
                    <th style="text-align: center; width: 100%;">Instansi</th>
                    <th style="text-align: center;">Permintaan</th>
                    <th style="text-align: center;">Biaya</th>
                    <th style="text-align: center;">Detail</th>
                    <th style="text-align: center; min-width: 80px">Action</th>
                </tr>
              </thead>
              <tbody>
                <?php
                $no = 1;
                foreach ($requestUmum as $row){
                      echo '<tr>
                              <td style="text-align: center;">'.$no.'</td>
                              <td>'.$row->nama_instansi.'</td>
                              <td style="text-align: center;">'.$row->jumlah_permintaan.'</td>
                              <td style="text-align: center;">'.$row->estimasi_biaya.'</td>
                              <td style="text-align: center;"><a data-toggle="modal" data-nm="'.$row->nama_instansi.'" data-jenis="'.$row->jenis_instansi.'" data-ins="'.$row->tipe_instansi.'" data-status="'.$row->status.'" data-pic="'.$row->pic.'" data-wa="'.$row->wa_pic.'" data-permintaan="'.$row->jumlah_permintaan.'" data-pemenuhan="'.$row->jumlah_pemenuhan.'" data-surat="'.$row->surat_permohonan.'" data-alamat="'.$row->alamat_kirim.'" data-wilayah="'.$row->id_wilayah.'" data-biaya="'.$row->estimasi_biaya.'" data-target="#detailRequest" href="#detailRequest">Lihat<a/></td> 
                              <td style="text-align: center;">
                                  <button class="btn btn-success" type="button" data-id="'.$row->id_transaksi_kebutuhan.'" data-toggle="modal" data-target="#acceptUmum"><i class="fa fa-check"></i></button>
                              </td>
                            </tr>';
                  $no++;
                  }
                // <button class="btn btn-info" type="button" data-toggle="modal" data-target="#edit"><i class="fa fa-edit"></i></button>
                ?>
              </tbody>
          </table>
        </div>
      </div>
  </div>

  <div class="ibox">
    <div class="ibox-head">
        <div class="ibox-title">Data Masuk (Exist Instansi)</div>
        <div class="ibox-tools">
            <a class="ibox-collapse"><i class="fa fa-minus"></i></a>
        </div>
    </div>
      <div class="ibox-body">
      <div style="overflow-x:auto;">
          <table class="table table-striped table-bordered table-hover" id="dataTables-data2" width="100%">
              <thead>
                <tr>
                    <th></th>
                    <th style="text-align: center;"></th>
                    <th style="text-align: center;"></th>
                    <th style="text-align: center;"></th>
                    <th style="text-align: center;"></th>
                    <th style="text-align: center;"></th>
                </tr>
                <tr>
                    <th style="text-align: center;">No</th>
                    <th style="text-align: center; width: 100%;">Instansi</th>
                    <th style="text-align: center;">Permintaan</th>
                    <th style="text-align: center;">Biaya</th>
                    <th style="text-align: center;">Detail</th>
                    <th style="text-align: center; min-width: 80px">Action</th>
                </tr>
              </thead>
              <tbody>
                <?php
                $no = 1;
                foreach ($request as $row){
                      echo '<tr>
                              <td style="text-align: center;">'.$no.'</td>
                              <td>'.$row->nama_instansi.'</td>
                              <td style="text-align: center;">'.$row->jumlah_permintaan.'</td>
                              <td style="text-align: center;">'.$row->estimasi_biaya.'</td>
                              <td style="text-align: center;"><a data-toggle="modal" data-nm="'.$row->nama_instansi.'" data-jenis="'.$row->jenis_instansi.'" data-ins="'.$row->tipe_instansi.'" data-status="'.$row->status.'" data-pic="'.$row->pic.'" data-wa="'.$row->wa_pic.'" data-permintaan="'.$row->jumlah_permintaan.'" data-pemenuhan="'.$row->jumlah_pemenuhan.'" data-surat="'.$row->surat_permohonan.'" data-alamat="'.$row->alamat_kirim.'" data-wilayah="'.$row->id_wilayah.'" data-biaya="'.$row->estimasi_biaya.'" data-target="#detailRequest" href="#detailRequest">Lihat<a/></td> 
                              <td style="text-align: center;">
                                  <button class="btn btn-success" type="button" data-id="'.$row->id_transaksi_kebutuhan.'" data-toggle="modal" data-target="#accept"><i class="fa fa-check"></i></button>
                              </td>
                            </tr>';
                  $no++;
                  }
                  // <button class="btn btn-info" type="button" data-toggle="modal" data-target="#edit"><i class="fa fa-edit"></i></button>
                ?>
              </tbody>
          </table>
        </div>
      </div>
  </div>

  <!-- Detil modal -->
  <div class="modal fade edit" id="detail" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">

        <div class="modal-header">
          <h4 class="modal-title" id="myModalLabel">Detail Request</h4>
          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
            <table class="table table-bordered" style="width: 100%;">
                <thead>
                    <tr>
                        <td style="background-color: #013880; color: #fff; width:20%">Nama Instansi</td>
                        <td id="nm_detil"></td>
                    </tr>
                    <tr>
                        <td style="background-color: #013880; color: #fff;">Jenis</td>
                        <td id="jenis_detil"></td>
                    </tr>
                    <tr>    
                        <td style="background-color: #013880; color: #fff;">Instansi</td>
                        <td id="instansi_detil"></td>
                    </tr>
                    <tr>
                        <td style="background-color: #013880; color: #fff;">Provinsi</td>
                        <td id="provinsi_detil"></td>
                    </tr>
                    <tr>
                        <td style="background-color: #013880; color: #fff;">Kota</td>
                        <td id="kota_detil"></td>
                    </tr>
                    <tr>    
                        <td style="background-color: #013880; color: #fff;">Pic</td>
                        <td id="pic_detil"></td>
                    </tr>
                    <tr>    
                        <td style="background-color: #013880; color: #fff;">Whatsapp</td>
                        <td id="wa_detil"></td>
                    </tr>
                    <tr>    
                        <td style="background-color: #013880; color: #fff;">Jumlah Permintaan</td>
                        <td id="permintaan_detil"></td>
                    </tr>
                    <tr>
                        <td style="background-color: #013880; color: #fff;">Jumlah Pemenuhan</td>
                        <td id="pemenuhan_detil"></td>
                    </tr>
                    <tr>
                        <td style="background-color: #013880; color: #fff;">Estimasi Biaya</td>
                        <td id="biaya_detil"></td>
                    </tr>                    
                    <tr>
                        <td style="background-color: #013880; color: #fff;">Surat Permohonan</td>
                        <td id="permohonan_detil"></td>
                    </tr>
                    <tr>
                        <td style="background-color: #013880; color: #fff;">Alamat Kirim</td>
                        <td id="alamat_detil"></td>
                    </tr>
                    <tr>
                        <td style="background-color: #013880; color: #fff;">Status</td>
                        <td id="status_detil"></td>
                    </tr>
                </thead>
            </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

  <!-- Detil modal -->
  <div class="modal fade edit" id="detailRequest" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">

        <div class="modal-header">
          <h4 class="modal-title" id="myModalLabel">Detail Request</h4>
          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
            <table class="table table-bordered" style="width: 100%;">
                <thead>
                    <tr>
                        <td style="background-color: #013880; color: #fff; width:20%">Nama Instansi</td>
                        <td id="nm_detil"></td>
                    </tr>
                    <tr>
                        <td style="background-color: #013880; color: #fff;">Jenis</td>
                        <td id="jenis_detil"></td>
                    </tr>
                    <tr>    
                        <td style="background-color: #013880; color: #fff;">Instansi</td>
                        <td id="instansi_detil"></td>
                    </tr>
                    <tr>
                        <td style="background-color: #013880; color: #fff;">Provinsi</td>
                        <td id="provinsi_detil"></td>
                    </tr>
                    <tr>
                        <td style="background-color: #013880; color: #fff;">Kota</td>
                        <td id="kota_detil"></td>
                    </tr>
                    <tr>    
                        <td style="background-color: #013880; color: #fff;">Pic</td>
                        <td id="pic_detil"></td>
                    </tr>
                    <tr>    
                        <td style="background-color: #013880; color: #fff;">Whatsapp</td>
                        <td id="wa_detil"></td>
                    </tr>
                    <tr>    
                        <td style="background-color: #013880; color: #fff;">Jumlah Permintaan</td>
                        <td id="permintaan_detil"></td>
                    </tr>
                    <tr>
                        <td style="background-color: #013880; color: #fff;">Estimasi Biaya</td>
                        <td id="biaya_detil"></td>
                    </tr>                    
                    <tr>
                        <td style="background-color: #013880; color: #fff;">Surat Permohonan</td>
                        <td id="permohonan_detil"></td>
                    </tr>
                    <tr>
                        <td style="background-color: #013880; color: #fff;">Alamat Kirim</td>
                        <td id="alamat_detil"></td>
                    </tr>
                    <tr>
                        <td style="background-color: #013880; color: #fff;">Status</td>
                        <td id="status_detil"></td>
                    </tr>
                </thead>
            </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

  <!-- Edit modal uang -->
  <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title" id="myModalLabel">Edit</h4>
        </div>
        <div class="modal-body">
          <form id="editForm" class="form-horizontal" action="" method="POST" enctype="multipart/form-data">
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3">Jumlah Permintaan <span class="required">*</span>
                      </label>
                      <div class="col-md-12 col-sm-12">
                        <input type="number" min="1" id="jumlah_permintaan" name="jumlah_permintaan" value="" required="required" class="form-control ">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3">Jumlah Pemenuhan <span class="required">*</span></label>
                      <div class="col-md-12 col-sm-12">
                        <input class="form-control" type="number" min="0" id="jumlah_pemenuhan" name="jumlah_pemenuhan" required="required">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3">Estimasi Biaya <span class="required">*</span></label>
                      <div class="col-md-12 col-sm-12">
                        <input class="form-control" type="text" min="1" id="estimasi_biaya" name="estimasi_biaya" required="required">
                      </div>
                    </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
          </form>
        </div>
      </div>
    </div>
  </div>

  <!-- Accept modal -->
  <div class="modal fade delete" id="accept" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md">
      <div class="modal-content">

        <div class="modal-header">
          <h4 class="modal-title" id="myModalLabel">Konfirmasi</h4>
          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
          <p>Apa anda yakin ingin menerima request ini?</p>
        </div>
        <div class="modal-footer">
        <form id="acceptForm" class="form-horizontal form-label-left" action="" method="POST" enctype="multipart/form-data">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
          <button type="submit" class="btn btn-primary">Accept</button>
        </form>
        </div>
      </div>
    </div>
  </div>

  <!-- Accept modal -->
  <div class="modal fade delete" id="acceptUmum" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md">
      <div class="modal-content">

        <div class="modal-header">
          <h4 class="modal-title" id="myModalLabel">Konfirmasi</h4>
          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
          <p>Apa anda yakin ingin menerima request ini?</p>
        </div>
        <div class="modal-footer">
        <form id="acceptFormUmum" class="form-horizontal form-label-left" action="" method="POST" enctype="multipart/form-data">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
          <button type="submit" class="btn btn-primary">Accept</button>
        </form>
        </div>
      </div>
    </div>
  </div>

  <!-- Finish modal -->
  <div class="modal fade delete" id="finish" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md">
      <div class="modal-content">

        <div class="modal-header">
          <h4 class="modal-title" id="myModalLabel">Konfirmasi</h4>
          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
          <p>Apa anda yakin ingin menyelesaikan request ini?</p>
        </div>
        <div class="modal-footer">
        <form id="finishForm" class="form-horizontal form-label-left" action="" method="POST" enctype="multipart/form-data">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
          <button type="submit" class="btn btn-primary">Finish</button>
        </form>
        </div>
      </div>
    </div>
  </div>

  <!-- CORE PLUGINS-->
  <script src="<?php echo config_item('assets');?>vendors/jquery/dist/jquery.min.js" type="text/javascript"></script>

  <!-- DataTables -->
  <script>
      $('.ibox').toggleClass("collapsed-mode").children(".ibox-body").slideToggle(200);
      $(document).ready(function(){
          $("table[id^='dataTables-data']").DataTable({
              pageLength: 5,
              lengthMenu: [5, 10, 20, 50, 100],
              responsive: true,
              scrollCollapse: true,
              initComplete: function () {
                  this.api().columns('.select-filter').every( function (i) {
                      th = $('thead tr:eq(0) th:eq('+i+')')
                      var column = this;
                      var select = $('<select class="chosen-filter" data-placeholder="Filter"><option value=""></option></select>')
                      .appendTo( th.empty() )
                      .on( 'change', function() {
                          var val = $.fn.dataTable.util.escapeRegex(
                              $(this).val()
                              );

                          column
                          .search( val ? '^'+val+'$' : '', true, false )
                          .draw();
                      } );

                      column.data().unique().sort().each( function ( d, j ) {
                          select.append( '<option value="'+d+'">'+d+'</option>' )
                      } );

                      $('.chosen-filter').select2({
                          allowClear: true,
                          width: '100%',
                      });
                  } );
              },                
          });
      });
  </script>

  <!-- Modal -->
  <script>
      $(document).ready(function() {
          $('#detailRequest').on('show.bs.modal', function (event) {

              if ( event.relatedTarget != null) {
                  var div = $(event.relatedTarget)
              }

              var modal = $(this)
              let id = div.data('wilayah');
              $.ajax({
                url : '<?php echo base_url(). 'relawan/request/selectprovinsi'; ?>',
                data: {
                  "id": id
                },
                type: 'post',
                dataType: 'json',
                success: function( result )
                {
                  $.each( result, function(k, v) {              
                    modal.find('#provinsi_detil').empty();
                    modal.find('#provinsi_detil').append(v);
                    let id_prov = k;
                    // alert(id_prov);
                    $.ajax({
                      url : '<?php echo base_url(). 'relawan/instansi/selectkota'; ?>',
                      data: {
                        "id": id_prov
                      },
                      type: 'post',
                      dataType: 'json',
                      success: function( result )
                      {
                        $.each( result, function(k, v) {              
                          if(k == div.data('wilayah')){
                            modal.find('#kota_detil').empty();
                            modal.find('#kota_detil').append(v); 
                          }
                        });
                      },
                      error: function()
                      {
                        //handle errors
                        alert('Error...');
                      }
                    });                    
                  });
                },
                error: function()
                {
                  //handle errors
                  alert('Error...');
                }
              });
              modal.find('#nm_detil').empty();
              modal.find('#nm_detil').append(div.data('nm'));
              modal.find('#jenis_detil').empty();
              modal.find('#jenis_detil').append(div.data('jenis'));
              modal.find('#instansi_detil').empty();
              modal.find('#instansi_detil').append(div.data('ins'));
              modal.find('#pic_detil').empty();
              modal.find('#pic_detil').append(div.data('pic'));
              modal.find('#wa_detil').empty();
              modal.find('#wa_detil').append(div.data('wa'));
              modal.find('#permintaan_detil').empty();
              modal.find('#permintaan_detil').append(div.data('permintaan'));
              modal.find('#biaya_detil').empty();
              modal.find('#biaya_detil').append(div.data('biaya'));
              modal.find('#permohonan_detil').empty();
              modal.find('#permohonan_detil').append("<a href='" + div.data('surat')+ "'><span class='badge badge-primary badge-pill m-r-5 m-b-5'>Download</span></a>");
              modal.find('#alamat_detil').empty();
              modal.find('#alamat_detil').append(div.data('alamat'));
              modal.find('#status_detil').empty();
              if(div.data('status') == 'Waiting'){
                modal.find('#status_detil').append("<span class='badge badge-warning badge-pill m-r-5 m-b-5'>" + div.data('status')+ "</span>");
              } else  {
                modal.find('#status_detil').append("<span class='badge badge-success badge-pill m-r-5 m-b-5'>" + div.data('status')+ "</span>");
              }
          });        
          $('#detail').on('show.bs.modal', function (event) {

              if ( event.relatedTarget != null) {
                  var div = $(event.relatedTarget)
              }

              var modal = $(this)
              let id = div.data('wilayah');
              $.ajax({
                url : '<?php echo base_url(). 'relawan/request/selectprovinsi'; ?>',
                data: {
                  "id": id
                },
                type: 'post',
                dataType: 'json',
                success: function( result )
                {
                  $.each( result, function(k, v) {              
                    modal.find('#provinsi_detil').empty();
                    modal.find('#provinsi_detil').append(v);
                    let id_prov = k;
                    // alert(id_prov);
                    $.ajax({
                      url : '<?php echo base_url(). 'relawan/instansi/selectkota'; ?>',
                      data: {
                        "id": id_prov
                      },
                      type: 'post',
                      dataType: 'json',
                      success: function( result )
                      {
                        $.each( result, function(k, v) { 
                        // alert(div.data('wilayah'));
                          if(k == div.data('wilayah')){
                            modal.find('#kota_detil').empty();
                            modal.find('#kota_detil').append(v); 
                          }
                        });
                      },
                      error: function()
                      {
                        //handle errors
                        alert('Error...');
                      }
                    });                    
                  });
                },
                error: function()
                {
                  //handle errors
                  alert('Error...');
                }
              });
              modal.find('#nm_detil').empty();
              modal.find('#nm_detil').append(div.data('nm'));
              modal.find('#jenis_detil').empty();
              modal.find('#jenis_detil').append(div.data('jenis'));
              modal.find('#instansi_detil').empty();
              modal.find('#instansi_detil').append(div.data('ins'));
              modal.find('#pic_detil').empty();
              modal.find('#pic_detil').append(div.data('pic'));
              modal.find('#wa_detil').empty();
              modal.find('#wa_detil').append(div.data('wa'));
              modal.find('#permintaan_detil').empty();
              modal.find('#permintaan_detil').append(div.data('permintaan'));
              modal.find('#pemenuhan_detil').empty();
              modal.find('#pemenuhan_detil').append(div.data('pemenuhan'));
              modal.find('#biaya_detil').empty();
              modal.find('#biaya_detil').append(div.data('biaya'));
              modal.find('#permohonan_detil').empty();
              modal.find('#permohonan_detil').append("<a href='" + div.data('surat')+ "'><span class='badge badge-primary badge-pill m-r-5 m-b-5'>Download</span></a>");
              modal.find('#alamat_detil').empty();
              modal.find('#alamat_detil').append(div.data('alamat'));
              modal.find('#status_detil').empty();
              if(div.data('status') == 'Waiting'){
                modal.find('#status_detil').append("<span class='badge badge-warning badge-pill m-r-5 m-b-5'>" + div.data('status')+ "</span>");
              } else  {
                modal.find('#status_detil').append("<span class='badge badge-success badge-pill m-r-5 m-b-5'>" + div.data('status')+ "</span>");
              }
          });
          
          $('#accept').on('show.bs.modal', function (event) {
              var div = $(event.relatedTarget)
              var modal = $(this)
              modal.find('#acceptForm').attr("action", function(i, value) {
                return  "<?php echo site_url("relawan/request/update_status/") ?>" + div.data('id'); 
              });
          });

          $('#acceptUmum').on('show.bs.modal', function (event) {
              var div = $(event.relatedTarget)
              var modal = $(this)
              modal.find('#acceptFormUmum').attr("action", function(i, value) {
                return  "<?php echo site_url("relawan/request/update_umum/") ?>" + div.data('id'); 
              });
          });

          $('#finish').on('show.bs.modal', function (event) {
              var div = $(event.relatedTarget)
              var modal = $(this)
              modal.find('#finishForm').attr("action", function(i, value) {
                return  "<?php echo site_url("relawan/request/finish_status/") ?>" + div.data('id'); 
              });
          });

          $('#edit').on('show.bs.modal', function (event) {

              if ( event.relatedTarget != null) {
                  var div = $(event.relatedTarget)
              }

              var modal = $(this)

              modal.find('#jumlah_permintaan').attr("value",div.data('permintaan'));
              modal.find('#jumlah_pemenuhan').attr("value",div.data('pemenuhan'));
              modal.find('#estimasi_biaya').attr("value",div.data('biaya'));
              modal.find('#editForm').attr("action", function(i, value) { 
                return  "<?php echo site_url("relawan/request/edit/") ?>" + div.data('id');  });
          });
      });
  </script>
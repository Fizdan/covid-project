        <div class="ibox">
            <div class="ibox-head">
                <div class="ibox-title">Input Produksi</div>
                <!-- <button class="btn btn-primary pull-right" type="button" data-toggle="modal" data-target="#tambah"><i class="fa fa-plus"></i> Tambah</button> -->
            </div>
            <div class="ibox-body">
                <div class="row">
                  <div class="col-md-2">
                  </div>
                  <div class="col-md-8">
                    <div class="ibox">
                        <div class="ibox-body">
                          <form class="form-horizontal" action="<?php echo base_url('relawan/progres/produksiAdd') ?>" method="POST" >
                              <div class="form-group"><label class="control-label col-md-3 col-sm-3">Nama Barang <span class="required">*</span></label>
                                <select id="id_kebutuhan" name="id_kebutuhan" class="form-control select2-apply" placeholder="" required>
                                  <option value="1" selected>Face Shield</option>
                                </select>
                              </div>
                              <div class="form-group"><label class="control-label col-md-3 col-sm-3">Jumlah Produksi <span class="required">*</span></label>
                                      <input type="number" min="1" id="jumlah_produksi" name="jumlah_produksi"  class="form-control" placeholder="Jumlah Produksi" required>
                              </div>
                
                             <div class="form-group row">
                                  <div class="col-md-12 ml-md-auto">
                                      <button type="submit" class="btn btn-primary pull-right">Submit</button>
                                  </div>
                              </div>                              
                          </form>                                                         
                        </div>                             
                    </div>                            
                  </div>
                  <div class="col-md-2">
                  </div>
                </div>
          </div>
      </div>

  <!-- CORE PLUGINS-->
  <script src="<?php echo config_item('assets');?>vendors/jquery/dist/jquery.min.js" type="text/javascript"></script>

  <!-- Select2 -->
  <script>
      $(document).ready(function() {
          $('.select2-apply').select2({
              width: '100%'
          });
      });
  </script>
        <div class="ibox">
            <div class="ibox-head">
                <div class="ibox-title">Data Donatur Uang</div>
                <button class="btn btn-primary pull-right" type="button" data-toggle="modal" data-target="#tambahUang"><i class="fa fa-plus"></i> Tambah</button>
            </div>
            <div class="ibox-body">
              <div style="overflow-x:auto;">
                <table class="table table-striped table-bordered table-hover" id="dataTables-data" width="100%">
                    <thead>
                        <tr>
                            <th></th>
                            <th style="text-align: center;"></th>
                            <th style="text-align: center;"></th>
                            <th style="text-align: center;"></th>
                            <th style="text-align: center;"></th>
                            <th style="text-align: center;"></th>
                        </tr>
                        <tr>
                          <th style="text-align: center;">No</th>
                          <th style="text-align: center;" class="select-filter">Nama Donatur</th>
                          <th style="text-align: center;" class="select-filter">Jumlah Donasi</th>
                          <th style="text-align: center;" class="select-filter">Rekening Donatur</th>
                          <th style="text-align: center;" class="select-filter">Instansi</th>
                          <th style="text-align: center; min-width: 80px">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                      <?php
                      $no = 1;
                      foreach ($uangDonasi as $row)
                        {
                            echo '<tr align="center">
                                    <td style="text-align: center;">'.$no.'</td>
                                    <td>'.$row->nama_donatur_uang.'</td>
                                    <td>'.$row->jumlah_uang.'</td>
                                    <td>'.$row->rekening_donatur_uang.'</td>
                                    <td>'.$row->nama.'</td>
                                    <td align="center">
                                        <button class="btn btn-info" type="button" data-toggle="modal" data-target="#editUang" data-id="'.$row->id_uang.'" data-nama="'.$row->nama_donatur_uang.'" data-jumlah="'.$row->jumlah_uang.'" data-rek="'.$row->rekening_donatur_uang.'" data-ins="'.$row->instansi_donatur_uang.'"><i class="fa fa-edit"></i></button>
                                        <button class="btn btn-danger" type="button" data-id="'.$row->id_uang.'" data-toggle="modal" data-target="#deleteUang"><i class="fa fa-minus"></i></button>
                                    </td>
                                  </tr>';
                        $no++;
                        }
                      ?>
                    </tbody>
                </table>
              </div>
          </div>
        </div>

        <div class="ibox">
            <div class="ibox-head">
                <div class="ibox-title">Data Donatur Barang</div>
                <button class="btn btn-primary pull-right" type="button" data-toggle="modal" data-target="#tambahBrg"><i class="fa fa-plus"></i> Tambah</button>
            </div>
            <div class="ibox-body">
              <div style="overflow-x:auto;">
                <table class="table table-striped table-bordered table-hover" id="dataTables-data2" width="100%">
                    <thead>
                        <tr>
                            <th></th>
                            <th style="text-align: center;"></th>
                            <th style="text-align: center;"></th>
                            <th style="text-align: center;"></th>
                            <th style="text-align: center;"></th>
                            <th style="text-align: center;"></th>
                        </tr>
                        <tr>
                          <th style="text-align: center;">No</th>
                          <th style="text-align: center;" class="select-filter">Nama Barang</th>
                          <th style="text-align: center;" class="select-filter">Jumlah Barang</th>
                          <th style="text-align: center;" class="select-filter">Nama Donatur</th>
                          <th style="text-align: center;" class="select-filter">Instansi</th>
                          <th style="text-align: center; min-width: 80px">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                      <?php
                      $no = 1;
                      foreach ($barangDonasi as $row)
                        {
                            echo '<tr align="center">
                                    <td style="text-align: center;">'.$no.'</td>
                                    <td>'.$row->nama_barang_donasi.'</td>
                                    <td>'.$row->jumlah_barang_donasi.'</td>
                                    <td>'.$row->nama_donatur_barang_donasi.'</td>
                                    <td>'.$row->nama.'</td>
                                    <td align="center">
                                        <button class="btn btn-info" type="button" data-toggle="modal" data-target="#editBrg" data-id="'.$row->id_barang_donasi.'" data-nama="'.$row->nama_barang_donasi.'" data-jumlah="'.$row->jumlah_barang_donasi.'" data-rek="'.$row->nama_donatur_barang_donasi.'" data-ins="'.$row->instansi_donatur_barang_donasi.'"><i class="fa fa-edit"></i></button>
                                        <button class="btn btn-danger" type="button" data-id="'.$row->id_barang_donasi.'" data-toggle="modal" data-target="#deleteBrg"><i class="fa fa-minus"></i></button>
                                    </td>
                                  </tr>';
                        $no++;
                        }
                      ?>
                    </tbody>
                </table>
              </div>
          </div>
        </div>

  <!-- Tambah Uang Modal -->
  <div class="modal fade" id="tambahUang" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title" id="myModalLabel">Tambah</h4>
        </div>
        <div class="modal-body">
          <form id="tambahFormUang" class="form-horizontal" action="<?php echo site_url("relawan/donatur/addUang/") ?>" method="POST" enctype="multipart/form-data">
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3">Nama Donatur <span class="required">*</span>
                      </label>
                      <div class="col-md-12 col-sm-12">
                        <input type="text" id="nama_donatur_uang" name="nama_donatur_uang" value="" required="required" class="form-control ">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3">Jumlah Donasi <span class="required">*</span></label>
                      <div class="col-md-12 col-sm-12">
                        <input class="form-control" type="number" id="jumlah_uang" name="jumlah_uang" required="required">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3">Rekening Donatur <span class="required">*</span></label>
                      <div class="col-md-12 col-sm-12">
                        <input class="form-control" id="rekening_donatur_uang" name="rekening_donatur_uang" required="required">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3">Instansi <span class="required">*</span></label>
                      <div class="col-md-12 col-sm-12">
                        <select name="instansi_donatur_uang" id="instansi_donatur_uang" class="form-control select2_single">
                          <option selected="" value="" disabled="">-- Pilih Instansi</option>
                          <?php foreach ($getInstansi as $row){
                                echo "<option value='".$row->id_instansi."'>".$row->nama_instansi."</option>";
                          } ?>      
                        </select>
                      </div>
                    </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
          </form>
        </div>
      </div>
    </div>
  </div>

  <!-- Tambah Barang Modal -->
  <div class="modal fade" id="tambahBrg" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title" id="myModalLabel">Tambah</h4>
        </div>
        <div class="modal-body">
          <form id="tambahFormBrg" class="form-horizontal" action="<?php echo site_url("relawan/donatur/addBarang/") ?>" method="POST" enctype="multipart/form-data">
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3">Nama Barang <span class="required">*</span>
                      </label>
                      <div class="col-md-12 col-sm-12">
                        <input type="text" id="nama_barang_donasi" name="nama_barang_donasi" value="" required="required" class="form-control ">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3">Jumlah Barang <span class="required">*</span></label>
                      <div class="col-md-12 col-sm-12">
                        <input class="form-control" type="number" id="jumlah_barang_donasi" name="jumlah_barang_donasi" required="required">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3">Nama Donatur <span class="required">*</span></label>
                      <div class="col-md-12 col-sm-12">
                        <input class="form-control" id="nama_donatur_barang_donasi" name="nama_donatur_barang_donasi" required="required">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3">Instansi <span class="required">*</span></label>
                      <div class="col-md-12 col-sm-12">
                        <select name="instansi_donatur_barang_donasi" id="instansi_donatur_barang_donasi" class="form-control select2_single">
                          <option selected="" value="" disabled="">-- Pilih Instansi</option>
                          <?php foreach ($getInstansi as $row){
                                echo "<option value='".$row->id_instansi."'>".$row->nama_instansi."</option>";
                          } ?>      
                        </select>
                      </div>
                    </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
          </form>
        </div>
      </div>
    </div>
  </div>

  <!-- Edit modal uang -->
  <div class="modal fade" id="editUang" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title" id="myModalLabel">Edit</h4>
        </div>
        <div class="modal-body">
          <form id="editFormUang" class="form-horizontal" action="" method="POST" enctype="multipart/form-data">
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3">Nama Donatur <span class="required">*</span>
                      </label>
                      <div class="col-md-12 col-sm-12">
                        <input type="text" id="nama_donatur_uang" name="nama_donatur_uang" value="" required="required" class="form-control ">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3">Jumlah Donasi <span class="required">*</span></label>
                      <div class="col-md-12 col-sm-12">
                        <input class="form-control" type="number" id="jumlah_uang" name="jumlah_uang" required="required">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3">Rekening Donatur <span class="required">*</span></label>
                      <div class="col-md-12 col-sm-12">
                        <input class="form-control" id="rekening_donatur_uang" name="rekening_donatur_uang" required="required">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3">Instansi <span class="required">*</span></label>
                      <div class="col-md-12 col-sm-12">
                        <select name="instansi_donatur_uang" id="instansi_donatur_uang" class="form-control select2_single">
                          <option selected="" value="" disabled="">-- Pilih Instansi</option>
                          <?php foreach ($getInstansi as $row){
                                echo "<option value='".$row->id_instansi."'>".$row->nama_instansi."</option>";
                          } ?>      
                        </select>
                      </div>
                    </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
          </form>
        </div>
      </div>
    </div>
  </div>

  <!-- Edit modal brg -->
  <div class="modal fade" id="editBrg" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title" id="myModalLabel">Edit</h4>
        </div>
        <div class="modal-body">
          <form id="editFormBrg" class="form-horizontal" action="" method="POST" enctype="multipart/form-data">
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3">Nama Barang <span class="required">*</span>
                      </label>
                      <div class="col-md-12 col-sm-12">
                        <input type="text" id="nama_barang_donasi" name="nama_barang_donasi" value="" required="required" class="form-control ">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3">Jumlah Barang <span class="required">*</span></label>
                      <div class="col-md-12 col-sm-12">
                        <input class="form-control" type="number" id="jumlah_barang_donasi" name="jumlah_barang_donasi" required="required">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3">Nama Donatur <span class="required">*</span></label>
                      <div class="col-md-12 col-sm-12">
                        <input class="form-control" id="nama_donatur_barang_donasi" name="nama_donatur_barang_donasi" required="required">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3">Instansi <span class="required">*</span></label>
                      <div class="col-md-12 col-sm-12">
                        <select name="instansi_donatur_barang_donasi" id="instansi_donatur_barang_donasi" class="form-control select2_single">
                          <option selected="" value="" disabled="">-- Pilih Instansi</option>
                          <?php foreach ($getInstansi as $row){
                                echo "<option value='".$row->id_instansi."'>".$row->nama_instansi."</option>";
                          } ?>      
                        </select>
                      </div>
                    </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
          </form>
        </div>
      </div>
    </div>
  </div>

  <!-- Delete modal uang -->
  <div class="modal fade" id="deleteUang" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md">
      <div class="modal-content">

        <div class="modal-header">
          <h4 class="modal-title" id="myModalLabel">Delete</h4>
          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
          <p>Apa anda yakin ingin menghapus data ini?</p>
        </div>
        <div class="modal-footer">
          <form id="delFormUang" class="form-horizontal form-label-left" action="" method="POST" enctype="multipart/form-data">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-danger">Delete</button>
          </form>
        </div>

      </div>
    </div>
  </div>

  <!-- Delete modal brg -->
  <div class="modal fade" id="deleteBrg" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md">
      <div class="modal-content">

        <div class="modal-header">
          <h4 class="modal-title" id="myModalLabel">Delete</h4>
          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
          <p>Apa anda yakin ingin menghapus data ini?</p>
        </div>
        <div class="modal-footer">
          <form id="delFormBrg" class="form-horizontal form-label-left" action="" method="POST" enctype="multipart/form-data">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-danger">Delete</button>
          </form>
        </div>

      </div>
    </div>
  </div>

  <!-- CORE PLUGINS-->
  <script src="<?php echo config_item('assets');?>vendors/jquery/dist/jquery.min.js" type="text/javascript"></script>

  <!-- Datatables -->
  <script>
      $(document).ready(function(){    
          $("table[id^='dataTables-data']").DataTable({
              pageLength: 10,
              responsive: true,
              scrollCollapse: true           
          });
      });
  </script>

  <script>
    $(document).ready(function(){
          $('#editBrg').on('show.bs.modal', function (event) {

              if ( event.relatedTarget != null) {
                  var div = $(event.relatedTarget)
              }

              var modal = $(this)

              modal.find('#nama_barang_donasi').attr("value",div.data('nama'));
              modal.find('#jumlah_barang_donasi').attr("value",div.data('jumlah'));
              modal.find('#nama_donatur_barang_donasi').attr("value",div.data('rek'));
              modal.find('#instansi_donatur_barang_donasi option[value="'+div.data('ins')+'"]').attr("selected", true);
              modal.find('#editFormBrg').attr("action", function(i, value) { return  "<?php echo site_url("relawan/donatur/editBarang/") ?>" + div.data('id'); });
          });
          $('#editBrg').on('hidden.bs.modal', function (event) {
              var div = $(event.relatedTarget)
              var modal = $(this);
              modal.find('#instansi_donatur_barang_donasi option').attr('selected', false);
          });

          $('#editUang').on('show.bs.modal', function (event) {

              if ( event.relatedTarget != null) {
                  var div = $(event.relatedTarget)
              }

              var modal = $(this)

              modal.find('#nama_donatur_uang').attr("value",div.data('nama'));
              modal.find('#jumlah_uang').attr("value",div.data('jumlah'));
              modal.find('#rekening_donatur_uang').attr("value",div.data('rek'));
              modal.find('#instansi_donatur_uang option[value="'+div.data('ins')+'"]').attr("selected", true);
              modal.find('#editFormUang').attr("action", function(i, value) { return  "<?php echo site_url("relawan/donatur/editUang/") ?>" + div.data('id'); });
          });
          $('#editUang').on('hidden.bs.modal', function (event) {
              var div = $(event.relatedTarget)
              var modal = $(this);
              modal.find('#instansi_donatur_uang option').attr('selected', false);
          });

          $('#deleteBrg').on('show.bs.modal', function (event) {
              var div = $(event.relatedTarget)
              var modal = $(this)
              modal.find('#delFormBrg').attr("action", function(i, value) {
                return  "<?php echo site_url("relawan/donatur/deleteBarang/") ?>" + div.data('id'); 
              });
          });
          $('#deleteUang').on('show.bs.modal', function (event) {
              var div = $(event.relatedTarget)
              var modal = $(this)
              modal.find('#delFormUang').attr("action", function(i, value) {
                return  "<?php echo site_url("relawan/donatur/deleteUang/") ?>" + div.data('id'); 
              });
          });
    });
  </script>
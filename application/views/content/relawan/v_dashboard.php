            <!-- START PAGE CONTENT-->
            <div class="ibox pl-3 pr-3">
            <div class="page-content fade-in-up">
                <div class="row">
                    <div class="col-lg-3 col-md-6">
                        <div class="ibox bg-info color-white widget-stat">
                            <div class="ibox-body">
                                <h2 class="m-b-5 font-strong"><?= $total_produksi ?></h2>
                                <div class="m-b-5">Total Produksi</div><i class="ti-bar-chart widget-stat-icon"></i>
                                <div><small><?= $total_produksi_perday ?> produksi hari ini</small></div>
                            </div>
                        </div>
                    </div>                    
                    <div class="col-lg-3 col-md-6">
                        <div class="ibox bg-success color-white widget-stat">
                            <div class="ibox-body">
                                <h2 class="m-b-5 font-strong"><?= $total_distribusi ?></h2>
                                <div class="m-b-5">Total Distribusi</div><i class="ti-shopping-cart widget-stat-icon"></i>
                                <div><small>Jumlah distribusi keseluruhan</small></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="ibox bg-warning color-white widget-stat">
                            <div class="ibox-body">
                                <h2 class="m-b-5 font-strong"><?= $total_distribusi_perday ?></h2>
                                <div class="m-b-5">Distribusi Hari Ini</div><i class="ti-shopping-cart widget-stat-icon"></i>
                                <div></i><small>Total distribusi hari ini</small></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="ibox bg-danger color-white widget-stat">
                            <div class="ibox-body">
                                <h2 class="m-b-5 font-strong"><?= $total_sisa ?></h2>
                                <div class="m-b-5">Sisa Produksi</div><i class="ti-bar-chart widget-stat-icon"></i>
                                <div><small>Sisa produksi saat ini</small></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="ibox">
        <div class="ibox-body">
            <div class="flexbox mb-4">
                <div>
                    <h3 class="m-0">Statistics</h3>
                    <div>Your shop sales analytics</div>
                </div>
                <div class="d-inline-flex">
                    <div class="px-3" style="border-right: 1px solid rgba(0,0,0,.1);">
                        <div class="text-muted">WEEKLY INCOME</div>
                        <div>
                            <span class="h2 m-0">$850</span>
                            <span class="text-success ml-2"><i class="fa fa-level-up"></i> +25%</span>
                        </div>
                    </div>
                    <div class="px-3">
                        <div class="text-muted">WEEKLY SALES</div>
                        <div>
                            <span class="h2 m-0">240</span>
                            <span class="text-warning ml-2"><i class="fa fa-level-down"></i> -12%</span>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <canvas id="bar_chart" style="height:260px;"></canvas>
            </div>
        </div>

    <!-- PAGE LEVEL PLUGINS-->
    <script src="<?php echo config_item('assets');?>vendors/jquery/dist/jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo config_item('assets');?>js/scripts/dashboard_1_demo.js" type="text/javascript"></script>
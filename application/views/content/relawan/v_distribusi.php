        <div class="ibox">
            <div class="ibox-head">
                <div class="ibox-title">Input Distribusi</div>
                <!-- <button class="btn btn-primary pull-right" type="button" data-toggle="modal" data-target="#tambah"><i class="fa fa-plus"></i> Tambah</button> -->
            </div>
            <div class="ibox-body">
                <div class="row">
                  <div class="col-md-2">
                  </div>
                  <div class="col-md-8">
                    <div class="ibox">
                        <div class="ibox-body">
                          <form class="form-horizontal" action="<?php echo base_url('relawan/progres/distribusiAdd') ?>" method="POST" >
                              <div class="form-group"><label class="control-label col-md-3 col-sm-3">Request Instansi <span class="required">*</span></label>
                                <select id="id_transaksi_kebutuhan" name="id_transaksi_kebutuhan" class="form-control select2-apply" placeholder="" required>
                                  <option selected disabled>Pilih Request</option>

                                  <?php
                                  foreach($request as $idx){
                                  
                                  echo '<option value="'.$idx->id_transaksi_kebutuhan.'">'.$idx->provinsi.' | '.$idx->kota.' | '.$idx->nama_instansi.' | '.$idx->pic.' </option>';
                                  }
                                  ?>
                                </select>
                              </div>
                              <div class="form-group"><label class="control-label col-md-3 col-sm-3">Jumlah Distribusi <span class="required">*</span></label>
                                      <input type="number" min="1" id="jumlah_distribusi" name="jumlah_distribusi"  class="form-control" placeholder="Jumlah Distribusi" required>
                              </div>
                              <div class="form-group"><label class="control-label col-md-3 col-sm-3">Kurir Pengiriman <span class="required">*</span></label>
                                <select id="kurir_pengiriman" name="kurir_pengiriman" class="form-control select2-apply" placeholder="" required>
                                  <option value="JNE">JNE</option>
                                  <option value="POS Indonesia" selected>POS Indonesia</option>
                                  <option value="Tiki">Tiki</option>
                                </select>
                              </div>
                              <div class="form-group"><label class="control-label col-md-3 col-sm-3">Resi Pengiriman</label>
                                      <input type="text" id="resi_pengiriman" name="resi_pengiriman"  class="form-control" placeholder="Nomor Resi">
                              </div>
                             <div class="form-group row">
                                  <div class="col-md-12 ml-md-auto">
                                      <button type="submit" class="btn btn-primary pull-right">Submit</button>
                                  </div>
                              </div>                              
                          </form>                                                         
                        </div>                             
                    </div>                            
                  </div>
                  <div class="col-md-2">
                  </div>
                </div>
          </div>
      </div>

  <!-- CORE PLUGINS-->
  <script src="<?php echo config_item('assets');?>vendors/jquery/dist/jquery.min.js" type="text/javascript"></script>

  <!-- Select2 -->
  <script>
      $(document).ready(function() {
          $('.select2-apply').select2({
              width: '100%'
          });
      });
  </script>
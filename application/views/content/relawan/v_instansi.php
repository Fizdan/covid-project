        <div class="ibox">
            <div class="ibox-head">
                <div class="ibox-title">Data Instansi</div>
                <button class="btn btn-primary pull-right" type="button" data-toggle="modal" data-target="#tambah"><i class="fa fa-plus"></i> Tambah</button>
            </div>
            <div class="ibox-body">
              <div style="overflow-x:auto;">
                <table class="table table-striped table-bordered table-hover" id="dataTables-data" width="100%">
                    <thead>
                        <tr>
                            <th></th>
                            <th style="text-align: center;"></th>
                            <th style="text-align: center;"></th>
                            <th style="text-align: center;"></th>
                            <th style="text-align: center;"></th>
                            <th style="text-align: center;"></th>
                            <th style="text-align: center;"></th>
                        </tr>
                        <tr>
                          <th style="text-align: center;">No</th>
                          <th style="text-align: center; width: 100%;" class="select-filter">Nama</th>
                          <th style="text-align: center;" class="select-filter">Jenis</th>
                          <th style="text-align: center;" class="select-filter">Instansi</th>
                          <th style="text-align: center;" class="select-filter">Provinsi</th>
                          <th style="text-align: center;" class="select-filter">Kota</th>
                          <th style="text-align: center; min-width: 80px">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                      <?php
                      $no = 1;
                      foreach ($instansi as $row)
                        {
                            echo '<tr>
                                    <td style="text-align: center;">'.$no.'</td>
                                    <td>'.$row->nama_instansi.'</td>
                                    <td>'.$row->jenis_instansi.'</td>
                                    <td>'.$row->tipe_instansi.'</td>
                                    <td>'.$row->provinsi.'</td>
                                    <td>'.$row->kota.'</td>
                                    <td align="center">
                                        <button class="btn btn-info" type="button" data-toggle="modal" data-target="#edit" data-id="'.$row->id_instansi.'" data-nama="'.$row->nama_instansi.'" data-jenis="'.$row->jenis_instansi.'" data-tipe="'.$row->id_tipe.'" data-wilayah="'.$row->id_wilayah.'"><i class="fa fa-edit"></i></button>
                                        <button class="btn btn-danger" type="button" data-id="'.$row->id_instansi.'" data-toggle="modal" data-target="#delete"><i class="fa fa-minus"></i></button>
                                    </td>
                                  </tr>';
                        $no++;
                        }
                      ?>
                    </tbody>
                </table>
              </div>
          </div>
      </div>

  <!-- Tambah modal -->
  <div class="modal fade" id="tambah" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title" id="myModalLabel">Tambah</h4>
        </div>
        <div class="modal-body">
          <form id="editForm" class="form-horizontal" action="<?php echo site_url("relawan/instansi/add/") ?>" method="POST" enctype="multipart/form-data">
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3">Nama Instansi <span class="required">*</span>
                      </label>
                      <div class="col-md-12 col-sm-12">
                        <input type="text" id="nama" name="nama" value="" required="required" class="form-control ">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3" for="last-name">Jenis Instansi <span class="required">*</span>
                      </label>
                      <div class="col-md-12 col-sm-12">
                        <select name="jenis" id="jenis" class="form-control select2_single" required>
                          <option selected="" disabled="">-- Pilih Jenis Instansi</option>
                          <option value="Pemerintah">Pemerintah</option>
                          <option value="Swasta">Swasta</option>
                        </select>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3">Tipe Instansi <span class="required">*</span></label>
                      <div class="col-md-12 col-sm-12">
                        <select name="tipe" id="tipe" class="form-control select2_single" required>
                          <option selected="" disabled="">-- Pilih Tipe Instansi</option>
                          <?php foreach ($getTipe as $row){
                                echo "<option value='".$row->id_tipe."'>".$row->tipe_nama."</option>";
                          } ?>      
                        </select>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3">Provinsi <span class="required">*</span></label>
                      <div class="col-md-12 col-sm-12">
                       <select name="provinsi" id="provinsi" class="form-control apply" required>
                          <option selected="" disabled="">-- Pilih Provinsi</option>
                          <?php foreach ($getProvinsi as $row){
                                echo "<option value='".$row->id_wilayah."'>".$row->nama_wilayah."</option>";
                          } ?>             
                        </select>
                      </div>
                    </div>
                    <div class="form-group" id="kota_menu" style="display:none;">
                      <label class="control-label col-md-3 col-sm-3">Kota <span class="required">*</span></label>
                      <div class="col-md-12 col-sm-12">
                       <select name="id_wilayah" id="id_wilayah" class="form-control apply" required>
                          <option selected="" disabled="">-- Pilih Kota</option>          
                        </select>
                      </div>
                    </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
          </form>
        </div>
      </div>
    </div>
  </div>

  <!-- Edit modal -->
  <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title" id="myModalLabel">Edit</h4>
        </div>
        <div class="modal-body">
          <form id="editForm" class="form-horizontal" action="" method="POST" enctype="multipart/form-data">
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3">Nama Instansi <span class="required">*</span>
                      </label>
                      <div class="col-md-12 col-sm-12">
                        <input type="text" id="nama" name="nama" value="" required="required" class="form-control ">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3" for="last-name">Jenis Instansi <span class="required">*</span>
                      </label>
                      <div class="col-md-12 col-sm-12">
                        <select name="jenis" id="jenis" class="form-control select2_single" required>
                          <option value="Pemerintah">Pemerintah</option>
                          <option value="Swasta">Swasta</option>
                        </select>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3">Tipe Instansi <span class="required">*</span></label>
                      <div class="col-md-12 col-sm-12">
                        <select name="tipe" id="tipe" class="form-control select2_single" required>
                          <?php foreach ($getTipe as $row){
                                echo "<option value='".$row->id_tipe."'>".$row->tipe_nama."</option>";
                          } ?>
                        </select>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3">Provinsi <span class="required">*</span></label>
                      <div class="col-md-12 col-sm-12">
                       <select name="provinsi" id="provinsiedit" class="form-control apply" required>
                          <option selected="" disabled="">-- Pilih Provinsi</option>
                          <?php foreach ($getProvinsi as $row){
                                echo "<option value='".$row->id_wilayah."'>".$row->nama_wilayah."</option>";
                          } ?>             
                        </select>
                      </div>
                    </div>
                    <div class="form-group" id="kota_menu">
                      <label class="control-label col-md-3 col-sm-3">Kota <span class="required">*</span></label>
                      <div class="col-md-12 col-sm-12">
                       <select name="id_wilayah" id="id_wilayahedit" class="form-control apply" required>
                          <option selected="" disabled="">-- Pilih Kota</option>          
                        </select>
                      </div>
                    </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
          </form>
        </div>
      </div>
    </div>
  </div>

  <!-- Delete modal -->
  <div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md">
      <div class="modal-content">

        <div class="modal-header">
          <h4 class="modal-title" id="myModalLabel">Delete</h4>
          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
          <p>Apa anda yakin ingin menghapus data ini?</p>
        </div>
        <div class="modal-footer">
          <form id="delForm" class="form-horizontal form-label-left" action="" method="POST" enctype="multipart/form-data">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-danger">Delete</button>
          </form>
        </div>

      </div>
    </div>
  </div>

  <!-- CORE PLUGINS-->
  <script src="<?php echo config_item('assets');?>vendors/jquery/dist/jquery.min.js" type="text/javascript"></script>

  <!-- Datatables -->
  <script>
      $(document).ready(function(){
          $('#dataTables-data').DataTable({
              pageLength: 10,
              responsive: true,
              initComplete: function () {
                  this.api().columns('.select-filter').every( function (i) {
                      th = $('thead tr:eq(0) th:eq('+i+')')
                      var column = this;
                      var select = $('<select class="chosen-filter" data-placeholder="Filter"><option value=""></option></select>')
                      .appendTo( th.empty() )
                      .on( 'change', function() {
                          var val = $.fn.dataTable.util.escapeRegex(
                              $(this).val()
                              );

                          column
                          .search( val ? '^'+val+'$' : '', true, false )
                          .draw();
                      } );

                      column.data().unique().sort().each( function ( d, j ) {
                          select.append( '<option value="'+d+'">'+d+'</option>' )
                      } );

                      $('.chosen-filter').select2({
                          allowClear: true,
                          width: '100%',
                      });
                  } );
              },                
          });
      });
  </script>

  <!-- Modal -->
  <script>
      $(document).ready(function() {
          $('#edit').on('show.bs.modal', function (event) {

              if ( event.relatedTarget != null) {
                  var div = $(event.relatedTarget)
              }

              var modal = $(this)

              modal.find('#nama').attr("value",div.data('nama'));
              modal.find('#limit').attr("value",div.data('limit'));
              modal.find('#jenis option[value="'+div.data('jenis')+'"]').attr("selected", true);
              modal.find('#tipe option[value="'+div.data('tipe')+'"]').attr("selected", true);

              let id = div.data('wilayah');
              $.ajax({
                url : '<?php echo base_url(). 'relawan/instansi/selectprovinsi'; ?>',
                data: {
                  "id": id
                },
                type: 'post',
                dataType: 'json',
                success: function( result )
                {
                  $.each( result, function(k, v) {
                    modal.find('#provinsiedit option[value="'+ k +'"]').attr("selected", true);
                    let id_prov = k;
                    // alert(id_prov);
                    $.ajax({
                      url : '<?php echo base_url(). 'relawan/instansi/selectkota'; ?>',
                      data: {
                        "id": id_prov
                      },
                      type: 'post',
                      dataType: 'json',
                      success: function( result )
                      {
                        $.each( result, function(k, v) {
                          modal.find('#id_wilayahedit option[value="'+ div.data('wilayah') +'"]').attr("selected", true);
                          $('#id_wilayahedit').append($('<option>', {value:k, text:v}));
                        });
                      },
                      error: function()
                      {
                        //handle errors
                        alert('Error...');
                      }
                    });                    
                  });
                },
                error: function()
                {
                  //handle errors
                  alert('Error...');
                }
              });
              // modal.find('#id_wilayah option[value="'+div.data('wilayah')+'"]').attr("selected", true);
              modal.find('#editForm').attr("action", function(i, value) { 
                return  "<?php echo site_url("relawan/instansi/edit/") ?>" + div.data('id'); 
              });
          });

        $('#edit').on('hidden.bs.modal', function (event) {
            var div = $(event.relatedTarget)
            var modal = $(this);
            modal.find('#jenis option').attr('selected', false);
            modal.find('#tipe option').attr('selected', false);
            modal.find('#provinsi option').attr('selected', false);
            modal.find('#id_wilayah option').attr('selected', false);
            modal.find('#provinsiedit option').attr('selected', false);
            modal.find('#id_wilayahedit option').attr('selected', false);
        });
          
          $('#delete').on('show.bs.modal', function (event) {
              var div = $(event.relatedTarget)
              var modal = $(this)
              modal.find('#delForm').attr("action", function(i, value) {
                return  "<?php echo site_url("relawan/instansi/delete/") ?>" + div.data('id'); 
              });
          });
      });
  </script>

  <!-- Ajax -->
  <script>
    // AJAX For Tambah Modal
    $('#provinsi').change(function(){
      $("#id_wilayah option").remove();
      $('#id_wilayah').append($('<option selected="" disabled="">-- Pilih Kota</option> '));
      $("#kota_menu").hide();
      let id = $(this).val();
      $.ajax({
        url : '<?php echo base_url(). 'relawan/instansi/selectkota'; ?>',
        data: {
          "id": id
        },
        type: 'post',
        dataType: 'json',
        success: function( result )
        {
          $.each( result, function(k, v) {
            $('#id_wilayah').append($('<option>', {value:k, text:v}));
            $("#kota_menu").show();
          });
        },
        error: function()
        {
          //handle errors
          alert('Error...');
        }
      });
    });

    //AJAX For Edit Modal
    $('#provinsiedit').change(function(){
      $("#id_wilayahedit option").remove();
      $('#id_wilayahedit').append($('<option selected="" disabled="">-- Pilih Kota</option> '));
      let id = $(this).val();
      $.ajax({
        url : '<?php echo base_url(). 'relawan/instansi/selectkota'; ?>',
        data: {
          "id": id
        },
        type: 'post',
        dataType: 'json',
        success: function( result )
        {
          $.each( result, function(k, v) {
            $('#id_wilayahedit').append($('<option>', {value:k, text:v}));
          });
        },
        error: function()
        {
          //handle errors
          alert('Error...');
        }
      });
    });
  </script>


  <!-- Select2 -->
  <script>
      $(document).ready(function() {
          $('.apply-test').select2({
              width: '100%'
          });
      });
  </script>
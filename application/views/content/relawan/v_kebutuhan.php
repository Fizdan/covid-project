        <div class="ibox">
            <div class="ibox-head">
                <div class="ibox-title">Data Kebutuhan</div>
                <!-- <button class="btn btn-primary pull-right" type="button" data-toggle="modal" data-target="#tambah"><i class="fa fa-plus"></i> Tambah</button> -->
            </div>
            <div class="ibox-body">
              <div style="overflow-x:auto;">
                <table class="table table-striped table-bordered table-hover" id="dataTables-data" width="100%">
                    <thead>
                        <tr>
                            <th></th>
                            <th style="text-align: center;"></th>
                            <th style="text-align: center;"></th>
                            <th style="text-align: center;"></th>
                            <th style="text-align: center;"></th>
                        </tr>
                        <tr>
                          <th style="text-align: center;">No</th>
                          <th style="text-align: center;">Nama Kebutuhan</th>
                          <th style="text-align: center;">Tersedia</th>
                          <th style="text-align: center;">Biaya Produksi</th>
                          <th style="text-align: center; min-width: 80px">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                      <?php
                      $no = 1;
                      foreach ($kebutuhan as $row)
                        {
                            echo '<tr align="center">
                                    <td style="text-align: center;">'.$no.'</td>
                                    <td>'.$row->nama_kebutuhan.'</td>
                                    <td>'.$row->tersedia.'</td>
                                    <td>'.$row->biaya_produksi_kebutuhan.'</td>
                                    <td align="center">
                                        <button class="btn btn-info" type="button" data-toggle="modal" data-target="#edit" data-id="'.$row->id_kebutuhan.'" data-nama="'.$row->nama_kebutuhan.'" data-tersedia="'.$row->tersedia.'" data-biaya="'.$row->biaya_produksi_kebutuhan.'"><i class="fa fa-edit"></i></button>
                                    </td>
                                  </tr>';
                        $no++;
                        }
                        // <button class="btn btn-danger" type="button" data-id="'.$row->id_kebutuhan.'" data-toggle="modal" data-target="#delete"><i class="fa fa-minus"></i></button>
                      ?>
                    </tbody>
                </table>
              </div>
          </div>
      </div>

  <!-- Edit modal -->
  <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title" id="myModalLabel">Edit</h4>
        </div>
        <div class="modal-body">
          <form id="editForm" class="form-horizontal" action="" method="POST" enctype="multipart/form-data">
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3">Nama Kebutuhan <span class="required">*</span>
                      </label>
                      <div class="col-md-12 col-sm-12">
                        <input type="text" id="nama" name="nama_kebutuhan" value="" required="required" class="form-control ">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3">Tersedia <span class="required">*</span></label>
                      <div class="col-md-12 col-sm-12">
                        <input type="number" class="form-control" id="tersedia" name="tersedia" required="required">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3">Biaya Produksi <span class="required">*</span></label>
                      <div class="col-md-12 col-sm-12">
                        <input class="form-control" id="biaya" name="biaya_produksi_kebutuhan" required="required">
                      </div>
                    </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
          </form>
        </div>
      </div>
    </div>
  </div>

  <!-- CORE PLUGINS-->
  <script src="<?php echo config_item('assets');?>vendors/jquery/dist/jquery.min.js" type="text/javascript"></script>

  <!-- Datatables -->
  <script>
      $(document).ready(function(){
          $('#dataTables-data').DataTable({
              pageLength: 10,
              responsive: true,
              initComplete: function () {
                  this.api().columns('.select-filter').every( function (i) {
                      th = $('thead tr:eq(0) th:eq('+i+')')
                      var column = this;
                      var select = $('<select class="chosen-filter" data-placeholder="Filter"><option value=""></option></select>')
                      .appendTo( th.empty() )
                      .on( 'change', function() {
                          var val = $.fn.dataTable.util.escapeRegex(
                              $(this).val()
                              );

                          column
                          .search( val ? '^'+val+'$' : '', true, false )
                          .draw();
                      } );

                      column.data().unique().sort().each( function ( d, j ) {
                          select.append( '<option value="'+d+'">'+d+'</option>' )
                      } );

                      $('.chosen-filter').select2({
                          allowClear: true,
                      });
                  } );
              },                
          });
      });
  </script>

  <!-- Modal -->
  <script>
      $(document).ready(function() {
          $('#edit').on('show.bs.modal', function (event) {

              if ( event.relatedTarget != null) {
                  var div = $(event.relatedTarget)
              }

              var modal = $(this)

              modal.find('#nama').attr("value",div.data('nama'));
              modal.find('#tersedia').attr("value",div.data('tersedia'));
              modal.find('#biaya').attr("value",div.data('biaya'));
              modal.find('#editForm').attr("action", function(i, value) { return  "<?php echo site_url("relawan/kebutuhan/edit/") ?>" + div.data('id'); });
          });

          // $('#delete').on('show.bs.modal', function (event) {
          //     var div = $(event.relatedTarget)
          //     var modal = $(this)
          //     modal.find('#delForm').attr("action", function(i, value) {
          //       return  "<?php echo site_url("relawan/instansi/delete/") ?>" + div.data('id'); 
          //     });
          // });
      });
  </script>
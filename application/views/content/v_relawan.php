<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo config_item('assets');?>material-kit/img/apple-icon.png">
    <link rel="icon" type="image/png" href="<?php echo config_item('assets');?>material-kit/img/favicon.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title><?php echo $title;?></title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />

    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />

    <!-- CSS Files -->
    <link href="<?php echo config_item('assets');?>material-kit/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo config_item('assets');?>material-kit/css/material-kit.css" rel="stylesheet"/>

</head>


<body class="signup-page">
    <nav class="navbar navbar-primary navbar-fixed-top" id="sectionsNav">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <img style="height:50px;" src="<?php echo config_item('assets');?>material-kit/img/logo.png">
            </div>

            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-left">
                    <li>
                        <a href="<?php echo base_url() ?>">
                            <b>Beranda</b>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo base_url('request') ?>">
                            <b>Request APD</b>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo base_url('relawan') ?>">
                            <b>Daftar Relawan</b>
                        </a>
                    </li>               
                </ul>
                
                <ul class="nav navbar-nav navbar-right">            
                    <li>
                        <a href="<?php echo base_url('login');?>">
                            <b>Masuk</b>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    
    <div class="page-header header-filter" style="background-image: url('<?php echo config_item('assets');?>material-kit/img/header.jpg'); background-size: cover; background-position: top center;">
        <div class="container">
            <div class="row">
                <div class="section section-basic">

                    <div class="card card-signup">
                        <h2 class="card-title text-center">Register</h2>
                        <div class="row">
                            <div class="col-md-5 col-md-offset-1">
                                <div class="info info-horizontal">
                                    <div class="icon icon-rose">
                                        <i class="material-icons">timeline</i>
                                    </div>
                                    <div class="description">
                                        <h4 class="info-title">Marketing</h4>
                                        <p class="description">
                                            We've created the marketing campaign of the website. It was a very interesting collaboration.
                                        </p>
                                    </div>
                                </div>

                                <div class="info info-horizontal">
                                    <div class="icon icon-primary">
                                        <i class="material-icons">code</i>
                                    </div>
                                    <div class="description">
                                        <h4 class="info-title">Fully Coded in HTML5</h4>
                                        <p class="description">
                                            We've developed the website with HTML5 and CSS3. The client has access to the code using GitHub.
                                        </p>
                                    </div>
                                </div>

                                <div class="info info-horizontal">
                                    <div class="icon icon-info">
                                        <i class="material-icons">group</i>
                                    </div>
                                    <div class="description">
                                        <h4 class="info-title">Built Audience</h4>
                                        <p class="description">
                                            There is also a Fully Customizable CMS Admin Dashboard for this product.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="social text-center">
                                    <button class="btn btn-just-icon btn-round btn-twitter">
                                        <i class="fa fa-twitter"></i>
                                    </button>
                                    <button class="btn btn-just-icon btn-round btn-dribbble">
                                        <i class="fa fa-dribbble"></i>
                                    </button>
                                    <button class="btn btn-just-icon btn-round btn-facebook">
                                        <i class="fa fa-facebook"> </i>
                                    </button>
                                    <h4> or be classical </h4>
                                </div>

                                <form class="form" method="" action="">
                                    <div class="card-content">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">face</i>
                                            </span>
                                            <input type="text" class="form-control" placeholder="First Name...">
                                        </div>

                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">email</i>
                                            </span>
                                            <input type="text" class="form-control" placeholder="Email...">
                                        </div>

                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">lock_outline</i>
                                            </span>
                                            <input type="password" placeholder="Password..." class="form-control" />
                                        </div>

                                        <!-- If you want to add a checkbox to this form, uncomment this code -->

                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="optionsCheckboxes" checked>
                                                I agree to the <a href="#something">terms and conditions</a>.
                                            </label>
                                        </div>
                                    </div>
                                    <div class="footer text-center">
                                        <a href="#pablo" class="btn btn-primary btn-round">Get Started</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    
    <!--   Core JS Files   -->
    <script src="<?php echo config_item('assets');?>material-kit/js/jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo config_item('assets');?>material-kit/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?php echo config_item('assets');?>material-kit/js/material.min.js"></script>

    <!--    Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/   -->
    <script src="<?php echo config_item('assets');?>material-kit/js/nouislider.min.js" type="text/javascript"></script>

    <!--    Control Center for Material Kit: activating the ripples, parallax effects, scripts from the example pages etc    -->
    <script src="<?php echo config_item('assets');?>material-kit/js/material-kit.js" type="text/javascript"></script>
    
    <!--    Plugin for Tags, full documentation here: http://xoxco.com/projects/code/tagsinput/   -->
    <script src="<?php echo config_item('assets');?>material-kit/js/bootstrap-tagsinput.js"></script>

</body>

</html>
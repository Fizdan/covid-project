<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo config_item('assets');?>material-kit/img/apple-icon.png">
	<link rel="icon" type="image/png" href="<?php echo config_item('assets');?>material-kit/img/favicon.png">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title><?php echo $title;?></title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />

	<!--     Fonts and icons     -->
	<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />

	<!-- CSS Files -->
    <link href="<?php echo config_item('assets');?>material-kit/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo config_item('assets');?>material-kit/css/material-kit.css" rel="stylesheet"/>

</head>


<body class="login-page">

	<div class="page-header header-filter" style="background-image: url('<?php echo config_item('assets');?>material-kit/img/header.jpg'); background-size: cover; background-position: top center;">
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
					<div class="card card-signup">
						<form class="form" method="POST" action="<?php echo base_url('login/submit'); ?>">
							<div class="header header-primary text-center">
								<h4 class="card-title">Login</h4>
							</div>
							<div class="card-content">

								<div class="input-group">
									<span class="input-group-addon">
										<i class="material-icons">email</i>
									</span>
									<input type="email" name="email" class="form-control" placeholder="Email...">
								</div>

								<div class="input-group">
									<span class="input-group-addon">
										<i class="material-icons">lock_outline</i>
									</span>
									<input type="password" name="password" placeholder="Password..." class="form-control" />
								</div>
							</div>
							<div class="text-center">
								<button type="submit" value="login" class="btn btn-secondary btn-lg">Masuk</button>
				               	<?php if ($this->session->flashdata('failed_login')) { ?>
				                  <div class="alert alert-danger">
				                    <button type="button" class="close"></button>
				                    <?php echo $this->session->flashdata('failed_login') ?>
				                  </div>
				                <?php } ?>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

</body>
	<!--   Core JS Files   -->
	<script src="<?php echo config_item('assets');?>material-kit/js/jquery.min.js" type="text/javascript"></script>
	<script src="<?php echo config_item('assets');?>material-kit/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="<?php echo config_item('assets');?>material-kit/js/material.min.js"></script>

	<!--	Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/   -->
	<script src="<?php echo config_item('assets');?>material-kit/js/nouislider.min.js" type="text/javascript"></script>

	<!--    Control Center for Material Kit: activating the ripples, parallax effects, scripts from the example pages etc    -->
	<script src="<?php echo config_item('assets');?>material-kit/js/material-kit.js" type="text/javascript"></script>
	
	<!--	Plugin for Tags, full documentation here: http://xoxco.com/projects/code/tagsinput/   -->
	<script src="<?php echo config_item('assets');?>material-kit/js/bootstrap-tagsinput.js"></script>

</html>
<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Request extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('M_Login');
		$this->load->model('Users/M_Request');
	}

	function index()
	{
		$data = array(
			'title' => 'Request - ITS Face Shield',
			'instansi' => $this->M_Request->get_instansi(),
            'getProvinsi'=> $this->M_Request->getProvinsi(),
            'getTipe'=> $this->M_Request->getTipe(),
		);
		$this->load->view('content/v_request', $data);
	}

	/*
	DOKUMENTASI RAJA ONGKIR

	//Mendapatkan semua propinsi
	$provinces = $this->rajaongkir->province();

	//Mendapatkan semua kota
	$cities = $this->rajaongkir->city();

	//Mendapatkan data ongkos kirim

	$cost = $this->rajaongkir->cost(origin, destination, gram, brand_pengirim);

	$cost = $this->rajaongkir->cost(501, 114, 1000, "jne");

	Response Bisa Dibaca di
	https://rajaongkir.com/dokumentasi/starter#province-response	

	*/

	function add()
	{
		$this->form_validation->set_rules('nama', 'Nama', 'required');
		$this->form_validation->set_rules('wa', 'Nomor Whatsapp', 'required');
		$this->form_validation->set_rules('alamat', 'Alamat', 'required');
		$this->form_validation->set_rules('instansi', 'Instansi', 'required');
		$this->form_validation->set_rules('jumlah', 'Jumlah', 'required');

		if ($this->form_validation->run() != FALSE) {
			$config['encrypt_name'] 	= TRUE;
			$config['upload_path']      = './uploads/dokumen/';
			$config['allowed_types']    = 'pdf';
			$config['max_size']			= 2048;
			$new_name 					= time() . $this->file_ext;
			$config['file_name'] 		= $new_name;
			$this->load->library('upload', $config);
			if (!$this->upload->do_upload('surat_permohonan')) {
				$this->session->set_flashdata('error', 'Gagal mengupload dokumen. Silahkan cek kembali dokumen.');
				redirect('request');
			} else {
				$instansi = $this->input->post('instansi');
				$jumlah = $this->input->post('jumlah');
				$limit = $this->M_Request->getRequestId($instansi);
				$limit = $limit[0]->tipe_limit;
				if($jumlah > $limit) $jumlah = $limit;
				$id_wilayah = $this->M_Request->getRequestId($instansi);
				$id_wilayah = $id_wilayah[0]->id_instansi;
				$provinces = $this->rajaongkir->cost(478, $id_wilayah, 50 * $jumlah, "pos");
				$provinces = json_decode($provinces,true);
				$cost = $provinces['rajaongkir']['results'][0]['costs'][0]['cost'][0]['value'];
				$insert = array(
					'id_instansi' => $instansi,
					'id_kebutuhan' => '1', // Face Shield ID
					'pic' => $this->input->post('nama'),
					'wa_pic' => $this->input->post('wa'),
					'alamat_kirim' => $this->input->post('alamat'),
					'jumlah_permintaan' => $jumlah,
					'surat_permohonan' => $this->upload->data('file_name'),
					'estimasi_biaya' => $cost,
				);
				$this->M_Request->insert_request($insert);
			}
		} else {
			$this->session->set_flashdata('error', 'Silahkan cek kembali pengisian anda.');
		}
		redirect('request');
	}

    function selectkota(){
        $output = [];
        if($this->input->post('id')){
            $menu = $this->db->select("*")
                            ->from("set_wilayah")
                            ->where("parent_id",$this->input->post('id'))
                            ->get()->result();
            foreach($menu as $idx => $item)
            {
                $output[$item->id_wilayah] = $item->nama_wilayah.' | '.$item->keterangan_wilayah.' | '.$item->kode_pos;
            }            
        }
        header('Content-Type: application/json');
        echo json_encode($output);
    }
    
	function addInstansiBaru(){
		$this->form_validation->set_rules('nama_instansi', 'Nama_instansi', 'required');
		$this->form_validation->set_rules('jenis_instansi', 'Jenis_instansi', 'required');
		$this->form_validation->set_rules('tipe_instansi', 'Tipe_instansi', 'required');
		$this->form_validation->set_rules('provinsi_instansi', 'Provinsi_Instansi', 'required');
		$this->form_validation->set_rules('kota_instansi', 'Kota_Instansi', 'required');
		$this->form_validation->set_rules('nama', 'Nama', 'required');
		$this->form_validation->set_rules('wa', 'Nomor Whatsapp', 'required');
		$this->form_validation->set_rules('alamat', 'Alamat', 'required');
		$this->form_validation->set_rules('jumlah', 'Jumlah', 'required');

		if ($this->form_validation->run() != FALSE) {
			$config['encrypt_name'] 	= TRUE;
			$config['upload_path']      = './uploads/dokumen/';
			$config['allowed_types']    = 'pdf';
			$config['max_size']			= 2048;
			$new_name 					= time() . $this->file_ext;
			$config['file_name'] 		= $new_name;
			$this->load->library('upload', $config);
			if (!$this->upload->do_upload('surat_permohonan')) {
				$this->session->set_flashdata('error', 'Gagal mengupload dokumen. Format file harus .pdf ,Silahkan cek kembali dokumen.');
				redirect('request');
			} else {
				//cara dapat instansi
				$instansi = $this->input->post('tipe_instansi');
				$jumlah = $this->input->post('jumlah');
				//id wilayah didapatkan dari id instansi
				$limit = $this->M_Request->getRequestIdTipe($instansi);
				$limit = $limit[0]->tipe_limit;
				if($jumlah > $limit) $jumlah = $limit;
				// $id_wilayah = $this->M_Request->getRequestId($instansi);
				$id_wilayah = $this->input->post('kota_instansi');
				$provinces = $this->rajaongkir->cost(478, $id_wilayah, 50 * $jumlah, "pos");
				$provinces = json_decode($provinces,true);
				$cost = $provinces['rajaongkir']['results'][0]['costs'][0]['cost'][0]['value'];
				// variablearray  insert insert ke trans kebutuhan umum
				$insert = array(
					'id_wilayah' => $id_wilayah,
					'id_kebutuhan' => '1', // Face Shield ID
					'pic' => $this->input->post('nama'),
					'wa_pic' => $this->input->post('wa'),
					'alamat_kirim' => $this->input->post('alamat'),
					'jumlah_permintaan' => $jumlah,
					'surat_permohonan' => $this->upload->data('file_name'),
					'estimasi_biaya' => $cost,
					'jumlah_pemenuhan' => null,
					'resi_pengiriman' => null,
					'bukti_transfer' => null,
					'nama_instansi' => $this->input->post('nama_instansi'),
					'jenis_instansi' => $this->input->post('jenis_instansi'),
					'id_tipe' => $this->input->post('tipe_instansi'),
					'status' => "Waiting"
				);

				// $data_set_instansi = array(
				// 	 'id_instansi' => ,
				// 	 'id_wilayah' => ,
				// 	 'id_prioritas' => null,
					 
				// );
				$this->M_Request->insert_tran_kebutuhan_umum($insert);
			}
		} else {
			$this->session->set_flashdata('error', 'Silahkan cek kembali pengisian anda.');
		}
		redirect('request');

	}
}

<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Instansi extends CI_Controller{
 
	function __construct(){
		parent::__construct();
        $this->load->model('M_Distribusi');

	}

	function index(){
        $header = array(
            'title' => 'Distribusi | ITS Face Shield',
            'content' => 'v_distribusi',
        );

        $data = array(
        	'header' => $header,
        );
        $data["distribusi"] = $this->M_Distribusi->getAll();
        $this->load->view('layout/v_app', $data);
		// header('Content-Type: application/json');
		// echo json_encode( $data ); //Use this for debug var / using var_dump()
	}
}
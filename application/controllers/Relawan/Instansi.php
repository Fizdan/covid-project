<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Instansi extends CI_Controller{
 
	function __construct(){
		parent::__construct();
		$this->load->model('M_Login');
        $this->load->model('M_Instansi');

		if($this->session->userdata('status') != "Login"){
			redirect(base_url("login"));
		}
	}

    function selectkota(){
        $output = [];
        if($this->input->post('id')){
            $menu = $this->db->select("*")
                            ->from("set_wilayah")
                            ->where("parent_id",$this->input->post('id'))
                            ->get()->result();
            foreach($menu as $idx => $item)
            {
                $output[$item->id_wilayah] = $item->nama_wilayah.' | '.$item->keterangan_wilayah.' | '.$item->kode_pos;
            }            
        }
        header('Content-Type: application/json');
        echo json_encode($output);
    }

    function selectprovinsi(){
        $output = [];
        if($this->input->post('id')){
            $menu = $this->db->select("*")
                            ->from("set_wilayah")
                            ->where("id_wilayah",$this->input->post('id'))
                            ->get()->result();

            $menu = $this->db->select("*")
                            ->from("set_wilayah")
                            ->where("id_wilayah",$menu[0]->parent_id)
                            ->get()->result();

            foreach($menu as $idx => $item)
            {
                $output[$item->id_wilayah] = $item->nama_wilayah;
            }            
        }
        header('Content-Type: application/json');
        echo json_encode($output);
    }

	function index(){
        $header = array(
            'title' => 'Instansi | ITS Face Shield',
            'content' => 'relawan/v_instansi',
        );

        $data = array(
        	'header' => $header,
           	'sess' => $this->M_Login->sessdata(),
            'getProvinsi'=> $this->M_Instansi->getProvinsi(),
            'getTipe'=> $this->M_Instansi->getTipe(),
        );
        $data["instansi"] = $this->M_Instansi->getAll();

        $this->load->view('layout/v_app', $data);
		// header('Content-Type: application/json');
		// echo json_encode( $data ); //Use this for debug var / using var_dump()
	}

	public function add()
    {
        $instansi = $this->M_Instansi;
        $validation = $this->form_validation;
        $validation->set_rules($instansi->rules());

        if ($validation->run()) {
            $instansi->save();
            $this->session->set_flashdata('success', 'Data berhasil disimpan');
        }

        redirect('relawan/instansi');
    }

    public function edit($id)
    {
        // var_dump($_POST);
        if (!isset($id)) redirect('relawan/instansi');
        $instansi = $this->M_Instansi;
        $validation = $this->form_validation;
        $validation->set_rules($instansi->rules());

        if ($validation->run()) {
            $instansi->update($id);
            $this->session->set_flashdata('success', 'Data berhasil diperbarui.');
        }
        
        redirect('relawan/instansi');
    }

    public function delete($id=null)
    {
        if (!isset($id)) show_404();
        
        if ($this->M_Instansi->delete($id)) {
            $this->session->set_flashdata('success', 'Data berhasil dihapus.');
            redirect('relawan/instansi');
        }
    }
}
<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Laporan extends CI_Controller{
 
	function __construct(){
		parent::__construct();
		$this->load->model('M_Login');
        $this->load->model('M_Laporan');

		if($this->session->userdata('status') != "Login"){
			redirect(base_url("login"));
		}
	}

	function index(){
        $header = array(
            'title' => 'Laporan | ITS Face Shield',
            'content' => 'relawan/v_laporan',
        );

        $data = array(
        	'header' => $header,
           	'sess' => $this->M_Login->sessdata(),
           	'produksi' => $this->M_Laporan->getProduksi()->result(),
           	'distribusi' => $this->M_Laporan->getDistribusi()->result(),
        );

        $this->load->view('layout/v_app', $data);
		// header('Content-Type: application/json');
		// echo json_encode( $data ); //Use this for debug var / using var_dump()
	}
}
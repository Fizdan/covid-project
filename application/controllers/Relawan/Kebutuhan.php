<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Kebutuhan extends CI_Controller{
 
	function __construct(){
		parent::__construct();
		$this->load->model('M_Login');
        $this->load->model('M_Kebutuhan');

		if($this->session->userdata('status') != "Login"){
			redirect(base_url("login"));
		}
	}

	function index(){
        $header = array(
            'title' => 'Kebutuhan | ITS Face Shield',
            'content' => 'relawan/v_kebutuhan',
        );

        $data = array(
        	'header' => $header,
           	'sess' => $this->M_Login->sessdata(),
        );
        $data["kebutuhan"] = $this->M_Kebutuhan->getAll();
        $this->load->view('layout/v_app', $data);
		// header('Content-Type: application/json');
		// echo json_encode( $data ); //Use this for debug var / using var_dump()
	}

	public function add()
    {
        $kebutuhan = $this->M_Kebutuhan;
        $validation = $this->form_validation;
        $validation->set_rules($kebutuhan->rules());

        if ($validation->run()) {
            $kebutuhan->save();
            $this->session->set_flashdata('success', 'Berhasil disimpan');
        }

        redirect('relawan/kebutuhan');
    }

    public function edit($id)
    {
        if (!isset($id)) redirect('relawan/kebutuhan');
       
        $kebutuhan = $this->M_Kebutuhan;
        $validation = $this->form_validation;
        $validation->set_rules($kebutuhan->rules());

        if ($validation->run()) {
            $kebutuhan->update($id);
            $this->session->set_flashdata('success', 'Berhasil disimpan');
        }

        // $data["kebutuhan"] = $kebutuhan->getById($id);
        // if (!$data["kebutuhan"]) show_404();
        
        redirect('relawan/kebutuhan');
    }

    public function delete($id=null)
    {
        if (!isset($id)) show_404();
        
        if ($this->product_model->delete($id)) {
            $this->session->set_flashdata('success', 'Data berhasil dihapus.');
            redirect('relawan/kebutuhan');
        }
    }
}

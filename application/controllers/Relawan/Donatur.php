<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Donatur extends CI_Controller{
 
	function __construct(){
		parent::__construct();
		$this->load->model('M_Login');
        $this->load->model('M_BarangDonasi');
        $this->load->model('M_UangDonasi');

		if($this->session->userdata('status') != "Login"){
			redirect(base_url("login"));
		}
	}

	function index(){
        $header = array(
            'title' => 'Donatur | ITS Face Shield',
            'content' => 'relawan/v_donatur',
        );

        $data = array(
        	'header' => $header,
           	'sess' => $this->M_Login->sessdata(),
        );
        $data["barangDonasi"] = $this->M_BarangDonasi->getAll();
        $data["uangDonasi"] = $this->M_UangDonasi->getAll();
        $data["getInstansi"] = $this->M_UangDonasi->getInstansi();
        $this->load->view('layout/v_app', $data);
		// header('Content-Type: application/json');
		// echo json_encode( $data ); //Use this for debug var / using var_dump()
	}

	public function addBarang()
    {
        $donatur = $this->M_BarangDonasi;
        $validation = $this->form_validation;
        $validation->set_rules($donatur->rules());

        if ($validation->run()) {
            $donatur->save();
            $this->session->set_flashdata('success', 'Data berhasil disimpan.');
        }

        redirect('relawan/donatur');
    }

    public function addUang()
    {
        $donatur = $this->M_UangDonasi;
        $validation = $this->form_validation;
        $validation->set_rules($donatur->rules());

        if ($validation->run()) {
            $donatur->save();
            $this->session->set_flashdata('success', 'Data berhasil disimpan.');
        }

        redirect('relawan/donatur');
    }

    public function editBarang($id)
    {
        if (!isset($id)) redirect('relawan/donatur');
       
        $donatur = $this->M_BarangDonasi;
        $validation = $this->form_validation;
        $validation->set_rules($donatur->rules());

        if ($validation->run()) {
            $donatur->update($id);
            $this->session->set_flashdata('success', 'Data berhasil diperbarui.');
        }

        // $data["donatur"] = $donatur->getById($id);
        // if (!$data["donatur"]) show_404();
        
        redirect('relawan/donatur');
    }

    public function editUang($id)
    {
        if (!isset($id)) redirect('relawan/donatur');
       
        $donatur = $this->M_UangDonasi;
        $validation = $this->form_validation;
        $validation->set_rules($donatur->rules());

        if ($validation->run()) {
            $donatur->update($id);
            $this->session->set_flashdata('success', 'Data berhasil diperbarui.');
        }

        // $data["donatur"] = $donatur->getById($id);
        // if (!$data["donatur"]) show_404();
        
        redirect('relawan/donatur');
    }

    public function deleteBarang($id=null)
    {
        if (!isset($id)) show_404();

        if ($this->M_BarangDonasi->delete($id)) {
            $this->session->set_flashdata('success', 'Data berhasil dihapus.');          
            redirect('relawan/donatur');
        }
    }

    public function deleteUang($id=null)
    {
        if (!isset($id)) show_404();

        if ($this->M_UangDonasi->delete($id)) {
            $this->session->set_flashdata('success', 'Data berhasil dihapus.');
            redirect('relawan/donatur');
        }
    }
}

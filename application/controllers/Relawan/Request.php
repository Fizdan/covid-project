<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Request extends CI_Controller{
 
	function __construct(){
		parent::__construct();
		$this->load->model('M_Login');
        $this->load->model('M_Request');

		if($this->session->userdata('status') != "Login"){
			redirect(base_url("login"));
		}
	}

    function selectkota(){
        $output = [];
        if($this->input->post('id')){
            $menu = $this->db->select("*")
                            ->from("set_wilayah")
                            ->where("parent_id",$this->input->post('id'))
                            ->get()->result();
            foreach($menu as $idx => $item)
            {
                $output[$item->id_wilayah] = $item->nama_wilayah.' | '.$item->keterangan_wilayah.' | '.$item->kode_pos;
            }            
        }
        header('Content-Type: application/json');
        echo json_encode($output);
    }

    function selectprovinsi(){
        $output = [];
        if($this->input->post('id')){
            $menu = $this->db->select("*")
                            ->from("set_wilayah")
                            ->where("id_wilayah",$this->input->post('id'))
                            ->get()->result();

            $menu = $this->db->select("*")
                            ->from("set_wilayah")
                            ->where("id_wilayah",$menu[0]->parent_id)
                            ->get()->result();

            foreach($menu as $idx => $item)
            {
                $output[$item->id_wilayah] = $item->nama_wilayah;
            }            
        }
        header('Content-Type: application/json');
        echo json_encode($output);
    }

	function index(){
        $header = array(
            'title' => 'Request | ITS Face Shield',
            'content' => 'relawan/v_request',
        );

        $data = array(
        	'header' => $header,
           	'sess' => $this->M_Login->sessdata(),
           	'request' => $this->M_Request->getTransaksiWait()->result(),
            'requestTerima' => $this->M_Request->getTransaksiAcc()->result(),
            'requestUmum' => $this->M_Request->getTransaksiUmum()->result(),
        );
        // var_dump($data);
        $this->load->view('layout/v_app', $data);
		// header('Content-Type: application/json');
		// echo json_encode( $data ); //Use this for debug var / using var_dump()
	}

    public function update_status($id){
        if (!isset($id)) show_404();
        $this->M_Request->accept($id,"Accepted");
        redirect('relawan/request');
    }

    public function finish_status($id){
        if (!isset($id)) show_404();
        $this->M_Request->accept($id,"Finished");
        redirect('relawan/request');
    }

    // Fungsi untuk tombol accept Instansi Baru
    public function update_umum($id){
        // if (!isset($id)) redirect('relawan/request');
        $this->M_Request->accept_umum($id,"Accepted"); //accept di kebutuhan umum
        $transaksi =  $this->db->select("*")
        ->from("tran_kebutuhan_umum")
        ->where("id_transaksi_kebutuhan",$id)
        ->get()->result();
        
        // var_dump($transaksi[0]->id_wilayah);
        $data_set_instansi = array(
            'id_wilayah' => $transaksi[0]->id_wilayah,
            'id_prioritas' => null,
            'id_tipe' => $transaksi[0]->id_tipe,
            'nama_instansi' => $transaksi[0]->nama_instansi,
            'jenis_instansi' => $transaksi[0]->jenis_instansi,
        );
        $this->M_Request->insert_set_instansi($data_set_instansi);
        $instansi =  $this->db->select("*")
        ->from("set_instansi")
        ->where("nama_instansi",$transaksi[0]->nama_instansi)
        ->get()->result();

        $data_tran_kebutuhan = array(
            'id_instansi' => $instansi[0]->id_instansi,
            'id_kebutuhan' => $transaksi[0]->id_kebutuhan,
            'pic' => $transaksi[0]->pic,
            'wa_pic'=>  $transaksi[0]->wa_pic,
            'alamat_kirim' =>  $transaksi[0]->alamat_kirim,
            'surat_permohonan' =>  $transaksi[0]->surat_permohonan,
            'jumlah_permintaan' =>  $transaksi[0]->jumlah_permintaan,
            'jumlah_pemenuhan' =>  $transaksi[0]->jumlah_pemenuhan,
            'estimasi_biaya' =>  $transaksi[0]->estimasi_biaya,
            'bukti_transfer' =>  $transaksi[0]->bukti_transfer,
            'status' =>  $transaksi[0]->status,

        );
        $this->M_Request->insert_tran_kebutuhan($data_tran_kebutuhan);
        $this->M_Request->delete_tran_kebutuhan_umum($id);
        $this->session->set_flashdata('success', 'Data Berhasi Di Update.');
        redirect('relawan/request');
    }

    public function edit($id)
    {
        if (!isset($id)) redirect('relawan/request');
        $this->form_validation->set_rules('jumlah_permintaan', 'Jumlah Permintaan', 'required');
        $this->form_validation->set_rules('jumlah_pemenuhan', 'Jumlah Pemenuhan', 'required');
        $this->form_validation->set_rules('estimasi_biaya', 'Estimasi Biaya', 'required');
        if ($this->form_validation->run() != FALSE) {
            $insert = array(
                // 'id_transaksi_kebutuhan' => $id,
                'jumlah_permintaan' => $this->input->post('jumlah_permintaan'),
                'jumlah_pemenuhan' => $this->input->post('jumlah_pemenuhan'),
                'estimasi_biaya' => $this->input->post('estimasi_biaya')
            );
            // var_dump($id);
            $this->M_Request->update($insert,$id);
        } else {
            $this->session->set_flashdata('error', 'Silahkan cek kembali pengisian anda.');
        }
        redirect('relawan/request');
    }
}
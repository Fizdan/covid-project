<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Dashboard extends CI_Controller{
 
	function __construct(){
		parent::__construct();
		$this->load->model('M_Login');
        $this->load->model('M_Dashboard');

		if($this->session->userdata('status') != "Login"){
			redirect(base_url("login"));
		}
	}

	function index(){
        $header = array(
            'title' => 'Dashboard | ITS Face Shield',
            'content' => 'relawan/v_dashboard',
        );

		$total_distribusi = $this->db->query("SELECT COALESCE(SUM(jumlah_distribusi),0) as jumlah FROM tran_distribusi")->result();
		$total_produksi = $this->db->query("SELECT COALESCE(SUM(jumlah_produksi),0) as jumlah FROM tran_produksi")->result();
		$total_produksi_perday = $this->db->query("SELECT COALESCE(SUM(jumlah_produksi),0) as jumlah FROM tran_produksi WHERE created_at = CURDATE()")->result();
		$total_distribusi_perday = $this->db->query("SELECT COALESCE(SUM(jumlah_distribusi),0) as jumlah FROM tran_distribusi WHERE created_at = CURDATE()")->result();
		$total_sisa = $this->db->query("SELECT COALESCE(SUM(jumlah_produksi),0)-(SELECT COALESCE(SUM(jumlah_distribusi),0) as jumlah FROM tran_distribusi) as jumlah FROM tran_produksi")->result();

        $data = array(
        	'header' => $header,
           	'sess' => $this->M_Login->sessdata(),
           	'total_distribusi' => $total_distribusi[0]->jumlah,
           	'total_produksi' => $total_produksi[0]->jumlah,
           	'total_produksi_perday' => $total_produksi_perday[0]->jumlah,
           	'total_distribusi_perday' => $total_distribusi_perday[0]->jumlah,
           	'total_sisa' => $total_sisa[0]->jumlah,
        );

        $this->load->view('layout/v_app', $data);
		// header('Content-Type: application/json');
		// echo json_encode( $data ); //Use this for debug var / using var_dump()
	}
}
<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Progres extends CI_Controller{
 
	function __construct(){
		parent::__construct();
		$this->load->model('M_Login');
        $this->load->model('M_Progres');

		if($this->session->userdata('status') != "Login"){
			redirect(base_url("login"));
		}
	}

	function index(){
        $this->distribusi();
	}

	function distribusi(){
        $header = array(
            'title' => 'Progres | ITS Face Shield',
            'content' => 'relawan/v_distribusi',
        );

        $data = array(
        	'header' => $header,
           	'sess' => $this->M_Login->sessdata(),
           	'request' => $this->M_Progres->getTransaksiAcc()->result(),
        );

        $this->load->view('layout/v_app', $data);
		// header('Content-Type: application/json');
		// echo json_encode( $data ); //Use this for debug var / using var_dump()
	}

	function distribusiAdd(){
		$this->form_validation->set_rules('id_transaksi_kebutuhan', 'Request Instansi', 'required');
		$this->form_validation->set_rules('jumlah_distribusi', 'Jumlah Distribusi', 'required');
		$this->form_validation->set_rules('kurir_pengiriman', 'Kurir Pengiriman', 'required');
		// $this->form_validation->set_rules('resi_pengiriman', 'Nomor Resi', 'required');

		if ($this->form_validation->run() != FALSE) {
			$id = $this->M_Login->sessdata();
			$insert = array(
				'id_transaksi_kebutuhan' => $this->input->post('id_transaksi_kebutuhan'),
				'jumlah_distribusi' => $this->input->post('jumlah_distribusi'),
				'kurir_pengiriman' => $this->input->post('kurir_pengiriman'),
				'resi_pengiriman' => $this->input->post('resi_pengiriman'),
				'created_by' => $id['id'],
			);
			$this->M_Progres->insert_distribusi($insert);
		} else {
			$this->session->set_flashdata('error', 'Silahkan cek kembali pengisian anda.');
		}
		redirect('relawan/progres/distribusi');
		// header('Content-Type: application/json');
		// echo json_encode( $data ); //Use this for debug var / using var_dump()
	}

	function produksiAdd(){
		$this->form_validation->set_rules('id_kebutuhan', 'Kebutuhan', 'required');
		$this->form_validation->set_rules('jumlah_produksi', 'Jumlah Produksi', 'required');

		if ($this->form_validation->run() != FALSE) {
			$id = $this->M_Login->sessdata();
			$insert = array(
				'id_kebutuhan' => $this->input->post('id_kebutuhan'),
				'jumlah_produksi' => $this->input->post('jumlah_produksi'),
				'created_by' => $id['id'],
			);
			$this->M_Progres->insert_produksi($insert);
		} else {
			$this->session->set_flashdata('error', 'Silahkan cek kembali pengisian anda.');
		}
		redirect('relawan/progres/produksi');
		// header('Content-Type: application/json');
		// echo json_encode( $data ); //Use this for debug var / using var_dump()
	}

	function produksi(){
        $header = array(
            'title' => 'Progres | ITS Face Shield',
            'content' => 'relawan/v_produksi',
        );

        $data = array(
        	'header' => $header,
           	'sess' => $this->M_Login->sessdata(),
        );

        $this->load->view('layout/v_app', $data);
		// header('Content-Type: application/json');
		// echo json_encode( $data ); //Use this for debug var / using var_dump()
	}
}
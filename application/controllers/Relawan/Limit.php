<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Limit extends CI_Controller{
 
    function __construct(){
        parent::__construct();
        $this->load->model('M_Login');
        $this->load->model('M_Limit');

        if($this->session->userdata('status') != "Login"){
            redirect(base_url("login"));
        }
    }

    function index(){
        $header = array(
            'title' => 'Limit | ITS Face Shield',
            'content' => 'relawan/v_limit',
        );

        $data = array(
            'header' => $header,
            'sess' => $this->M_Login->sessdata(),
        );
        $data["limit"] = $this->M_Limit->getAll();
        $this->load->view('layout/v_app', $data);
        // header('Content-Type: application/json');
        // echo json_encode( $data ); //Use this for debug var / using var_dump()
    }


    public function edit($id)
    {
        if (!isset($id)) redirect('relawan/limit');
       
        $kebutuhan = $this->M_Limit;
        $validation = $this->form_validation;
        $validation->set_rules($kebutuhan->rules());

        if ($validation->run()) {
            $kebutuhan->update($id);
            $this->session->set_flashdata('success', 'Berhasil disimpan');
        }

        redirect('relawan/limit');
    }

}

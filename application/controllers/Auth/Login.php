<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller{
 
	function __construct(){
		parent::__construct();
		$this->load->model('M_Login');

		if($this->session->userdata('status') == "Login"){
			redirect(base_url("dashboard"));
		}
	}
 
	function index(){
        $data = array(
            'title' => 'Login | ITS Face Shield',
        );
		$this->load->view('auth/v_login', $data);
	}
 
	function submit(){
		$email = $this->input->post('email');
		$password = $this->input->post('password');
		$where = array(
			'email' => $email,
			'password' => $password
			);
			
		$check = $this->M_Login->check($where)->row();

		if($check){
			$data_session = array(
					'nama' => $check->nama_pengguna,
					'email' => $check->email,	
					'jenis_pengguna' => $check->jenis_pengguna,			
					'id' => $check->id,					
					'status' => "Login"
					);
			$this->session->set_userdata($data_session);
			redirect(base_url("dashboard"));
		} else {
			$this->session->set_flashdata('failed_login', 'Username atau Password Salah!');
			redirect(base_url('login'));
		}
	}
}
<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * RajaOngkir API Key
 * Silakan daftar akun di RajaOngkir.com untuk mendapatkan API Key
 * http://rajaongkir.com/akun/daftar
 */
$config['rajaongkir_api_key'] = "8564c11d7c09bbd78efd1df74533ec2c";

/**
 * RajaOngkir account type: starter or basic
 * http://rajaongkir.com/dokumentasi#akun-ringkasan
 * 
 */
$config['rajaongkir_account_type'] = "starter";

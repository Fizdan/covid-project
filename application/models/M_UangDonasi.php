<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class M_UangDonasi extends CI_Model{	

    private $_table = "uang_donasi";
    public $id_uang;
    public $nama_donatur_uang;
    public $jumlah_uang;

	function check($where){		
		return $this->db->get_where($this->_table,$where);
    }
    
    public function rules()
{
    return [
        ['field' => 'id_uang',
        'label' => 'id_uang',
        'rules' => 'numeric'],

        ['field' => 'jumlah_uang',
        'label' => 'jumlah_uang',
        'rules' => 'numeric'],
        
        ['field' => 'nama_donatur_uang',
        'label' => 'nama_donatur_uang',
        'rules' => 'required'],

        ['field' => 'rekening_donatur_uang',
        'label' => 'rekening_donatur_uang',
        'rules' => 'required'],

        // ['field' => 'instansi_donatur_uang',
        // 'label' => 'instansi_donatur_uang',
        // 'rules' => 'required']
    ];
}

    function getAll(){		
        return $this->db->select("*, (SELECT nama_instansi FROM set_instansi WHERE set_instansi.id_instansi = a.instansi_donatur_uang) as nama")->from("uang_donasi a")->get()->result();
    }
    
    public function getInstansi(){
        return $this->db->get('set_instansi')->result();
    }

    public function getById($id)
    {
        return $this->db->get_where($this->_table, ["id_uang" => $id])->row();
    }

    public function save()
    {
        $post = $this->input->post();
        $this->nama_donatur_uang = $post["nama_donatur_uang"];
        $this->jumlah_uang = $post["jumlah_uang"];
        $this->rekening_donatur_uang = $post["rekening_donatur_uang"];
        $this->instansi_donatur_uang = $post["instansi_donatur_uang"];
        return $this->db->insert($this->_table, $this);
    }

    public function update($id)
    {
        $post = $this->input->post();
        $this->id_uang = $id;
        $this->nama_donatur_uang = $post["nama_donatur_uang"];
        $this->jumlah_uang = $post["jumlah_uang"];
        $this->rekening_donatur_uang = $post["rekening_donatur_uang"];
        $this->instansi_donatur_uang = $post["instansi_donatur_uang"];
        $this->db->update($this->_table, $this, array('id_uang' => $id));
    }

    public function delete($id)
    {
        return $this->db->delete($this->_table, array("id_uang" => $id));
    }
}

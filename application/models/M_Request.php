<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Request extends CI_Model{

	function getRequest(){
		return $this->db->get('set_instansi')->result();
	}

	function insert_request($data){
		$this->db->trans_begin();
		try {
	        $this->db->insert('tran_kebutuhan', $data);
	        $this->db->trans_commit();
	        $this->session->set_flashdata('success', 'Data telah berhasil ditambahkan.');
	    } catch (\Exception $e) {
	    	$this->db->trans_rollback();
	    	$this->session->set_flashdata('error', $e->getMessage());
	        return;
	    }
	}

	function accept($id,$data){
		// var_dump($data);
		$this->db->trans_begin();
		try {
			$this->db->where('id_transaksi_kebutuhan', $id);
	        $this->db->update('tran_kebutuhan', ['status' => $data]);
	        $this->db->trans_commit();
	        $this->session->set_flashdata('success', 'Data telah berhasil diperbarui.');
	    } catch (\Exception $e) {
	    	$this->db->trans_rollback();
	    	$this->session->set_flashdata('error', $e->getMessage());
	        return;
	    }		
	}

	function accept_umum($id,$data){
		// var_dump($data);
		$this->db->trans_begin();
		try {
			$this->db->where('id_transaksi_kebutuhan', $id);
	        $this->db->update('tran_kebutuhan_umum', ['status' => $data]);
	        $this->db->trans_commit();
	        $this->session->set_flashdata('success', 'Data telah berhasil diperbarui.');
	    } catch (\Exception $e) {
	    	$this->db->trans_rollback();
	    	$this->session->set_flashdata('error', $e->getMessage());
	        return;
	    }		
	}

	function insert_set_instansi($data){
		$this->db->trans_begin();
		try {
	        $this->db->insert('set_instansi', $data);
	        $this->db->trans_commit();
	        $this->session->set_flashdata('success', 'Data telah berhasil ditambahkan.');
	    } catch (\Exception $e) {
	    	$this->db->trans_rollback();
	    	$this->session->set_flashdata('error', $e->getMessage());
	        return;
	    }
	}

	function insert_tran_kebutuhan($data){
		$this->db->trans_begin();
		try {
	        $this->db->insert('tran_kebutuhan', $data);
	        $this->db->trans_commit();
	        $this->session->set_flashdata('success', 'Data telah berhasil ditambahkan.');
	    } catch (\Exception $e) {
	    	$this->db->trans_rollback();
	    	$this->session->set_flashdata('error', $e->getMessage());
	        return;
	    }
	}

	public function delete_tran_kebutuhan_umum($id)
    {
        return $this->db->delete("tran_kebutuhan_umum", array("id_transaksi_kebutuhan" => $id));
	}
	
	function get_tran_kebutuhan_umum($id){
		try {
	        return $this->db->select("*")
	        		->from("tran_kebutuhan_umum ")
	        		->where("id_transaksi_kebutuhan",$id)
	        		->get();
	    } catch (\Exception $e) {
	    	$this->session->set_flashdata('error', $e->getMessage());
	        return;
	    }
	}

	function update($insert,$id){
		$this->db->trans_begin();
		try {
			$this->db->where('id_transaksi_kebutuhan', $id);
	        $this->db->update('tran_kebutuhan', $insert);
	        $this->db->trans_commit();
	        $this->session->set_flashdata('success', 'Data telah berhasil diperbarui.');
	    } catch (\Exception $e) {
	    	$this->db->trans_rollback();
	    	$this->session->set_flashdata('error', $e->getMessage());
	        return;
	    }		
	}

	function getTransaksiWait(){
		try {
	        return $this->db->select("*, (SELECT tipe_nama FROM set_tipe WHERE set_tipe.id_tipe = b.id_tipe) as tipe_instansi")
	        		->from("tran_kebutuhan a")
	        		->join("set_instansi b","b.id_instansi = a.id_instansi")
	        		->where("status","Waiting")
	        		->get();
	    } catch (\Exception $e) {
	    	$this->session->set_flashdata('error', $e->getMessage());
	        return;
	    }
	}

	function getTransaksiUmum(){
		try {
	        return $this->db->select("*, (SELECT tipe_nama FROM set_tipe WHERE set_tipe.id_tipe = a.id_tipe) as tipe_instansi")
	        		->from("tran_kebutuhan_umum a")
	        		->where("status","Waiting")
	        		->get();
	    } catch (\Exception $e) {
	    	$this->session->set_flashdata('error', $e->getMessage());
	        return;
	    }
	}

	function getTransaksiAcc(){
		try {
	        return $this->db->select("*, (SELECT tipe_nama FROM set_tipe WHERE set_tipe.id_tipe = b.id_tipe) as tipe_instansi")
	        		->from("tran_kebutuhan a")
	        		->join("set_instansi b","b.id_instansi = a.id_instansi")
	        		->where("status","Accepted")
	        		->get();
	    } catch (\Exception $e) {
	    	$this->session->set_flashdata('error', $e->getMessage());
	        return;
	    }
	}
}
<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Request extends CI_Model{

	function get_instansi(){
		return $this->db->query("
			SELECT a.id_instansi,
				   a.nama_instansi,
				   a.jenis_instansi, 
				   a.id_tipe, 
				   (SELECT nama_wilayah FROM set_wilayah WHERE id_wilayah = a.id_wilayah) as kota,
				   (SELECT kode_pos FROM set_wilayah WHERE id_wilayah = a.id_wilayah) as kode_pos, 
				   (SELECT nama_wilayah FROM set_wilayah WHERE id_wilayah = (SELECT parent_id FROM set_wilayah WHERE id_wilayah = a.id_wilayah )) as provinsi 
			FROM set_instansi a
			")->result();
	}

	function getRequestId($id){
		return $this->db->select("*")
						->from("set_instansi a")
						->join("set_tipe b", "b.id_tipe = a.id_tipe")
						->where(['id_instansi' => $id])
						->get()->result();
		// return $this->db->get_where('set_instansi', ['id_instansi' => $id])->result();
	}


	function getRequestIdTipe($id){
		return $this->db->select("*")
						->from("set_tipe a")
						->where(['id_tipe' => $id])
						->get()->result();
		// return $this->db->get_where('set_instansi', ['id_instansi' => $id])->result();
	}

    public function getTipe(){
        return $this->db->get('set_tipe')->result();
    }

    public function getProvinsi()
    {
        return $this->db->get_where("set_wilayah", ["parent_id" => NULL])->result();
    }

	function insert_request($data){
		$this->db->trans_begin();
		try {
	        $this->db->insert('tran_kebutuhan', $data);
	        $this->db->trans_commit();
	        $this->session->set_flashdata('success', 'Request telah berhasil ditambahkan.');
	    } catch (\Exception $e) {
	    	$this->db->trans_rollback();
	    	$this->session->set_flashdata('error', $e->getMessage());
	        return;
	    }
	}

	function insert_set_instansi($data){
		$this->db->trans_begin();
		try {
	        $this->db->insert('set_instansi', $data);
	        $this->db->trans_commit();
	        $this->session->set_flashdata('success', 'Request telah berhasil ditambahkan.');
	    } catch (\Exception $e) {
	    	$this->db->trans_rollback();
	    	$this->session->set_flashdata('error', $e->getMessage());
	        return;
	    }
	}

	function insert_tran_kebutuhan_umum($data){
		$this->db->trans_begin();
		try {
	        $this->db->insert('tran_kebutuhan_umum', $data);
	        $this->db->trans_commit();
	        $this->session->set_flashdata('success', 'Request telah berhasil ditambahkan.');
	    } catch (\Exception $e) {
	    	$this->db->trans_rollback();
	    	$this->session->set_flashdata('error', $e->getMessage());
	        return;
	    }
	}
}
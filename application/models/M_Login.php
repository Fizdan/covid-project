<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Login extends CI_Model{	
	function check($where){		
		return $this->db->get_where("users",$where);
	}

	// function pswd_encode($ifpsd){
	// 	$result=md5($ifpsd.'*&@-_#%^.Fhb+}{');
	// 	return $result;
	// }

	function sessdata(){
		$data['nama'] = $this->session->userdata('nama');	
		$data['jenis_pengguna'] = $this->session->userdata('jenis_pengguna');	
		$data['id'] = $this->session->userdata('id');	
		$data['email'] = $this->session->userdata('email');			
		return $data;
	}
}
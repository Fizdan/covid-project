<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Kebutuhan extends CI_Model{	

    private $_table = "set_kebutuhan";

	function check($where){		
		return $this->db->get_where("set_kebutuhan",$where);
    }
    
    public function rules()
{
    return [
        ['field' => 'nama_kebutuhan',
        'label' => 'nama_kebutuhan',
        'rules' => 'required'],

        ['field' => 'tersedia',
        'label' => 'tersedia',
        'rules' => 'numeric'],
        
        ['field' => 'biaya_produksi_kebutuhan',
        'label' => 'biaya_produksi_kebutuhan',
        'rules' => 'numeric']
    ];
}

    function getAll(){		
		return $this->db->get($this->_table)->result();
    }
    
    public function getById($id)
    {
        return $this->db->get_where($this->_table, ["id_kebutuhan" => $id])->row();
    }

    public function save()
    {
        $post = $this->input->post();

        $this->nama_kebutuhan = $post["nama_kebutuhan"];
        $this->tersedia = $post["tersedia"];
        $this->biaya_produksi_kebutuhan = $post["biaya_produksi_kebutuhan"];


        return $this->db->insert($this->_table, $this);
    }

    public function update($id)
    {
        $post = $this->input->post();
        $this->id_kebutuhan = $id;
        $this->nama_kebutuhan = $post["nama_kebutuhan"];
        $this->tersedia = $post["tersedia"];
        $this->biaya_produksi_kebutuhan = $post["biaya_produksi_kebutuhan"];
        $this->db->update($this->_table, $this, array('id_kebutuhan' => $id));
    }

    public function delete($id)
    {
        return $this->db->delete($this->_table, array("id_kebutuhan" => $id));
    }
}

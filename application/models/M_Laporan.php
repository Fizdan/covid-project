<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Laporan extends CI_Model{

	function getRequest(){
		return $this->db->get('set_instansi')->result();
	}

	function getDistribusi(){
		try {
	        return $this->db->select("c.nama_instansi,a.jumlah_distribusi,a.kurir_pengiriman,a.resi_pengiriman,a.created_at,
	        	(SELECT nama_pengguna FROM users WHERE users.id = a.created_by) as oleh, 
	        	(SELECT nama_wilayah FROM set_wilayah WHERE set_wilayah.id_wilayah = c.id_wilayah) as kota, 
	        	(SELECT nama_wilayah FROM set_wilayah WHERE set_wilayah.id_wilayah = (SELECT parent_id FROM set_wilayah WHERE set_wilayah.id_wilayah = c.id_wilayah)) as provinsi")
	        		->from("tran_distribusi a")
	        		->join("tran_kebutuhan b","b.id_transaksi_kebutuhan = a.id_transaksi_kebutuhan")
	        		->join("set_instansi c","c.id_instansi = b.id_instansi")
	        		->get();
	    } catch (\Exception $e) {
	    	$this->session->set_flashdata('error', $e->getMessage());
	        return;
	    }
	}

	function getProduksi(){
		try {
	        return $this->db->select("a.jumlah_produksi, a.created_at, b.nama_kebutuhan, (SELECT nama_pengguna FROM users WHERE users.id = a.created_by) as oleh")
	        		->from("tran_produksi a")
	        		->join("set_kebutuhan b","b.id_kebutuhan = a.id_kebutuhan")
	        		->get();
	    } catch (\Exception $e) {
	    	$this->session->set_flashdata('error', $e->getMessage());
	        return;
	    }
	}

}
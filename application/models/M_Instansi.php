<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Instansi extends CI_Model{	

    private $_table = "set_instansi";
    public $id_instansi;
    public $id_wilayah;
    public $id_prioritas;
    public $nama_instansi;
    public $id_tipe;
    public $jenis_instansi;

	function check($where){		
		return $this->db->get_where("set_instansi",$where);
    }
    
    public function rules()
{
    return [
        ['field' => 'id_wilayah',
        'label' => 'Kota',
        'rules' => 'numeric'],

        // ['field' => 'id_prioritas',
        // 'label' => 'id_prioritas',
        // 'rules' => 'numeric'],
        
        ['field' => 'nama',
        'label' => 'Nama Instansi',
        'rules' => 'required'],

        ['field' => 'tipe',
        'label' => 'Tipe Instansi',
        'rules' => 'required'],

        ['field' => 'jenis',
        'label' => 'Jenis Instansi',
        'rules' => 'required']
    ];
}

    function getAll(){		
        return $this->db->query("
            SELECT a.id_wilayah,
                   a.id_instansi,
                   a.nama_instansi,
                   a.jenis_instansi,
                   a.id_tipe,
                   (SELECT tipe_nama FROM set_tipe where set_tipe.id_tipe = a.id_tipe) as tipe_instansi, 
                   (SELECT tipe_limit FROM set_tipe where set_tipe.id_tipe = a.id_tipe) as 'limit',
                   (SELECT nama_wilayah FROM set_wilayah WHERE id_wilayah = a.id_wilayah) as kota, 
                   (SELECT nama_wilayah FROM set_wilayah WHERE id_wilayah = (SELECT parent_id FROM set_wilayah WHERE id_wilayah = a.id_wilayah )) as provinsi 
            FROM set_instansi a
            ")->result();
    }
    
    public function getById($id)
    {
        return $this->db->get_where($this->_table, ["id_instansi" => $id])->row();
    }

    public function getTipe(){
        return $this->db->get('set_tipe')->result();
    }

    public function getProvinsi()
    {
        return $this->db->get_where("set_wilayah", ["parent_id" => NULL])->result();
    }

    public function save()
    {
        $post = $this->input->post();
        $this->id_wilayah = $post["id_wilayah"];
        // $this->id_prioritas = $post["id_prioritas"];
        $this->nama_instansi = $post["nama"];
        $this->id_tipe = $post["tipe"];
        $this->jenis_instansi = $post["jenis"];
        return $this->db->insert($this->_table, $this);
    }

    public function update($id)
    {
        $post = $this->input->post();
        $this->id_instansi = $id;
        $this->id_wilayah = $post["id_wilayah"];
        // $this->id_prioritas = $post["id_prioritas"];
        $this->nama_instansi = $post["nama"];
        $this->id_tipe = $post["tipe"];
        $this->jenis_instansi = $post["jenis"];
        $this->db->where(['id_instansi' => $id]);
        $this->db->update($this->_table, $this);
    }

    public function delete($id)
    {
        return $this->db->delete($this->_table, array("id_instansi" => $id));
    }
}
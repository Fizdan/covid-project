<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class M_BarangDonasi extends CI_Model{	

    private $_table = "barang_donasi";
    public $id_barang_donasi;
    public $jumlah_barang_donasi;
    public $nama_barang_donasi;
    public $nama_donatur_barang_donasi;
    public $instansi_donatur_barang_donasi;


	function check($where){		
		return $this->db->get_where($this->_table,$where);
    }
    
    public function rules()
{
    return [
        ['field' => 'id_barang_donasi',
        'label' => 'id_barang_donasi',
        'rules' => 'numeric'],

        ['field' => 'jumlah_barang_donasi',
        'label' => 'jumlah_barang_donasi',
        'rules' => 'numeric'],
        
        ['field' => 'nama_barang_donasi',
        'label' => 'nama_barang_donasi',
        'rules' => 'required'],

        ['field' => 'nama_donatur_barang_donasi',
        'label' => 'nama_donatur_barang_donasi',
        'rules' => 'required'],

        // ['field' => 'instansi_donatur_barang_donasi',
        // 'label' => 'instansi_donatur_barang_donasi',
        // 'rules' => 'required']
    ];
}

    function getAll(){		
		return $this->db->select("*, (SELECT nama_instansi FROM set_instansi WHERE set_instansi.id_instansi = a.instansi_donatur_barang_donasi) as nama")->from("barang_donasi a")->get()->result();
    }
    
    public function getInstansi(){
        return $this->db->get('set_instansi')->result();
    }

    public function getById($id)
    {
        return $this->db->get_where($this->_table, ["id_barang_donasi" => $id])->row();
    }

    public function save()
    {
        $post = $this->input->post();
        $this->jumlah_barang_donasi = $post["jumlah_barang_donasi"];
        $this->nama_barang_donasi = $post["nama_barang_donasi"];
        $this->nama_donatur_barang_donasi = $post["nama_donatur_barang_donasi"];
        $this->instansi_donatur_barang_donasi = $post["instansi_donatur_barang_donasi"];
        return $this->db->insert($this->_table, $this);
    }

    public function update($id)
    {
        $post = $this->input->post();
        $this->id_barang_donasi = $id;
        $this->jumlah_barang_donasi = $post["jumlah_barang_donasi"];
        $this->nama_barang_donasi = $post["nama_barang_donasi"];
        $this->nama_donatur_barang_donasi = $post["nama_donatur_barang_donasi"];
        $this->instansi_donatur_barang_donasi = $post["instansi_donatur_barang_donasi"];
        $this->db->update($this->_table, $this, array('id_barang_donasi' => $id));
    }

    public function delete($id)
    {
        return $this->db->delete($this->_table, array("id_barang_donasi" => $id));
    }
}

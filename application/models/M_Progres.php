<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Progres extends CI_Model{

	function getRequest(){
		return $this->db->get('set_instansi')->result();
	}

	function insert_distribusi($data){
		$this->db->trans_begin();
		try {
	        $this->db->insert('tran_distribusi', $data);
	        $this->db->set('jumlah_pemenuhan', 'jumlah_pemenuhan+'.$data['jumlah_distribusi'].'', FALSE);
	        $this->db->where('id_transaksi_kebutuhan', $data['id_transaksi_kebutuhan']);
	        $this->db->update('tran_kebutuhan');
	        $this->db->trans_commit();
	        $this->session->set_flashdata('success', 'Data telah berhasil ditambahkan.');
	    } catch (\Exception $e) {
	    	$this->db->trans_rollback();
	    	$this->session->set_flashdata('error', $e->getMessage());
	        return;
	    }
	}

	function insert_produksi($data){
		$this->db->trans_begin();
		try {
	        $this->db->insert('tran_produksi', $data);
	        $this->db->trans_commit();
	        $this->session->set_flashdata('success', 'Data telah berhasil ditambahkan.');
	    } catch (\Exception $e) {
	    	$this->db->trans_rollback();
	    	$this->session->set_flashdata('error', $e->getMessage());
	        return;
	    }
	}

	function getTransaksiAcc(){
		try {
	        return $this->db->select("*, 
	        	(SELECT nama_wilayah FROM set_wilayah WHERE set_wilayah.id_wilayah = b.id_wilayah) as kota,  
	        	(SELECT nama_wilayah FROM set_wilayah WHERE set_wilayah.id_wilayah = (SELECT parent_id FROM set_wilayah WHERE set_wilayah.id_wilayah = b.id_wilayah)) as provinsi")
	        		->from("tran_kebutuhan a")
	        		->join("set_instansi b","b.id_instansi = a.id_instansi")
	        		->where("status","Accepted")
	        		->get();
	    } catch (\Exception $e) {
	    	$this->session->set_flashdata('error', $e->getMessage());
	        return;
	    }
	}
}
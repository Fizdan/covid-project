<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Limit extends CI_Model{	

    private $_table = "set_tipe";

	function check($where){		
		return $this->db->get_where("set_kebutuhan",$where);
    }
    
    public function rules()
    {
    return [
        ['field' => 'tipe_limit',
        'label' => 'Limit Request',
        'rules' => 'required'],

        // ['field' => 'tipe_nama',
        // 'label' => 'Nama Tipe Instansi',
        // 'rules' => 'required'],
    ];
    }

    function getAll(){		
		return $this->db->get($this->_table)->result();
    }
    
    public function getById($id)
    {
        return $this->db->get_where($this->_table, ["id_kebutuhan" => $id])->row();
    }

    public function update($id)
    {
        $post = $this->input->post();
        $this->id_tipe = $id;
        // $this->tipe_nama = $post["tipe_nama"];
        $this->tipe_limit = $post["tipe_limit"];
        $this->db->update($this->_table, $this, array('id_tipe' => $id));
    }
}

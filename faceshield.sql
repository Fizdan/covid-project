/*
 Navicat Premium Data Transfer

 Source Server         : Tim Relawan COVID-19 IT
 Source Server Type    : MySQL
 Source Server Version : 100231
 Source Host           : face.alhidayah.id:3306
 Source Schema         : u2314635_face

 Target Server Type    : MySQL
 Target Server Version : 100231
 File Encoding         : 65001

 Date: 03/05/2020 16:35:08
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for barang_donasi
-- ----------------------------
DROP TABLE IF EXISTS `barang_donasi`;
CREATE TABLE `barang_donasi`  (
  `id_barang_donasi` bigint(20) NOT NULL AUTO_INCREMENT,
  `jumlah_barang_donasi` bigint(20) NULL DEFAULT NULL,
  `nama_barang_donasi` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT 'NULL',
  `created_at` timestamp(0) NOT NULL DEFAULT current_timestamp(0),
  `updated_at` timestamp(0) NOT NULL DEFAULT current_timestamp(0),
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  `nama_donatur_barang_donasi` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `instansi_donatur_barang_donasi` bigint(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id_barang_donasi`) USING BTREE,
  INDEX `instansi_donatur_barang_donasi`(`instansi_donatur_barang_donasi`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for set_instansi
-- ----------------------------
DROP TABLE IF EXISTS `set_instansi`;
CREATE TABLE `set_instansi`  (
  `id_instansi` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_wilayah` bigint(20) NULL DEFAULT NULL,
  `id_prioritas` bigint(20) NULL DEFAULT NULL,
  `id_tipe` int(11) NULL DEFAULT NULL,
  `nama_instansi` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `jenis_instansi` enum('Pemerintah','Swasta') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT current_timestamp(0),
  `updated_at` timestamp(0) NULL DEFAULT current_timestamp(0) ON UPDATE CURRENT_TIMESTAMP(0),
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id_instansi`) USING BTREE,
  INDEX `fk_wilayah_instansi_to_wilayah`(`id_wilayah`) USING BTREE,
  INDEX `fk_prioritas_instansi_to_prioritas`(`id_prioritas`) USING BTREE,
  INDEX `fk_tipe_idtipe_to_idtipe`(`id_tipe`) USING BTREE,
  CONSTRAINT `fk_prioritas_instansi_to_prioritas` FOREIGN KEY (`id_prioritas`) REFERENCES `set_prioritas` (`id_prioritas`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_tipe_idtipe_to_idtipe` FOREIGN KEY (`id_tipe`) REFERENCES `set_tipe` (`id_tipe`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_wilayah_instansi_to_wilayah` FOREIGN KEY (`id_wilayah`) REFERENCES `set_wilayah` (`id_wilayah`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for set_kebutuhan
-- ----------------------------
DROP TABLE IF EXISTS `set_kebutuhan`;
CREATE TABLE `set_kebutuhan`  (
  `id_kebutuhan` bigint(20) NOT NULL AUTO_INCREMENT,
  `nama_kebutuhan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tersedia` bigint(20) NULL DEFAULT NULL,
  `biaya_produksi_kebutuhan` bigint(255) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT current_timestamp(0),
  `updated_at` timestamp(0) NULL DEFAULT current_timestamp(0) ON UPDATE CURRENT_TIMESTAMP(0),
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id_kebutuhan`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of set_kebutuhan
-- ----------------------------
INSERT INTO `set_kebutuhan` VALUES (1, 'Face Shield', 5, 500000, '2020-04-17 15:36:29', '2020-05-02 00:15:30', NULL);

-- ----------------------------
-- Table structure for set_prioritas
-- ----------------------------
DROP TABLE IF EXISTS `set_prioritas`;
CREATE TABLE `set_prioritas`  (
  `id_prioritas` bigint(20) NOT NULL AUTO_INCREMENT,
  `keterangan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id_prioritas`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of set_prioritas
-- ----------------------------
INSERT INTO `set_prioritas` VALUES (1, 'RS Rujukan COVID');
INSERT INTO `set_prioritas` VALUES (2, 'RS Pemerintah Non Rujukan COVID');
INSERT INTO `set_prioritas` VALUES (3, 'Puskesmas / Faskes 1');
INSERT INTO `set_prioritas` VALUES (4, 'RS Swasta / Klinik Donatur');
INSERT INTO `set_prioritas` VALUES (5, 'Umum');

-- ----------------------------
-- Table structure for set_tipe
-- ----------------------------
DROP TABLE IF EXISTS `set_tipe`;
CREATE TABLE `set_tipe`  (
  `id_tipe` int(11) NOT NULL AUTO_INCREMENT,
  `tipe_nama` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tipe_limit` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id_tipe`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of set_tipe
-- ----------------------------
INSERT INTO `set_tipe` VALUES (1, 'Rumah Sakit', 1);
INSERT INTO `set_tipe` VALUES (2, 'Puskesmas', 20);
INSERT INTO `set_tipe` VALUES (3, 'Klinik Kesehatan/Medical Center', 200);
INSERT INTO `set_tipe` VALUES (4, 'Keamanan/Linmas', 20);

-- ----------------------------
-- Table structure for set_wilayah
-- ----------------------------
DROP TABLE IF EXISTS `set_wilayah`;
CREATE TABLE `set_wilayah`  (
  `id_wilayah` bigint(20) NOT NULL AUTO_INCREMENT,
  `parent_id` bigint(20) NULL DEFAULT NULL,
  `nama_wilayah` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `keterangan_wilayah` enum('Provinsi','Kota','Kabupaten') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `kode_pos` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT current_timestamp(0),
  `updated_at` timestamp(0) NULL DEFAULT current_timestamp(0) ON UPDATE CURRENT_TIMESTAMP(0),
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id_wilayah`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 536 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of set_wilayah
-- ----------------------------
INSERT INTO `set_wilayah` VALUES (1, NULL, 'Bali', 'Provinsi', NULL, '2020-05-01 18:17:39', '2020-05-01 18:20:32', NULL);
INSERT INTO `set_wilayah` VALUES (2, NULL, 'Bangka Belitung', 'Provinsi', NULL, '2020-05-01 18:17:39', '2020-05-01 18:20:32', NULL);
INSERT INTO `set_wilayah` VALUES (3, NULL, 'Banten', 'Provinsi', NULL, '2020-05-01 18:17:39', '2020-05-01 18:20:32', NULL);
INSERT INTO `set_wilayah` VALUES (4, NULL, 'Bengkulu', 'Provinsi', NULL, '2020-05-01 18:17:39', '2020-05-01 18:20:32', NULL);
INSERT INTO `set_wilayah` VALUES (5, NULL, 'DI Yogyakarta', 'Provinsi', NULL, '2020-05-01 18:17:39', '2020-05-01 18:20:32', NULL);
INSERT INTO `set_wilayah` VALUES (6, NULL, 'DKI Jakarta', 'Provinsi', NULL, '2020-05-01 18:17:39', '2020-05-01 18:20:32', NULL);
INSERT INTO `set_wilayah` VALUES (7, NULL, 'Gorontalo', 'Provinsi', NULL, '2020-05-01 18:17:39', '2020-05-01 18:20:32', NULL);
INSERT INTO `set_wilayah` VALUES (8, NULL, 'Jambi', 'Provinsi', NULL, '2020-05-01 18:17:39', '2020-05-01 18:20:32', NULL);
INSERT INTO `set_wilayah` VALUES (9, NULL, 'Jawa Barat', 'Provinsi', NULL, '2020-05-01 18:17:39', '2020-05-01 18:20:32', NULL);
INSERT INTO `set_wilayah` VALUES (10, NULL, 'Jawa Tengah', 'Provinsi', NULL, '2020-05-01 18:17:39', '2020-05-01 18:20:32', NULL);
INSERT INTO `set_wilayah` VALUES (11, NULL, 'Jawa Timur', 'Provinsi', NULL, '2020-05-01 18:17:39', '2020-05-01 18:20:32', NULL);
INSERT INTO `set_wilayah` VALUES (12, NULL, 'Kalimantan Barat', 'Provinsi', NULL, '2020-05-01 18:17:39', '2020-05-01 18:20:32', NULL);
INSERT INTO `set_wilayah` VALUES (13, NULL, 'Kalimantan Selatan', 'Provinsi', NULL, '2020-05-01 18:17:39', '2020-05-01 18:20:32', NULL);
INSERT INTO `set_wilayah` VALUES (14, NULL, 'Kalimantan Tengah', 'Provinsi', NULL, '2020-05-01 18:17:39', '2020-05-01 18:20:32', NULL);
INSERT INTO `set_wilayah` VALUES (15, NULL, 'Kalimantan Timur', 'Provinsi', NULL, '2020-05-01 18:17:39', '2020-05-01 18:20:32', NULL);
INSERT INTO `set_wilayah` VALUES (16, NULL, 'Kalimantan Utara', 'Provinsi', NULL, '2020-05-01 18:17:39', '2020-05-01 18:20:32', NULL);
INSERT INTO `set_wilayah` VALUES (17, NULL, 'Kepulauan Riau', 'Provinsi', NULL, '2020-05-01 18:17:39', '2020-05-01 18:20:32', NULL);
INSERT INTO `set_wilayah` VALUES (18, NULL, 'Lampung', 'Provinsi', NULL, '2020-05-01 18:17:39', '2020-05-01 18:20:32', NULL);
INSERT INTO `set_wilayah` VALUES (19, NULL, 'Maluku', 'Provinsi', NULL, '2020-05-01 18:17:39', '2020-05-01 18:20:32', NULL);
INSERT INTO `set_wilayah` VALUES (20, NULL, 'Maluku Utara', 'Provinsi', NULL, '2020-05-01 18:17:39', '2020-05-01 18:20:32', NULL);
INSERT INTO `set_wilayah` VALUES (21, NULL, 'Nanggroe Aceh Darussalam (NAD)', 'Provinsi', NULL, '2020-05-01 18:17:39', '2020-05-01 18:20:32', NULL);
INSERT INTO `set_wilayah` VALUES (22, NULL, 'Nusa Tenggara Barat (NTB)', 'Provinsi', NULL, '2020-05-01 18:17:39', '2020-05-01 18:20:32', NULL);
INSERT INTO `set_wilayah` VALUES (23, NULL, 'Nusa Tenggara Timur (NTT)', 'Provinsi', NULL, '2020-05-01 18:17:39', '2020-05-01 18:20:32', NULL);
INSERT INTO `set_wilayah` VALUES (24, NULL, 'Papua', 'Provinsi', NULL, '2020-05-01 18:17:39', '2020-05-01 18:20:32', NULL);
INSERT INTO `set_wilayah` VALUES (25, NULL, 'Papua Barat', 'Provinsi', NULL, '2020-05-01 18:17:39', '2020-05-01 18:20:32', NULL);
INSERT INTO `set_wilayah` VALUES (26, NULL, 'Riau', 'Provinsi', NULL, '2020-05-01 18:17:39', '2020-05-01 18:20:32', NULL);
INSERT INTO `set_wilayah` VALUES (27, NULL, 'Sulawesi Barat', 'Provinsi', NULL, '2020-05-01 18:17:39', '2020-05-01 18:20:32', NULL);
INSERT INTO `set_wilayah` VALUES (28, NULL, 'Sulawesi Selatan', 'Provinsi', NULL, '2020-05-01 18:17:39', '2020-05-01 18:20:32', NULL);
INSERT INTO `set_wilayah` VALUES (29, NULL, 'Sulawesi Tengah', 'Provinsi', NULL, '2020-05-01 18:17:39', '2020-05-01 18:20:32', NULL);
INSERT INTO `set_wilayah` VALUES (30, NULL, 'Sulawesi Tenggara', 'Provinsi', NULL, '2020-05-01 18:17:39', '2020-05-01 18:20:32', NULL);
INSERT INTO `set_wilayah` VALUES (31, NULL, 'Sulawesi Utara', 'Provinsi', NULL, '2020-05-01 18:17:39', '2020-05-01 18:20:32', NULL);
INSERT INTO `set_wilayah` VALUES (32, NULL, 'Sumatera Barat', 'Provinsi', NULL, '2020-05-01 18:17:39', '2020-05-01 18:20:32', NULL);
INSERT INTO `set_wilayah` VALUES (33, NULL, 'Sumatera Selatan', 'Provinsi', NULL, '2020-05-01 18:17:39', '2020-05-01 18:20:32', NULL);
INSERT INTO `set_wilayah` VALUES (34, NULL, 'Sumatera Utara', 'Provinsi', NULL, '2020-05-01 18:17:39', '2020-05-01 18:20:32', NULL);
INSERT INTO `set_wilayah` VALUES (35, 21, 'Aceh Barat', 'Kabupaten', '23681', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (36, 21, 'Aceh Barat Daya', 'Kabupaten', '23764', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (37, 21, 'Aceh Besar', 'Kabupaten', '23951', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (38, 21, 'Aceh Jaya', 'Kabupaten', '23654', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (39, 21, 'Aceh Selatan', 'Kabupaten', '23719', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (40, 21, 'Aceh Singkil', 'Kabupaten', '24785', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (41, 21, 'Aceh Tamiang', 'Kabupaten', '24476', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (42, 21, 'Aceh Tengah', 'Kabupaten', '24511', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (43, 21, 'Aceh Tenggara', 'Kabupaten', '24611', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (44, 21, 'Aceh Timur', 'Kabupaten', '24454', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (45, 21, 'Aceh Utara', 'Kabupaten', '24382', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (46, 32, 'Agam', 'Kabupaten', '26411', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (47, 23, 'Alor', 'Kabupaten', '85811', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (48, 19, 'Ambon', 'Kota', '97222', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (49, 34, 'Asahan', 'Kabupaten', '21214', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (50, 24, 'Asmat', 'Kabupaten', '99777', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (51, 1, 'Badung', 'Kabupaten', '80351', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (52, 13, 'Balangan', 'Kabupaten', '71611', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (53, 15, 'Balikpapan', 'Kota', '76111', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (54, 21, 'Banda Aceh', 'Kota', '23238', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (55, 18, 'Bandar Lampung', 'Kota', '35139', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (56, 9, 'Bandung', 'Kabupaten', '40311', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (57, 9, 'Bandung', 'Kota', '40111', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (58, 9, 'Bandung Barat', 'Kabupaten', '40721', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (59, 29, 'Banggai', 'Kabupaten', '94711', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (60, 29, 'Banggai Kepulauan', 'Kabupaten', '94881', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (61, 2, 'Bangka', 'Kabupaten', '33212', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (62, 2, 'Bangka Barat', 'Kabupaten', '33315', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (63, 2, 'Bangka Selatan', 'Kabupaten', '33719', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (64, 2, 'Bangka Tengah', 'Kabupaten', '33613', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (65, 11, 'Bangkalan', 'Kabupaten', '69118', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (66, 1, 'Bangli', 'Kabupaten', '80619', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (67, 13, 'Banjar', 'Kabupaten', '70619', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (68, 9, 'Banjar', 'Kota', '46311', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (69, 13, 'Banjarbaru', 'Kota', '70712', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (70, 13, 'Banjarmasin', 'Kota', '70117', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (71, 10, 'Banjarnegara', 'Kabupaten', '53419', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (72, 28, 'Bantaeng', 'Kabupaten', '92411', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (73, 5, 'Bantul', 'Kabupaten', '55715', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (74, 33, 'Banyuasin', 'Kabupaten', '30911', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (75, 10, 'Banyumas', 'Kabupaten', '53114', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (76, 11, 'Banyuwangi', 'Kabupaten', '68416', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (77, 13, 'Barito Kuala', 'Kabupaten', '70511', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (78, 14, 'Barito Selatan', 'Kabupaten', '73711', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (79, 14, 'Barito Timur', 'Kabupaten', '73671', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (80, 14, 'Barito Utara', 'Kabupaten', '73881', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (81, 28, 'Barru', 'Kabupaten', '90719', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (82, 17, 'Batam', 'Kota', '29413', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (83, 10, 'Batang', 'Kabupaten', '51211', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (84, 8, 'Batang Hari', 'Kabupaten', '36613', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (85, 11, 'Batu', 'Kota', '65311', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (86, 34, 'Batu Bara', 'Kabupaten', '21655', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (87, 30, 'Bau-Bau', 'Kota', '93719', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (88, 9, 'Bekasi', 'Kabupaten', '17837', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (89, 9, 'Bekasi', 'Kota', '17121', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (90, 2, 'Belitung', 'Kabupaten', '33419', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (91, 2, 'Belitung Timur', 'Kabupaten', '33519', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (92, 23, 'Belu', 'Kabupaten', '85711', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (93, 21, 'Bener Meriah', 'Kabupaten', '24581', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (94, 26, 'Bengkalis', 'Kabupaten', '28719', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (95, 12, 'Bengkayang', 'Kabupaten', '79213', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (96, 4, 'Bengkulu', 'Kota', '38229', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (97, 4, 'Bengkulu Selatan', 'Kabupaten', '38519', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (98, 4, 'Bengkulu Tengah', 'Kabupaten', '38319', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (99, 4, 'Bengkulu Utara', 'Kabupaten', '38619', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (100, 15, 'Berau', 'Kabupaten', '77311', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (101, 24, 'Biak Numfor', 'Kabupaten', '98119', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (102, 22, 'Bima', 'Kabupaten', '84171', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (103, 22, 'Bima', 'Kota', '84139', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (104, 34, 'Binjai', 'Kota', '20712', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (105, 17, 'Bintan', 'Kabupaten', '29135', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (106, 21, 'Bireuen', 'Kabupaten', '24219', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (107, 31, 'Bitung', 'Kota', '95512', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (108, 11, 'Blitar', 'Kabupaten', '66171', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (109, 11, 'Blitar', 'Kota', '66124', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (110, 10, 'Blora', 'Kabupaten', '58219', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (111, 7, 'Boalemo', 'Kabupaten', '96319', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (112, 9, 'Bogor', 'Kabupaten', '16911', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (113, 9, 'Bogor', 'Kota', '16119', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (114, 11, 'Bojonegoro', 'Kabupaten', '62119', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (115, 31, 'Bolaang Mongondow (Bolmong)', 'Kabupaten', '95755', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (116, 31, 'Bolaang Mongondow Selatan', 'Kabupaten', '95774', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (117, 31, 'Bolaang Mongondow Timur', 'Kabupaten', '95783', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (118, 31, 'Bolaang Mongondow Utara', 'Kabupaten', '95765', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (119, 30, 'Bombana', 'Kabupaten', '93771', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (120, 11, 'Bondowoso', 'Kabupaten', '68219', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (121, 28, 'Bone', 'Kabupaten', '92713', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (122, 7, 'Bone Bolango', 'Kabupaten', '96511', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (123, 15, 'Bontang', 'Kota', '75313', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (124, 24, 'Boven Digoel', 'Kabupaten', '99662', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (125, 10, 'Boyolali', 'Kabupaten', '57312', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (126, 10, 'Brebes', 'Kabupaten', '52212', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (127, 32, 'Bukittinggi', 'Kota', '26115', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (128, 1, 'Buleleng', 'Kabupaten', '81111', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (129, 28, 'Bulukumba', 'Kabupaten', '92511', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (130, 16, 'Bulungan (Bulongan)', 'Kabupaten', '77211', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (131, 8, 'Bungo', 'Kabupaten', '37216', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (132, 29, 'Buol', 'Kabupaten', '94564', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (133, 19, 'Buru', 'Kabupaten', '97371', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (134, 19, 'Buru Selatan', 'Kabupaten', '97351', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (135, 30, 'Buton', 'Kabupaten', '93754', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (136, 30, 'Buton Utara', 'Kabupaten', '93745', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (137, 9, 'Ciamis', 'Kabupaten', '46211', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (138, 9, 'Cianjur', 'Kabupaten', '43217', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (139, 10, 'Cilacap', 'Kabupaten', '53211', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (140, 3, 'Cilegon', 'Kota', '42417', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (141, 9, 'Cimahi', 'Kota', '40512', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (142, 9, 'Cirebon', 'Kabupaten', '45611', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (143, 9, 'Cirebon', 'Kota', '45116', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (144, 34, 'Dairi', 'Kabupaten', '22211', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (145, 24, 'Deiyai (Deliyai)', 'Kabupaten', '98784', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (146, 34, 'Deli Serdang', 'Kabupaten', '20511', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (147, 10, 'Demak', 'Kabupaten', '59519', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (148, 1, 'Denpasar', 'Kota', '80227', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (149, 9, 'Depok', 'Kota', '16416', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (150, 32, 'Dharmasraya', 'Kabupaten', '27612', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (151, 24, 'Dogiyai', 'Kabupaten', '98866', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (152, 22, 'Dompu', 'Kabupaten', '84217', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (153, 29, 'Donggala', 'Kabupaten', '94341', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (154, 26, 'Dumai', 'Kota', '28811', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (155, 33, 'Empat Lawang', 'Kabupaten', '31811', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (156, 23, 'Ende', 'Kabupaten', '86351', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (157, 28, 'Enrekang', 'Kabupaten', '91719', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (158, 25, 'Fakfak', 'Kabupaten', '98651', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (159, 23, 'Flores Timur', 'Kabupaten', '86213', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (160, 9, 'Garut', 'Kabupaten', '44126', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (161, 21, 'Gayo Lues', 'Kabupaten', '24653', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (162, 1, 'Gianyar', 'Kabupaten', '80519', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (163, 7, 'Gorontalo', 'Kabupaten', '96218', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (164, 7, 'Gorontalo', 'Kota', '96115', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (165, 7, 'Gorontalo Utara', 'Kabupaten', '96611', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (166, 28, 'Gowa', 'Kabupaten', '92111', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (167, 11, 'Gresik', 'Kabupaten', '61115', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (168, 10, 'Grobogan', 'Kabupaten', '58111', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (169, 5, 'Gunung Kidul', 'Kabupaten', '55812', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (170, 14, 'Gunung Mas', 'Kabupaten', '74511', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (171, 34, 'Gunungsitoli', 'Kota', '22813', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (172, 20, 'Halmahera Barat', 'Kabupaten', '97757', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (173, 20, 'Halmahera Selatan', 'Kabupaten', '97911', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (174, 20, 'Halmahera Tengah', 'Kabupaten', '97853', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (175, 20, 'Halmahera Timur', 'Kabupaten', '97862', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (176, 20, 'Halmahera Utara', 'Kabupaten', '97762', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (177, 13, 'Hulu Sungai Selatan', 'Kabupaten', '71212', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (178, 13, 'Hulu Sungai Tengah', 'Kabupaten', '71313', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (179, 13, 'Hulu Sungai Utara', 'Kabupaten', '71419', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (180, 34, 'Humbang Hasundutan', 'Kabupaten', '22457', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (181, 26, 'Indragiri Hilir', 'Kabupaten', '29212', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (182, 26, 'Indragiri Hulu', 'Kabupaten', '29319', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (183, 9, 'Indramayu', 'Kabupaten', '45214', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (184, 24, 'Intan Jaya', 'Kabupaten', '98771', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (185, 6, 'Jakarta Barat', 'Kota', '11220', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (186, 6, 'Jakarta Pusat', 'Kota', '10540', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (187, 6, 'Jakarta Selatan', 'Kota', '12230', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (188, 6, 'Jakarta Timur', 'Kota', '13330', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (189, 6, 'Jakarta Utara', 'Kota', '14140', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (190, 8, 'Jambi', 'Kota', '36111', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (191, 24, 'Jayapura', 'Kabupaten', '99352', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (192, 24, 'Jayapura', 'Kota', '99114', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (193, 24, 'Jayawijaya', 'Kabupaten', '99511', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (194, 11, 'Jember', 'Kabupaten', '68113', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (195, 1, 'Jembrana', 'Kabupaten', '82251', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (196, 28, 'Jeneponto', 'Kabupaten', '92319', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (197, 10, 'Jepara', 'Kabupaten', '59419', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (198, 11, 'Jombang', 'Kabupaten', '61415', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (199, 25, 'Kaimana', 'Kabupaten', '98671', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (200, 26, 'Kampar', 'Kabupaten', '28411', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (201, 14, 'Kapuas', 'Kabupaten', '73583', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (202, 12, 'Kapuas Hulu', 'Kabupaten', '78719', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (203, 10, 'Karanganyar', 'Kabupaten', '57718', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (204, 1, 'Karangasem', 'Kabupaten', '80819', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (205, 9, 'Karawang', 'Kabupaten', '41311', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (206, 17, 'Karimun', 'Kabupaten', '29611', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (207, 34, 'Karo', 'Kabupaten', '22119', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (208, 14, 'Katingan', 'Kabupaten', '74411', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (209, 4, 'Kaur', 'Kabupaten', '38911', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (210, 12, 'Kayong Utara', 'Kabupaten', '78852', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (211, 10, 'Kebumen', 'Kabupaten', '54319', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (212, 11, 'Kediri', 'Kabupaten', '64184', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (213, 11, 'Kediri', 'Kota', '64125', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (214, 24, 'Keerom', 'Kabupaten', '99461', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (215, 10, 'Kendal', 'Kabupaten', '51314', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (216, 30, 'Kendari', 'Kota', '93126', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (217, 4, 'Kepahiang', 'Kabupaten', '39319', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (218, 17, 'Kepulauan Anambas', 'Kabupaten', '29991', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (219, 19, 'Kepulauan Aru', 'Kabupaten', '97681', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (220, 32, 'Kepulauan Mentawai', 'Kabupaten', '25771', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (221, 26, 'Kepulauan Meranti', 'Kabupaten', '28791', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (222, 31, 'Kepulauan Sangihe', 'Kabupaten', '95819', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (223, 6, 'Kepulauan Seribu', 'Kabupaten', '14550', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (224, 31, 'Kepulauan Siau Tagulandang Biaro (Sitaro)', 'Kabupaten', '95862', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (225, 20, 'Kepulauan Sula', 'Kabupaten', '97995', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (226, 31, 'Kepulauan Talaud', 'Kabupaten', '95885', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (227, 24, 'Kepulauan Yapen (Yapen Waropen)', 'Kabupaten', '98211', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (228, 8, 'Kerinci', 'Kabupaten', '37167', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (229, 12, 'Ketapang', 'Kabupaten', '78874', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (230, 10, 'Klaten', 'Kabupaten', '57411', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (231, 1, 'Klungkung', 'Kabupaten', '80719', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (232, 30, 'Kolaka', 'Kabupaten', '93511', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (233, 30, 'Kolaka Utara', 'Kabupaten', '93911', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (234, 30, 'Konawe', 'Kabupaten', '93411', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (235, 30, 'Konawe Selatan', 'Kabupaten', '93811', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (236, 30, 'Konawe Utara', 'Kabupaten', '93311', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (237, 13, 'Kotabaru', 'Kabupaten', '72119', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (238, 31, 'Kotamobagu', 'Kota', '95711', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (239, 14, 'Kotawaringin Barat', 'Kabupaten', '74119', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (240, 14, 'Kotawaringin Timur', 'Kabupaten', '74364', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (241, 26, 'Kuantan Singingi', 'Kabupaten', '29519', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (242, 12, 'Kubu Raya', 'Kabupaten', '78311', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (243, 10, 'Kudus', 'Kabupaten', '59311', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (244, 5, 'Kulon Progo', 'Kabupaten', '55611', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (245, 9, 'Kuningan', 'Kabupaten', '45511', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (246, 23, 'Kupang', 'Kabupaten', '85362', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (247, 23, 'Kupang', 'Kota', '85119', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (248, 15, 'Kutai Barat', 'Kabupaten', '75711', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (249, 15, 'Kutai Kartanegara', 'Kabupaten', '75511', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (250, 15, 'Kutai Timur', 'Kabupaten', '75611', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (251, 34, 'Labuhan Batu', 'Kabupaten', '21412', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (252, 34, 'Labuhan Batu Selatan', 'Kabupaten', '21511', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (253, 34, 'Labuhan Batu Utara', 'Kabupaten', '21711', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (254, 33, 'Lahat', 'Kabupaten', '31419', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (255, 14, 'Lamandau', 'Kabupaten', '74611', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (256, 11, 'Lamongan', 'Kabupaten', '64125', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (257, 18, 'Lampung Barat', 'Kabupaten', '34814', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (258, 18, 'Lampung Selatan', 'Kabupaten', '35511', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (259, 18, 'Lampung Tengah', 'Kabupaten', '34212', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (260, 18, 'Lampung Timur', 'Kabupaten', '34319', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (261, 18, 'Lampung Utara', 'Kabupaten', '34516', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (262, 12, 'Landak', 'Kabupaten', '78319', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (263, 34, 'Langkat', 'Kabupaten', '20811', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (264, 21, 'Langsa', 'Kota', '24412', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (265, 24, 'Lanny Jaya', 'Kabupaten', '99531', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (266, 3, 'Lebak', 'Kabupaten', '42319', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (267, 4, 'Lebong', 'Kabupaten', '39264', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (268, 23, 'Lembata', 'Kabupaten', '86611', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (269, 21, 'Lhokseumawe', 'Kota', '24352', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (270, 32, 'Lima Puluh Koto/Kota', 'Kabupaten', '26671', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (271, 17, 'Lingga', 'Kabupaten', '29811', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (272, 22, 'Lombok Barat', 'Kabupaten', '83311', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (273, 22, 'Lombok Tengah', 'Kabupaten', '83511', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (274, 22, 'Lombok Timur', 'Kabupaten', '83612', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (275, 22, 'Lombok Utara', 'Kabupaten', '83711', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (276, 33, 'Lubuk Linggau', 'Kota', '31614', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (277, 11, 'Lumajang', 'Kabupaten', '67319', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (278, 28, 'Luwu', 'Kabupaten', '91994', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (279, 28, 'Luwu Timur', 'Kabupaten', '92981', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (280, 28, 'Luwu Utara', 'Kabupaten', '92911', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (281, 11, 'Madiun', 'Kabupaten', '63153', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (282, 11, 'Madiun', 'Kota', '63122', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (283, 10, 'Magelang', 'Kabupaten', '56519', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (284, 10, 'Magelang', 'Kota', '56133', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (285, 11, 'Magetan', 'Kabupaten', '63314', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (286, 9, 'Majalengka', 'Kabupaten', '45412', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (287, 27, 'Majene', 'Kabupaten', '91411', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (288, 28, 'Makassar', 'Kota', '90111', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (289, 11, 'Malang', 'Kabupaten', '65163', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (290, 11, 'Malang', 'Kota', '65112', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (291, 16, 'Malinau', 'Kabupaten', '77511', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (292, 19, 'Maluku Barat Daya', 'Kabupaten', '97451', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (293, 19, 'Maluku Tengah', 'Kabupaten', '97513', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (294, 19, 'Maluku Tenggara', 'Kabupaten', '97651', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (295, 19, 'Maluku Tenggara Barat', 'Kabupaten', '97465', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (296, 27, 'Mamasa', 'Kabupaten', '91362', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (297, 24, 'Mamberamo Raya', 'Kabupaten', '99381', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (298, 24, 'Mamberamo Tengah', 'Kabupaten', '99553', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (299, 27, 'Mamuju', 'Kabupaten', '91519', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (300, 27, 'Mamuju Utara', 'Kabupaten', '91571', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (301, 31, 'Manado', 'Kota', '95247', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (302, 34, 'Mandailing Natal', 'Kabupaten', '22916', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (303, 23, 'Manggarai', 'Kabupaten', '86551', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (304, 23, 'Manggarai Barat', 'Kabupaten', '86711', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (305, 23, 'Manggarai Timur', 'Kabupaten', '86811', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (306, 25, 'Manokwari', 'Kabupaten', '98311', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (307, 25, 'Manokwari Selatan', 'Kabupaten', '98355', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (308, 24, 'Mappi', 'Kabupaten', '99853', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (309, 28, 'Maros', 'Kabupaten', '90511', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (310, 22, 'Mataram', 'Kota', '83131', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (311, 25, 'Maybrat', 'Kabupaten', '98051', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (312, 34, 'Medan', 'Kota', '20228', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (313, 12, 'Melawi', 'Kabupaten', '78619', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (314, 8, 'Merangin', 'Kabupaten', '37319', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (315, 24, 'Merauke', 'Kabupaten', '99613', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (316, 18, 'Mesuji', 'Kabupaten', '34911', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (317, 18, 'Metro', 'Kota', '34111', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (318, 24, 'Mimika', 'Kabupaten', '99962', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (319, 31, 'Minahasa', 'Kabupaten', '95614', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (320, 31, 'Minahasa Selatan', 'Kabupaten', '95914', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (321, 31, 'Minahasa Tenggara', 'Kabupaten', '95995', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (322, 31, 'Minahasa Utara', 'Kabupaten', '95316', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (323, 11, 'Mojokerto', 'Kabupaten', '61382', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (324, 11, 'Mojokerto', 'Kota', '61316', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (325, 29, 'Morowali', 'Kabupaten', '94911', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (326, 33, 'Muara Enim', 'Kabupaten', '31315', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (327, 8, 'Muaro Jambi', 'Kabupaten', '36311', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (328, 4, 'Muko Muko', 'Kabupaten', '38715', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (329, 30, 'Muna', 'Kabupaten', '93611', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (330, 14, 'Murung Raya', 'Kabupaten', '73911', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (331, 33, 'Musi Banyuasin', 'Kabupaten', '30719', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (332, 33, 'Musi Rawas', 'Kabupaten', '31661', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (333, 24, 'Nabire', 'Kabupaten', '98816', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (334, 21, 'Nagan Raya', 'Kabupaten', '23674', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (335, 23, 'Nagekeo', 'Kabupaten', '86911', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (336, 17, 'Natuna', 'Kabupaten', '29711', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (337, 24, 'Nduga', 'Kabupaten', '99541', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (338, 23, 'Ngada', 'Kabupaten', '86413', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (339, 11, 'Nganjuk', 'Kabupaten', '64414', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (340, 11, 'Ngawi', 'Kabupaten', '63219', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (341, 34, 'Nias', 'Kabupaten', '22876', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (342, 34, 'Nias Barat', 'Kabupaten', '22895', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (343, 34, 'Nias Selatan', 'Kabupaten', '22865', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (344, 34, 'Nias Utara', 'Kabupaten', '22856', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (345, 16, 'Nunukan', 'Kabupaten', '77421', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (346, 33, 'Ogan Ilir', 'Kabupaten', '30811', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (347, 33, 'Ogan Komering Ilir', 'Kabupaten', '30618', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (348, 33, 'Ogan Komering Ulu', 'Kabupaten', '32112', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (349, 33, 'Ogan Komering Ulu Selatan', 'Kabupaten', '32211', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (350, 33, 'Ogan Komering Ulu Timur', 'Kabupaten', '32312', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (351, 11, 'Pacitan', 'Kabupaten', '63512', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (352, 32, 'Padang', 'Kota', '25112', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (353, 34, 'Padang Lawas', 'Kabupaten', '22763', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (354, 34, 'Padang Lawas Utara', 'Kabupaten', '22753', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (355, 32, 'Padang Panjang', 'Kota', '27122', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (356, 32, 'Padang Pariaman', 'Kabupaten', '25583', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (357, 34, 'Padang Sidempuan', 'Kota', '22727', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (358, 33, 'Pagar Alam', 'Kota', '31512', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (359, 34, 'Pakpak Bharat', 'Kabupaten', '22272', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (360, 14, 'Palangka Raya', 'Kota', '73112', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (361, 33, 'Palembang', 'Kota', '30111', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (362, 28, 'Palopo', 'Kota', '91911', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (363, 29, 'Palu', 'Kota', '94111', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (364, 11, 'Pamekasan', 'Kabupaten', '69319', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (365, 3, 'Pandeglang', 'Kabupaten', '42212', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (366, 9, 'Pangandaran', 'Kabupaten', '46511', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (367, 28, 'Pangkajene Kepulauan', 'Kabupaten', '90611', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (368, 2, 'Pangkal Pinang', 'Kota', '33115', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (369, 24, 'Paniai', 'Kabupaten', '98765', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (370, 28, 'Parepare', 'Kota', '91123', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (371, 32, 'Pariaman', 'Kota', '25511', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (372, 29, 'Parigi Moutong', 'Kabupaten', '94411', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (373, 32, 'Pasaman', 'Kabupaten', '26318', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (374, 32, 'Pasaman Barat', 'Kabupaten', '26511', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (375, 15, 'Paser', 'Kabupaten', '76211', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (376, 11, 'Pasuruan', 'Kabupaten', '67153', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (377, 11, 'Pasuruan', 'Kota', '67118', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (378, 10, 'Pati', 'Kabupaten', '59114', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (379, 32, 'Payakumbuh', 'Kota', '26213', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (380, 25, 'Pegunungan Arfak', 'Kabupaten', '98354', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (381, 24, 'Pegunungan Bintang', 'Kabupaten', '99573', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (382, 10, 'Pekalongan', 'Kabupaten', '51161', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (383, 10, 'Pekalongan', 'Kota', '51122', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (384, 26, 'Pekanbaru', 'Kota', '28112', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (385, 26, 'Pelalawan', 'Kabupaten', '28311', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (386, 10, 'Pemalang', 'Kabupaten', '52319', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (387, 34, 'Pematang Siantar', 'Kota', '21126', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (388, 15, 'Penajam Paser Utara', 'Kabupaten', '76311', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (389, 18, 'Pesawaran', 'Kabupaten', '35312', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (390, 18, 'Pesisir Barat', 'Kabupaten', '35974', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (391, 32, 'Pesisir Selatan', 'Kabupaten', '25611', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (392, 21, 'Pidie', 'Kabupaten', '24116', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (393, 21, 'Pidie Jaya', 'Kabupaten', '24186', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (394, 28, 'Pinrang', 'Kabupaten', '91251', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (395, 7, 'Pohuwato', 'Kabupaten', '96419', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (396, 27, 'Polewali Mandar', 'Kabupaten', '91311', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (397, 11, 'Ponorogo', 'Kabupaten', '63411', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (398, 12, 'Pontianak', 'Kabupaten', '78971', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (399, 12, 'Pontianak', 'Kota', '78112', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (400, 29, 'Poso', 'Kabupaten', '94615', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (401, 33, 'Prabumulih', 'Kota', '31121', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (402, 18, 'Pringsewu', 'Kabupaten', '35719', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (403, 11, 'Probolinggo', 'Kabupaten', '67282', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (404, 11, 'Probolinggo', 'Kota', '67215', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (405, 14, 'Pulang Pisau', 'Kabupaten', '74811', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (406, 20, 'Pulau Morotai', 'Kabupaten', '97771', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (407, 24, 'Puncak', 'Kabupaten', '98981', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (408, 24, 'Puncak Jaya', 'Kabupaten', '98979', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (409, 10, 'Purbalingga', 'Kabupaten', '53312', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (410, 9, 'Purwakarta', 'Kabupaten', '41119', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (411, 10, 'Purworejo', 'Kabupaten', '54111', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (412, 25, 'Raja Ampat', 'Kabupaten', '98489', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (413, 4, 'Rejang Lebong', 'Kabupaten', '39112', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (414, 10, 'Rembang', 'Kabupaten', '59219', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (415, 26, 'Rokan Hilir', 'Kabupaten', '28992', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (416, 26, 'Rokan Hulu', 'Kabupaten', '28511', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (417, 23, 'Rote Ndao', 'Kabupaten', '85982', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (418, 21, 'Sabang', 'Kota', '23512', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (419, 23, 'Sabu Raijua', 'Kabupaten', '85391', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (420, 10, 'Salatiga', 'Kota', '50711', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (421, 15, 'Samarinda', 'Kota', '75133', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (422, 12, 'Sambas', 'Kabupaten', '79453', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (423, 34, 'Samosir', 'Kabupaten', '22392', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (424, 11, 'Sampang', 'Kabupaten', '69219', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (425, 12, 'Sanggau', 'Kabupaten', '78557', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (426, 24, 'Sarmi', 'Kabupaten', '99373', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (427, 8, 'Sarolangun', 'Kabupaten', '37419', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (428, 32, 'Sawah Lunto', 'Kota', '27416', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (429, 12, 'Sekadau', 'Kabupaten', '79583', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (430, 28, 'Selayar (Kepulauan Selayar)', 'Kabupaten', '92812', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (431, 4, 'Seluma', 'Kabupaten', '38811', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (432, 10, 'Semarang', 'Kabupaten', '50511', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (433, 10, 'Semarang', 'Kota', '50135', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (434, 19, 'Seram Bagian Barat', 'Kabupaten', '97561', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (435, 19, 'Seram Bagian Timur', 'Kabupaten', '97581', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (436, 3, 'Serang', 'Kabupaten', '42182', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (437, 3, 'Serang', 'Kota', '42111', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (438, 34, 'Serdang Bedagai', 'Kabupaten', '20915', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (439, 14, 'Seruyan', 'Kabupaten', '74211', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (440, 26, 'Siak', 'Kabupaten', '28623', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (441, 34, 'Sibolga', 'Kota', '22522', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (442, 28, 'Sidenreng Rappang/Rapang', 'Kabupaten', '91613', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (443, 11, 'Sidoarjo', 'Kabupaten', '61219', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (444, 29, 'Sigi', 'Kabupaten', '94364', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (445, 32, 'Sijunjung (Sawah Lunto Sijunjung)', 'Kabupaten', '27511', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (446, 23, 'Sikka', 'Kabupaten', '86121', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (447, 34, 'Simalungun', 'Kabupaten', '21162', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (448, 21, 'Simeulue', 'Kabupaten', '23891', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (449, 12, 'Singkawang', 'Kota', '79117', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (450, 28, 'Sinjai', 'Kabupaten', '92615', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (451, 12, 'Sintang', 'Kabupaten', '78619', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (452, 11, 'Situbondo', 'Kabupaten', '68316', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (453, 5, 'Sleman', 'Kabupaten', '55513', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (454, 32, 'Solok', 'Kabupaten', '27365', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (455, 32, 'Solok', 'Kota', '27315', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (456, 32, 'Solok Selatan', 'Kabupaten', '27779', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (457, 28, 'Soppeng', 'Kabupaten', '90812', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (458, 25, 'Sorong', 'Kabupaten', '98431', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (459, 25, 'Sorong', 'Kota', '98411', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (460, 25, 'Sorong Selatan', 'Kabupaten', '98454', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (461, 10, 'Sragen', 'Kabupaten', '57211', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (462, 9, 'Subang', 'Kabupaten', '41215', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (463, 21, 'Subulussalam', 'Kota', '24882', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (464, 9, 'Sukabumi', 'Kabupaten', '43311', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (465, 9, 'Sukabumi', 'Kota', '43114', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (466, 14, 'Sukamara', 'Kabupaten', '74712', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (467, 10, 'Sukoharjo', 'Kabupaten', '57514', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (468, 23, 'Sumba Barat', 'Kabupaten', '87219', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (469, 23, 'Sumba Barat Daya', 'Kabupaten', '87453', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (470, 23, 'Sumba Tengah', 'Kabupaten', '87358', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (471, 23, 'Sumba Timur', 'Kabupaten', '87112', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (472, 22, 'Sumbawa', 'Kabupaten', '84315', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (473, 22, 'Sumbawa Barat', 'Kabupaten', '84419', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (474, 9, 'Sumedang', 'Kabupaten', '45326', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (475, 11, 'Sumenep', 'Kabupaten', '69413', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (476, 8, 'Sungaipenuh', 'Kota', '37113', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (477, 24, 'Supiori', 'Kabupaten', '98164', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (478, 11, 'Surabaya', 'Kota', '60119', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (479, 10, 'Surakarta (Solo)', 'Kota', '57113', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (480, 13, 'Tabalong', 'Kabupaten', '71513', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (481, 1, 'Tabanan', 'Kabupaten', '82119', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (482, 28, 'Takalar', 'Kabupaten', '92212', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (483, 25, 'Tambrauw', 'Kabupaten', '98475', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (484, 16, 'Tana Tidung', 'Kabupaten', '77611', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (485, 28, 'Tana Toraja', 'Kabupaten', '91819', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (486, 13, 'Tanah Bumbu', 'Kabupaten', '72211', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (487, 32, 'Tanah Datar', 'Kabupaten', '27211', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (488, 13, 'Tanah Laut', 'Kabupaten', '70811', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (489, 3, 'Tangerang', 'Kabupaten', '15914', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (490, 3, 'Tangerang', 'Kota', '15111', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (491, 3, 'Tangerang Selatan', 'Kota', '15332', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (492, 18, 'Tanggamus', 'Kabupaten', '35619', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (493, 34, 'Tanjung Balai', 'Kota', '21321', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (494, 8, 'Tanjung Jabung Barat', 'Kabupaten', '36513', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (495, 8, 'Tanjung Jabung Timur', 'Kabupaten', '36719', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (496, 17, 'Tanjung Pinang', 'Kota', '29111', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (497, 34, 'Tapanuli Selatan', 'Kabupaten', '22742', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (498, 34, 'Tapanuli Tengah', 'Kabupaten', '22611', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (499, 34, 'Tapanuli Utara', 'Kabupaten', '22414', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (500, 13, 'Tapin', 'Kabupaten', '71119', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (501, 16, 'Tarakan', 'Kota', '77114', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (502, 9, 'Tasikmalaya', 'Kabupaten', '46411', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (503, 9, 'Tasikmalaya', 'Kota', '46116', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (504, 34, 'Tebing Tinggi', 'Kota', '20632', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (505, 8, 'Tebo', 'Kabupaten', '37519', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (506, 10, 'Tegal', 'Kabupaten', '52419', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (507, 10, 'Tegal', 'Kota', '52114', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (508, 25, 'Teluk Bintuni', 'Kabupaten', '98551', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (509, 25, 'Teluk Wondama', 'Kabupaten', '98591', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (510, 10, 'Temanggung', 'Kabupaten', '56212', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (511, 20, 'Ternate', 'Kota', '97714', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (512, 20, 'Tidore Kepulauan', 'Kota', '97815', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (513, 23, 'Timor Tengah Selatan', 'Kabupaten', '85562', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (514, 23, 'Timor Tengah Utara', 'Kabupaten', '85612', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (515, 34, 'Toba Samosir', 'Kabupaten', '22316', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (516, 29, 'Tojo Una-Una', 'Kabupaten', '94683', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (517, 29, 'Toli-Toli', 'Kabupaten', '94542', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (518, 24, 'Tolikara', 'Kabupaten', '99411', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (519, 31, 'Tomohon', 'Kota', '95416', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (520, 28, 'Toraja Utara', 'Kabupaten', '91831', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (521, 11, 'Trenggalek', 'Kabupaten', '66312', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (522, 19, 'Tual', 'Kota', '97612', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (523, 11, 'Tuban', 'Kabupaten', '62319', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (524, 18, 'Tulang Bawang', 'Kabupaten', '34613', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (525, 18, 'Tulang Bawang Barat', 'Kabupaten', '34419', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (526, 11, 'Tulungagung', 'Kabupaten', '66212', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (527, 28, 'Wajo', 'Kabupaten', '90911', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (528, 30, 'Wakatobi', 'Kabupaten', '93791', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (529, 24, 'Waropen', 'Kabupaten', '98269', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (530, 18, 'Way Kanan', 'Kabupaten', '34711', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (531, 10, 'Wonogiri', 'Kabupaten', '57619', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (532, 10, 'Wonosobo', 'Kabupaten', '56311', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (533, 24, 'Yahukimo', 'Kabupaten', '99041', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (534, 24, 'Yalimo', 'Kabupaten', '99481', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);
INSERT INTO `set_wilayah` VALUES (535, 5, 'Yogyakarta', 'Kota', '55111', '2020-05-01 18:27:39', '2020-05-01 18:27:39', NULL);

-- ----------------------------
-- Table structure for tran_distribusi
-- ----------------------------
DROP TABLE IF EXISTS `tran_distribusi`;
CREATE TABLE `tran_distribusi`  (
  `id_distribusi` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_transaksi_kebutuhan` bigint(20) NULL DEFAULT NULL,
  `jumlah_distribusi` int(11) NULL DEFAULT NULL,
  `kurir_pengiriman` enum('JNE','POS Indonesia','Tiki') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `resi_pengiriman` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_by` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT current_timestamp(0),
  `updated_at` timestamp(0) NULL DEFAULT current_timestamp(0) ON UPDATE CURRENT_TIMESTAMP(0),
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id_distribusi`) USING BTREE,
  INDEX `fk_trans_kebutuhan_to_distribusi`(`id_transaksi_kebutuhan`) USING BTREE,
  CONSTRAINT `fk_trans_kebutuhan_to_distribusi` FOREIGN KEY (`id_transaksi_kebutuhan`) REFERENCES `tran_kebutuhan` (`id_transaksi_kebutuhan`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tran_kebutuhan
-- ----------------------------
DROP TABLE IF EXISTS `tran_kebutuhan`;
CREATE TABLE `tran_kebutuhan`  (
  `id_transaksi_kebutuhan` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_instansi` bigint(20) NULL DEFAULT NULL,
  `id_kebutuhan` bigint(20) NULL DEFAULT NULL,
  `pic` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `wa_pic` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `alamat_kirim` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `surat_permohonan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `jumlah_permintaan` bigint(20) NULL DEFAULT NULL,
  `jumlah_pemenuhan` bigint(20) NOT NULL DEFAULT 0,
  `estimasi_biaya` bigint(255) NULL DEFAULT NULL,
  `bukti_transfer` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `status` enum('Waiting','Accepted','Finished') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT 'Waiting',
  `created_at` timestamp(0) NULL DEFAULT current_timestamp(0),
  `updated_at` timestamp(0) NULL DEFAULT current_timestamp(0) ON UPDATE CURRENT_TIMESTAMP(0),
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id_transaksi_kebutuhan`) USING BTREE,
  INDEX `fk_instansi_transaksi_to_instansi`(`id_instansi`) USING BTREE,
  INDEX `fk_kebutuhan_transaksi_to_kebutuhan`(`id_kebutuhan`) USING BTREE,
  CONSTRAINT `fk_instansi_transaksi_to_instansi` FOREIGN KEY (`id_instansi`) REFERENCES `set_instansi` (`id_instansi`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_kebutuhan_transaksi_to_kebutuhan` FOREIGN KEY (`id_kebutuhan`) REFERENCES `set_kebutuhan` (`id_kebutuhan`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tran_kebutuhan_umum
-- ----------------------------
DROP TABLE IF EXISTS `tran_kebutuhan_umum`;
CREATE TABLE `tran_kebutuhan_umum`  (
  `id_transaksi_kebutuhan` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_wilayah` bigint(20) NULL DEFAULT NULL,
  `id_kebutuhan` bigint(20) NULL DEFAULT NULL,
  `pic` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `wa_pic` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `alamat_kirim` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `surat_permohonan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `jumlah_permintaan` bigint(20) NULL DEFAULT NULL,
  `jumlah_pemenuhan` bigint(20) NULL DEFAULT NULL,
  `estimasi_biaya` bigint(255) NULL DEFAULT NULL,
  `resi_pengiriman` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `status` enum('Waiting','Accepted','Finished') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT 'Waiting',
  `bukti_transfer` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `nama_instansi` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `jenis_instansi` enum('Pemerintah','Swasta') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `id_tipe` int(11) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT current_timestamp(0),
  `updated_at` timestamp(0) NULL DEFAULT current_timestamp(0) ON UPDATE CURRENT_TIMESTAMP(0),
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id_transaksi_kebutuhan`) USING BTREE,
  INDEX `fk_instansi_transaksi_to_instansi`(`id_wilayah`) USING BTREE,
  INDEX `fk_kebutuhan_transaksi_to_kebutuhan`(`id_kebutuhan`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tran_produksi
-- ----------------------------
DROP TABLE IF EXISTS `tran_produksi`;
CREATE TABLE `tran_produksi`  (
  `id_produksi` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_kebutuhan` bigint(20) NULL DEFAULT NULL,
  `jumlah_produksi` int(11) NULL DEFAULT NULL,
  `created_by` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT current_timestamp(0),
  `updated_at` timestamp(0) NULL DEFAULT current_timestamp(0) ON UPDATE CURRENT_TIMESTAMP(0),
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id_produksi`) USING BTREE,
  INDEX `fk_trans_kebutuhan_to_distribusi`(`id_kebutuhan`) USING BTREE,
  CONSTRAINT `produksi_kebutuhan_to_kebutuhan` FOREIGN KEY (`id_kebutuhan`) REFERENCES `set_kebutuhan` (`id_kebutuhan`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tran_status
-- ----------------------------
DROP TABLE IF EXISTS `tran_status`;
CREATE TABLE `tran_status`  (
  `id_status` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_transaksi_kebutuhan` bigint(20) NULL DEFAULT NULL,
  `nama_penerima` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `jabatan_penerima` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tanggal_serah_terima` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tanda_terima` enum('Sudah','Belum') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `waktu_pengambilan` datetime(0) NULL DEFAULT NULL,
  `distribusi_perhari` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT current_timestamp(0),
  `updated_at` timestamp(0) NULL DEFAULT current_timestamp(0) ON UPDATE CURRENT_TIMESTAMP(0),
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id_status`) USING BTREE,
  INDEX `fk_status_transaksi_to_status`(`id_transaksi_kebutuhan`) USING BTREE,
  CONSTRAINT `fk_status_transaksi_to_status` FOREIGN KEY (`id_transaksi_kebutuhan`) REFERENCES `tran_kebutuhan` (`id_transaksi_kebutuhan`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for uang_donasi
-- ----------------------------
DROP TABLE IF EXISTS `uang_donasi`;
CREATE TABLE `uang_donasi`  (
  `id_uang` bigint(20) NOT NULL AUTO_INCREMENT,
  `jumlah_uang` bigint(20) NULL DEFAULT NULL,
  `nama_donatur_uang` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT 'NULL',
  `created_at` timestamp(0) NOT NULL DEFAULT current_timestamp(0),
  `updated_at` timestamp(0) NOT NULL DEFAULT current_timestamp(0),
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  `rekening_donatur_uang` bigint(20) NULL DEFAULT NULL,
  `instansi_donatur_uang` bigint(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id_uang`) USING BTREE,
  INDEX `instansi_donatur_uang`(`instansi_donatur_uang`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_pengguna` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `jenis_pengguna` enum('Pengguna APD','ITS','Publik','Donatur','Penyalur','Produsen','Kontrol') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `wa` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `alamat` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `email` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created` timestamp(0) NOT NULL DEFAULT current_timestamp(0) ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'Administrator', 'ITS', NULL, NULL, 'admin@gmail.com', 'admin', '2020-05-01 08:16:49');

SET FOREIGN_KEY_CHECKS = 1;
